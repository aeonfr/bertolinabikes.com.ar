if (typeof JSZip == 'undefined'
  || typeof saveAs == 'undefined'
  || typeof _ == 'undefined'
  || typeof EXIF == 'undefined'){
  alert('Una o más dependencias no se cargaron correctamente, lo que podría causar que el programa no funcione. Por favor inténtalo en otro momento. Si el problema continúa por más de 12 horas, o si surge de forma regular, se debe seguramente a un asunto que hay que corregir en el programa, en ese caso contáctese con el administrador.')
}

var imgInput = document.querySelector('input[type="file"]'),
    watermarkInput = document.querySelector('input[type="checkbox"][name="watermark"]'),
    contrastInput = document.querySelector('input[type="checkbox"][name="contrast"]'),
    progress = document.querySelector('progress'),
    $progressCurrVal = document.querySelector('#progressCurrVal'),
    $progressTotalVal = document.querySelector('#progressTotalVal'),
    $progressTask = document.querySelector('#progressTask'),
    canvas = document.querySelector('canvas'),
    ctx = canvas.getContext('2d'),
    signo = document.querySelector('img'),
    zip = new JSZip(),
    //zipFolder = zip.folder("imagenes"),
    images = [];

imgInput.removeAttribute('disabled')

imgInput.addEventListener('change', function(){

  $progressTask.innerHTML = 'Subiendo';
  let progressTotalVal = this.files.length;
  progress.setAttribute('max', progressTotalVal );
  $progressTotalVal.innerHTML = progressTotalVal;
  let progressCurrVal = 0;
  progress.setAttribute('value', progressCurrVal );
  $progressCurrVal.innerHTML = progressCurrVal;

  _.forEach(this.files, function(file){
    let reader = new FileReader();


    reader.onload = function(){
      let image = new Image();

      $progressTask.innerHTML = 'Subiendo...';
      let progressCurrVal = Number(progress.getAttribute('value')) + 1;
      progress.setAttribute('value', progressCurrVal);
      $progressCurrVal.innerHTML = progressCurrVal;

      image.onload = function(){
        images.push({image: image, name: file.name});
        if ( progressCurrVal == progressTotalVal ){ startRendering(); }
      }

      if (!reader.result)
        return alert('Hay problemas, prueba utilizar la última versión de Google Chrome');
      image.src = reader.result;
    }
    
    if (!reader.readAsDataURL) return alert('Hay problemas, prueba utilizar la última versión de Google Chrome')
    reader.readAsDataURL(file);
  
  });

});

function startRendering(){

  $progressTask.innerHTML = 'Redimensionando';
  let progressCurrVal = 0;
  progress.setAttribute('value', progressCurrVal );
  $progressCurrVal.innerHTML = progressCurrVal;

  _.forEach(images, function({ image, name }, i){

    $progressTask.innerHTML = 'Redimensionando: '+name;
    let progressCurrVal = i+1;
    progress.setAttribute('value', progressCurrVal );
    $progressCurrVal.innerHTML = progressCurrVal;

    EXIF.getData(image, function() {
      
      let exifOrientation = EXIF.getTag(this, "Orientation"); // número del 1 al 8
      // Esta imágen muestra los posibles valores de orientación
      // y espejado de las imágenes:
      // https://www.daveperrett.com/images/articles/2012-07-28-exif-orientation-handling-is-a-ghetto/EXIF_Orientations.jpg

      let redimensionar = function (maxWH){

        // redimensionar canvas
        if (image.width >= image.height){
          // landscape
          if (image.width < maxWH){
            maxWH = image.width;
          }
          canvas.width = drawWidth = maxWH;
          canvas.height = drawHeight = canvas.width * (image.height / image.width);

        } else if (image.width < image.height){
          // portrait
          if (image.height < maxWH){
            maxWH = image.height;
          }
          canvas.height = drawHeight = maxWH;
          canvas.width = drawWidth = canvas.height * (image.width / image.height);
        }

        ctx.save();

        var width  = canvas.width;
        var height = canvas.height;

        if (exifOrientation){


          if (exifOrientation > 4) {
            canvas.width  = height;
            canvas.height = width;
          }

          switch(exifOrientation){
            case 2:
              // horizontal flip
              ctx.translate(width, 0);
              ctx.scale(-1, 1);
              break;
            case 3:
              // 180° rotate left
              ctx.translate(width, height);
              ctx.rotate(Math.PI);
              break;
            case 4:
              // vertical flip
              ctx.translate(0, height);
              ctx.scale(1, -1);
              break;
            case 5:
              // vertical flip + 90 rotate right
              ctx.rotate(0.5 * Math.PI);
              ctx.scale(1, -1);
              break;
            case 6:
              // 90° rotate right
              ctx.rotate(0.5 * Math.PI);
              ctx.translate(0, -height);
              break;
            case 7:
              // horizontal flip + 90 rotate right
              ctx.rotate(0.5 * Math.PI);
              ctx.translate(width, -height);
              ctx.scale(-1, 1);
              break;
            case 8:
              // 90° rotate left
              ctx.rotate(-0.5 * Math.PI);
              ctx.translate(-width, 0);
              break;
          }

        }


        ctx.drawImage(image, 0, 0, width, height);

        if (contrastInput.checked){
          ctx.globalCompositeOperation = "multiply";
          ctx.drawImage(image, 0, 0, width, height);
          ctx.globalCompositeOperation = "screen";
          ctx.drawImage(image, 0, 0, width, height);

        }


        ctx.restore();
        
        if ( watermarkInput.checked ){

          ctx.save();
          
          let watermarkReduction = wR = 0.5,
            watermarkSize = Math.max(canvas.width, canvas.height) * wR;
          
          if (exifOrientation && exifOrientation > 4){

            switch (exifOrientation){
              case 5:
                // vertical flip + 90 rotate right
                ctx.rotate(0.5 * Math.PI);
                ctx.scale(1, -1);
                break;
              case 6:
                // 90° rotate left
                ctx.rotate(-0.5 * Math.PI);
                ctx.translate(-canvas.width, 0);
                break;
              case 7:
                // horizontal flip + 90 rotate right
                ctx.rotate(0.5 * Math.PI);
                ctx.translate(canvas.width, -canvas.height);
                ctx.scale(-1, 1);
                break;
              case 8:
                // 90° rotate right
                ctx.rotate(0.5 * Math.PI);
                ctx.translate(0, -canvas.height);
                break;
            }
          }

          ctx.globalCompositeOperation = "overlay"; //"color", "color-burn", "color-dodge", "darken", "difference", "exclusion", "hard-light", "hue", "lighten", "luminosity", "multiply", "overlay", "saturation", "screen", "soft-light"
          ctx.drawImage(signo, (canvas.width * 0.5 - watermarkSize/2), (canvas.height * 0.5 - watermarkSize/2), watermarkSize, watermarkSize );
          

          ctx.restore();
          
        }
        

        //add img to zip folder
        let newImageName = name.split('.')[0] + '.jpg',
          dataURL = canvas.toDataURL('image/jpeg', 0.75).replace('data:image/jpeg;base64,', '');
        
        if (images.length > 1){
          return zip.file(newImageName, dataURL, {base64: true});
        } else {
          progress.setAttribute('value', 0 );
          $progressCurrVal.innerHTML = 0;
          $progressTask.innerHTML = 'Generando archivo...';
          return canvas.toBlob(function(blob) {
              saveAs(blob, newImageName);
              listo();
          }, "image/jpeg", 0.75);
        }
        
      }

      redimensionar(1200);
         
        
    });



  });

  if (images.length > 1){
    //generate zip file asynchronously
    progress.setAttribute('value', 0 );
    $progressCurrVal.innerHTML = 0;
    $progressTask.innerHTML = 'Generando archivo ZIP...';
    
    zip.generateAsync({type:'blob'})
    .then(function(content){
      saveAs(content, "imagenes.zip");
      listo();
    });

  }


}

function listo(){
  zip = new JSZip();
  images = [];

  progress.setAttribute('max', 1 );
  $progressTotalVal.innerHTML = 1;
  progress.setAttribute('value', 1 );
  $progressCurrVal.innerHTML = 1;
  $progressTask.innerHTML = 'Listo.';
}
// For vueify to not include hot-reload and other stuff
process.env.NODE_ENV = 'production';

var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var stripCssComments = require('gulp-strip-css-comments');
// redimensionar imágenes, manteniendo optimizado y limpio el repositorio.
var del = require('del');
var cache = require('gulp-cached');
var jimp = require("gulp-jimp-resize");
// aumentar la performance de javascript
var concat = require('gulp-concat');
//var rename = require('gulp-rename');
var uglify = require('gulp-uglify');

var browserify = require('browserify')
var vueify = require('vueify')
var fs = require('fs');

//var uglifyify = require('ug')

//sass
gulp.task('sass', function () {
    return gulp.src(['_scss/*.scss'])
        .pipe(sass({
            outputStyle: 'compact',
            includePaths: ['./_scss/']
        }).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(stripCssComments())
        .pipe(gulp.dest('static/recursos/css/'));
});


//js
gulp.task('js', function(){

    // photoswipe.js
    gulp.src(['./_js/lib/photoswipe.min.js', './_js/lib/photoswipe-ui-default.min.js', './_js/initphotoswipe.js'])
        .pipe(concat('bundle-photoswipe.js'))
        .pipe(uglify())
        .pipe(gulp.dest('static/recursos/js/'));

    // share.js
    gulp.src('./_js/share.js')
    .pipe(uglify())
    .pipe(gulp.dest('./static/recursos/js/'));

    // mercadolibreStatusCheck.js
    gulp.src(['./_js/lib/axios.min.js', './_js/mercadolibreStatusCheck.js'])
    .pipe(concat('mercadolibreStatusCheck.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./static/recursos/js/'));


    
    
});

gulp.task('vue', function(){
    browserify('./_js/comprar-single/main.js')
    .transform(vueify)
    .transform('uglifyify')
    .bundle()
    .pipe(fs.createWriteStream("./static/recursos/js/comprar-single.js"))
})

//images
gulp.task('images', function() {
    return gulp.src(
        './static/imagenes/**/*.{png,jpg,bmp}', {base:'static/imagenes/'}
        )
    .pipe(cache('imageThumbs'))
    .pipe(jimp({
        sizes: [
            {"height": 360},
        ]
    }))
    .pipe(gulp.dest('static/thumbnails/'));
});

// clean up everything in thumbs folder and re-build it
gulp.task('image-folder-clean', function(){
    return del('static/thumbnails/**/*');
});

gulp.task('watch', function () {
    gulp.watch('_js/*.js', ['js']);
    gulp.watch(['_js/comprar-single/main.js', '_js/comprar-single/*.vue'], ['vue']);
    gulp.watch('_scss/*.scss', ['sass']);
});

// build task: aimed at the server (netlify)

gulp.task('build', ['sass', 'js'])

gulp.task('default', ['sass', 'js', 'watch']);

// lentísima, se recomienda usar "cloudinary" para las imágenes a partir de ahora
gulp.task('regen-thumbnails', ['image-folder-clean', 'images']);
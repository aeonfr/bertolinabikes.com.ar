<!--

pg. 2 - TP

¿Skyline Loki no cambió de precio?

Descripción de "Pedales Exustar" es =LEN(2)

Precio de Piñón XT M800 y casco Bluegrass es exactamente el mismo (checkeando q no sea un error)

-->

# www.bertolinabikes.com.ar

Sitio web que corre bajo el generador de sitios estáticos “Hugo”.

## Contenidos

### Instalar entorno
### Subir imágenes
### Modificar contenido
### Modificar layouts y CSS
---

## Instalar entorno

Para instalar el entorno para modificar el contenido es necesario instalar estos programas:

- GoLang
- Hugo
- Git (y luego de instalarlo, iniciá sesión en tu cuenta)
- Opcional: en vez de instalar Git, instalar “GitHub for Windows” - tiene una mejorada interfaz gráfica más intuitiva y simple, aunque es más pesado y consume más recursos.

La instalación “inicial” también requiere clonar el repositorio a una carpeta de la PC en la que se va a trabajar. El repositorio se clona de git con el siguiente comando: `git clone https://...[url_del_repositorio]` ([o descargar en formato `zip` por este link](https://github.com/AeonFr/bertolinabikes/archive/master.zip)).

Nota: Si ya instalaste el repositorio previamente, antes de realizar cambios deberás hacer un `pull` desde GitHub para asegurarte que tu versión del repositorio es la más reciente. Para ello usá el comando `git pull origin master`.


### Subir imágenes:

---

Para actualizar el contenido es necesario tener instalado Hugo y Git.
Modificar el contenido requiere de conocimiento básico de la consola del sistema operativo. En Windows 10, vé a Menú > Buscar > y busca "PowerShell", o en otras versiones de Windows, presiona Menú > Ejecutar > y escribe "cmd" y luego `ENTER`.

1. El servidor se empieza a correr con Hugo y se realizan los cambios en el contenido. Para esto, usá la consola para navegar hasta la carpeta donde guardaste el repositorio y empezá el servidor con el comando `hugo serve`.
2. El sitio empezará a correr localmente en la URL: <http://localhost:1313/>, al cual podés acceder desde cualquier navegador web dentro de la misma computadora.
3. Las imágenes se sitúan en la carpeta `static/recursos/`. Subí todas las imágenes que quieras y podrás verlas en `http://localhost:1313/imagenes/{...nombre del archivo...}`.
4. Una vez que estás conforme con los cambios realizados, cerrá el servicio de `hugo serve` con CTRL+C. Luego, “pusheá” tus cambios en el repositorio de `git`. Para ello, deberás: (1) crear un cambio de estado, (2) hacer un commit y (3) hacer un `push` a github.com (subir los cambios a internet).\
Podés utilizar la interfaz gráfica de GIT, que se abre poniendo en la consola `git gui`. Si instalaste GitHub for Windows, podrás utilizar una interfaz un poco más amigable. Y si preferís simplemente utilizar la consola, sólo es cuestión de introducir los tres siguientes comandos...
```
git add .
git commit -m "nombre del commit: acá, explicá los cambios realizados en el contenido"
git push origin master
```
5. Luego de hacer el `push`, los cambios se reflejarán en www.bertolinabikes.com.ar dentro de los próximos 5 minutos, siempre y cuando se cumplan las siguiente condiciones:
		5.1. Tenés suficiente acceso como para hacer `push` en este repositorio.
		5.2. El servidor netlify.com está configurado correctamente y el sitio se genera actualmente con el branch `master`.
		5.3. No haz modificado código donde no debías, ni haz introducido un `merge conflict` al olvidar de hacer un `git pull` antes de modificar el contenido en tu copia local del repositorio.


## Modificar el contenido

Nota: en esta ocasión se explica todo más rápido, para una explicación más detallada, mirar el tutorial de cómo subir imágenes hasta el paso 3.

1. Abrí la consola
2. Hacé un pull y empezá el servidor:
```
git pull origin
hugo serve
``` 

El contenido se ubica en la carpeta `content/` dentro del repositorio.

Para un poco más de información sobre en qué formato se encuentra este contenido y cómo modificarlo, mirar este artículo:

[Modificar el contenido](content/meta/modificar-el-contenido.md)

## Modificar layouts y CSS

Para modificar layouts del sitio, deberás conocer el lenguaje de programación Go y el sistema de templating de Hugo. Los layouts se ubican en la carpeta `themes/bertolina-theme/layouts/`

Para modificar la hoja de estilos, tener en cuenta que se genera por SASS teniendo en cuenta el contenido de la carpeta `_scss`.
Deberás instalar Gulp y correr la tarea por defecto para que todos los cambios en dicha carpeta modifiquen las hojas de estilos del sitio.

```
gulp
```

Recordar pushear los cambios para que se vean en el sitio final.

```
git add .
git commit -m "cambios en el contenido"
git push origin master
```
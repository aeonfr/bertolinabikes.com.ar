---
title: "Lanzamiento de nuestro nuevo sitio web"
date: 2017-11-17T20:35:04-03:00
draft: false
cloudinary: aeonfr
imagen: v1512673441/bertolina/bienvenidos-al-nuevo-sitio-3.jpg
---

Estamos contentos de finalmente lanzar nuestro propio sitio web. Empezamos el proyecto como una oportunidad de darle un espacio propio al contenido que generamos en otras plataformas, como MercadoLibre, Facebook e Instagram.

Al tener el contenido en nuestro propio sitio, podemos unificar la información y presentarla de forma más significativa. Creemos que este es el camino a seguir para mejorar la experiencia de nuestros clientes.

En la sección [**Productos**](/productos) vamos a estar subiendo información completa sobre las bicicletas y componentes que trabajamos.

Los productos tienen links hacia una o varias publicaciones de MercadoLibre, donde se puede consultar por stock y realizar la compra. Dentro de algunas bicicletas puede haber 2, 4 o más links hacia publicaciones de <abbr>ML</abbr>; de esta forma se puede comparar el precio entre bicicletas con distintos sistemas de freno, o con cambios de distinta gamma, etc.

Además, es posible navegar en los productos por [**marcas**](/marcas/) o [**categorías**](/categorias/).

Incluimos en los productos un link directo a Whatsapp, que abre una conversación con Eugenio Bertolina (ventas) e inserta el link del producto. De esta forma esperamos reducir la fricción a la hora de realizar una consulta.

El catálogo actualmente está completo con aproximadamente el 25% de las publicaciones, que corresponden a los productos más vendidos. Seguiremos trabajando para tener pronto un catálogo lo más completo posible.

<a href="/productos/" class="no-decoration db p1 bg-negro blanco flex">
  <span>  Explorar todos los productos</span>
  <svg class="w1 h1 ml-auto dib" fill="currentColor" viewBox="0 0 24 24">
    <path d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"/>
    </svg>
</a>

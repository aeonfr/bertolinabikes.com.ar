---
title: "¡Cumplimos 2 años y festejamos con las mejores ofertas!"
date: 2017-11-28T19:07:17-03:00
draft: true
cloudinary: aeonfr
imagen: "v1511998690/bertolina/2017-11-29-portada-2-años-1.jpg"
gallery:
  - cid: "v1513041327/bertolina/2years/xtr-shift.jpg"
    size: 1280x1000
    description: "Oferta: XTR Shift"
  - cid: "v1513041270/bertolina/2years/venzo-stinger-verde.jpg"
    size: 1280x1000
    description: "Oferta: Venzo Stinger (todos los colores)"
  - cid: "v1513041307/bertolina/2years/venzo-loki.jpg"
    size: 1280x1000
    description: "Oferta: Venzo Loki (todos los colores)"
  - cid: "v1513041226/bertolina/2years/venzo-eolo.jpg"
    size: 1280x1000
    description: "Oferta: Venzo Eolo (todos los colores)"
  - cid: "v1513041534/bertolina/2years/venzo-amphion.jpg"
    size: 1280x1000
    description: "Oferta: Venzo Amphion (todos los colores)"
  - cid: "v1513041414/bertolina/2years/GT.jpg"
    size: 1280x1000
    description: "Oferta: Bicicleta GT"
  - cid: "bertolina/2years/pieza-metal-negra.jpg"
    size: 663x1000
    description: "Oferta: Pieza de aluminio negro"
  - cid: "v1513041513/bertolina/2years/otras-suspensiones.jpg"
    size: 663x1000
    description: "Oferta: Suspensiones ROCK SHOX"
  - cid: "v1513041534/bertolina/2years/maxxis_wtb.jpg"
    size: 1280x1000
    description: "Oferta: Llantas MAXXIS y WTB"
  - cid: "v1513041500/bertolina/2years/marco-venzo-verde.jpg"
    size: 1280x1000
    description: "Oferta: Marco Venzo (todos los colores)"
  - cid: "v1513041533/bertolina/2years/llantas-maxxis.jpg"
    size: 1280x1000
    description: "Oferta: Llantas MAXXIS IKON - CROSSMARK - PACE - ¡Otros tamaños disponibles!"
  - cid: "v1513041468/bertolina/2years/indumentaria.jpg"
    size: 1280x1000
    description: "Oferta: Indumentaria"
  - cid: "v1513041416/bertolina/2years/fox_rhythm.jpg"
    size: 1280x1000
    description: "Oferta: Suspensión Fox Rhythm"
  - cid: v1513041505/bertolina/2years/disk-brake-pads.jpg
    size: 1280x1000
    description: "Oferta: Disk Brake Pads"
  - cid: v1513041452/bertolina/2years/flowtrail_y_EA_70_xct.jpg
    size: 1280x1000
    description: "Oferta: Flowtrail y EA 70 XCT"
  - cid: v1513041389/bertolina/2years/dragon.jpg
    size: 663x1000
    description: "Oferta: Dragon"
  - cid: v1513041371/bertolina/2years/corona-roja.jpg
    size: 663x1000
    description: "Oferta: Corona cromada roja"
  - cid: v1513041359/bertolina/2years/corona-negra.jpg
    size: 663x1000
    description: "Oferta: Corona cromada negra (diseño 1)"
  - cid: v1513041417/bertolina/2years/corona-negra-2.jpg
    size: 663x1000
    description: "Oferta: Corona cromada negra (diseño 2)"
  - cid: v1513041340/bertolina/2years/corona-first-narrow_negra.jpg
    size: 663x1000
    description: "Oferta: Corona 'First Narrow' metálica negra"
  - cid: v1513041348/bertolina/2years/corona-azul.jpg
    size: 663x1000
    description: "Oferta: Corona cromada Azul"
  - cid: v1513041284/bertolina/2years/cascos.jpg
    size: 1280x1000
    description: "Oferta: Cascos"
  - cid: v1513041224/bertolina/2years/cascos_protec.jpg
    size: 1280x1000
    description: "Oferta: Cascos Pro-Tec (variedad de colores y diseños)"
  - cid: v1513041235/bertolina/2years/casco_met_1.jpg
    size: 1280x1000
    description: "Oferta: Cascos MET (diseño 1)"
  - cid: v1513041324/bertolina/2years/casco_met_2.jpg
    size: 1280x1000
    description: "Oferta: Cascos MET (diseño 2)"
  - cid: v1513041227/bertolina/2years/asientos-.jpg
    size: 1280x1000
    description: "Oferta: Asientos"
  - cid: v1513041206/bertolina/2years/asiento-01.jpg
    size: 1280x1000
    description: "Oferta: Asientos antiprostáticos"
---


Celebramos dos años desde la apertura de nuestro local con las mejores ofertas del año. Durante todo Diciembre podés aprobechar productos a precios de descuento.

Contactate con el producto que querés por Facebook o Whatsapp. Al igual que siempre podés pagar en tarjeta (Todo Pago) o mediante transferencia bancaria.

Los descuentos también son válidos en el espacio comercial ubicado en Pueyrredón al 1506 (Córdoba Capital).

Muy pronto vamos a subir todos los precios de los productos en oferta (que se muestran en la galería de abajo). Por ahora podés consultarnos por Facebook o Whatsapp.


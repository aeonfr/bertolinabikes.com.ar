---
title: "Hot Sale 2018"
date: 2018-05-09
draft: false
cloudinary: "aeonfr"
imagen: "bertolina/las-ofertas-del-hot-sale-estan-llegando-20-2"
---

<div class="f1 mt2 rojo-oscuro">Del 14 al 20 de Mayo tenés importantes beneficios en tu compra online.</div>

<ul class="f2 gris-oscuro mt2">

  <li><b>12, 18 y 24 cuotas sin interés</b> pagando con tu billetera virtual de TodoPago en <a href="/hot-sale">los productos en oferta</a>.</li>
  <li>Como siempre, <b>hasta 12 cuotas sin interés</b> con tu tarjeta Visa, Mastercard o American Express en todos nuestros productos.</li>
  <li>3, 6 y hasta 12 cuotas comprando por MercadoLibre (se aplica a las <a href="https://www.mercadopago.com.ar/promociones/">tarjetas en promoción</a>).</li>
</ul>

<div class="f2 mt2 gris-oscuro">+ Podés ver todos los productos en oferta <b>directamente desde nuestro sitio web</b>, o explorar todo nuestro catálogo <a href="https://perfil.mercadolibre.com.ar/BERTOLINA+BIKES">en MercadoLibre</a>.</div>


<a class="dib mt2 no-decoration bg-rojo bg-rojo-oscuro-hover shadow-1 blanco px1 py0-5 small-caps" href="/hot-sale">Explorar Ofertas</a>
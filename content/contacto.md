---
title: "Contacto"
layout: "page"
---


<div class="flex-l mb1">
  
  <div class="flex-grow-1 mb0">
    <b>Celular:</b>
    <a class="nowrap db-ns f2 no-decoration rojo mt0-5" href="tel:543517052401">(351) 157 052 401</a>
  </div>

  <div class="flex-grow-1 mb0">
    <b>Teléfono:</b>
    <a class="nowrap db-ns f2 no-decoration rojo mt0-5" href="tel:035148940101">(0351) 489 4010</a>
  </div>

  <div class="flex-grow-1 mb0">
    <b>Mail:</b>
    <a class="nowrap db-ns f2 no-decoration rojo mt0-5" href="mailto:bertolinabikes@gmail.com">bertolinabikes@gmail.com</a>
  </div>

</div>

**Dirección**: Pueyrredón 1506, Córdoba (X5000), Argentina

**Horarios de atención**:\
• **Lunes a Viernes**: 10 a 14 horas y 16 a 20 horas\
• **Sábados**: 10 a 14 horas

---

<iframe
  class="bg-casi-blanco mb1"
  width="100%"
  height="450"
  style="max-height:80vh;filter: grayscale();"
  frameborder="0" style="border:0"
  src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCK0Ucj8UKZIxsSTZmtdKyByxkghk51RsU&q=Bertolina+Bikes,Córdoba" allowfullscreen>
</iframe>


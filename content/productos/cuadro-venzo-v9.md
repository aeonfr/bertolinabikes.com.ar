---
  title: "Cuadro Venzo Elemento Rodado 29 (Aluminio V9)"
  weight: -27
  imagen: "cuadro-venzo-elemento.jpg"
  description: "Cuadro Venzo Elemento Aluminio V9 Super Liviano Envio Gratis"
  marcas: 
    - "venzo"
  categorias: 
    - "componentes"
  mercadolibre_id: "645515446"
  date: "2018-06-06"
---


Cuadro VENZO Elemento Rodado 29

# Características

- Material: Aluminio V9
- Peso: 1,7 kg aproximadamente, varía segun el talle
- Frente: Conico  (1 1/8 - 1 1/2)
- Tipo freno: Solo Disco

# Talles

- "S" (15)
- "M" (17)
- "L" (19)
- "XL" (21)

*CONSULTAR POR OTROS RODADOS*
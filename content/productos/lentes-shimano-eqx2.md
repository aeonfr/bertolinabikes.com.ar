---
draft: true
title: "Lentes Shimano Eqx2"
date: 2017-11-08T20:28:19-03:00
description: "Lentes Shimano EQX2 PH Fotocromaticos"
imagen: "lentes-shimano-eqx2.jpg"
weight: -1
categorias: ["indumentaria"]
marcas: ["shimano"]
envio_gratis: false
mercadolibre:
  - link: https://articulo.mercadolibre.com.ar/MLA-653831050-lentes-shimano-eqx2-fotocromaticos-con-repuesto-y-estuche-_JM
    titulo: Lentes Shimano Eqx2 Fotocromaticos Con Repuesto Y Estuche
---

# Descripción
Lentes Shimano EQX2 PH Fotocromaticos

# Características
- Marco reforzado
- Soporte nazal y patillas ergonomicas
- Lente desmontable (cambiable)
- Repuesto amarillo
- Estuche rigido
- Funda

# Colores del marco
- Azul
- Negro

![Lentes Shimano EQX2 - Marco blanco](/imagenes/lentes-shimano-eqx2.jpg)
![Lentes Shimano EQX2 - Marco azul](/imagenes/lentes-shimano-eqx2-02.jpg)
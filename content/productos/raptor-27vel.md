---
  title: "Venzo Raptor Rodado 29 - 27 Veloc Disco Hidraul"
  date: "2018-08-16"
  mercadolibre_id: "646342814"
  imgUrl: true
  imagen: "http://mla-s1-p.mlstatic.com/650613-MLA27963257227_082018-F.jpg"
  marcas: 
    - "venzo"
  description: "Bicicleta para Mountain Bike Venzo Raptor 2018"
  categorias: ["bicicletas"]
  weight: -101
---


BICICLETA VENZO RAPTOR 2018 RODADO 29 CON 27 VELOCIDADES SHIMANO

# CARACTERISTICAS:
- Rodado:29
- Cuadro: AMPHION ALUMINIO LIVIANO
- Transmisión: FULL SHIMANO 27 velocidades (Cambio,piñon, cadena, shifters Shimano ACERA, descarrilador Altus)
- Frenos: FRENOS A DISCO HIDRAULICOS SHIMANO
- Suspensión: Rst GILA con bloqueo y regulacion
- Llantas: DOBLE PARED LIVIANAS
- Mazas a ruleman
- Cubiertas: MTB 29x2.0
- Manubrio, Stem, Portasilla, Asiento en combinación con el color del cuadro

#TALLES:
- S (16")
- M (18")
- L (20")
- XL (22")

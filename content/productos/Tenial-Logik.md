---
  title: "Teknial Logik Rodado 20 Plegable Shimano Nexus 3v"
  date: "2018-08-16"
  mercadolibre_id: "742000576"
  imgUrl: true
  imagen: "http://mla-s1-p.mlstatic.com/911939-MLA27929225803_082018-F.jpg"
  marcas: 
    - "teknial"
  description: "Bicicleta Teknial LOGIK Rodado 20 Plegable."
  categorias: ["bicicletas"]
  weight: -99
---

Bicicleta Teknial LOGIK Rodado 20 Plegable

# Características

- CUADRO – RODADO 20 ALUMINIO FOLDING BIKE
- SHIFTER – SHIMANO SL – 3S41E NEXUS
- PLATO – TEKNIAL PRO7-S244A1 44T
- PIÑÓN – SHIMANO NEXUS 3 VELOCIDADES INTERNO
- CADENA – KMC Z410
- PEDALES – FEIMIN FP-909
- LLANTA – ALUMINIO DOBLE PARED 28H
- CUBIERTA – INNOVA 20*1.5
- STEAM – TEKNIAL ALUMINIO MD-DS08/DS07
- MANUBRIO – TEKNIAL ALUMINIO 560MM
- FRENOS – ALUMINIO V-BRAKE
- ASIENTO – TEKNIAL
- CAÑO DE ASIENTO – ALUMINIO CON ABRAZADERA 34*550MM
---
  title: "Pedales Shimano Xt M8000"
  imagen: "pedal-deore-xt.jpg"
  mercadolibre_id: "621426033"
  marcas: 
    - "shimano"
  categorias: 
    - "componentes"
  description: "Pedales para Mountain Bike Shimano XT M8000. Con Calas"
  weight: -10
  date: "2016-05-23"
---


Pedales Shimano XT M8000 con calas
---
title: "Luz Trasera 5 Leds Recargable"
date: 2017-11-20T21:11:47-03:00
description: "Luz de led trasera para bicicleta recargable con USB"
cloudinary: "aeonfr"
imagen: "v1511223332/bertolina/luz-trasera-5-leds.jpg"
weight: 2
categorias: ["accesorios", "seguridad"]
envio_gratis: false
mercadolibre:
  - link: https://articulo.mercadolibre.com.ar/MLA-688855181-luz-trasera-5-leds-para-bicicleta-recargable-usb-3-funciones-_JM
---

# Descripción
Luz de led trasera para bicicleta recargable con USB

- 5 Leds
- 4 Funciones
- 15 Lumenes
- Hasta 12 hs de duracion 
- A prueba de agua
- Bateria de litio
- Incluye cable USB
- 2 hs de carga

{{% img cloudinary="aeonfr" cid="v1511223332/bertolina/luz-trasera-5-leds.jpg" %}}
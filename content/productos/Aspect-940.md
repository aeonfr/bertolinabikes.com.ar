---
  title: "Scott Aspect 940 Rodado 29 27 Vel Frenos Hidraul"
  date: "2018-08-07"
  mercadolibre_id: "740924346"
  imgUrl: true
  imagen: "http://mla-s1-p.mlstatic.com/737676-MLA27894470297_082018-F.jpg"
  marcas: 
    - "Scott"
  description: "Bicicleta Scott Aspect 940"
  categorias: ["bicicletas"]
  weight: -99
---
Bicicleta Scott Aspect 940 Rodado 29 modelo 2018

# Características
- Cuadro de aluminio 
- Horquilla de 100 mm Suntour XCM-HLO
- Shimano Alivio-Acera, 27 velocidades
- Frenos Shimano
- Doble refuerzo / Geometría Performance / Cableado interno
- Bloqueo hidráulico
- Cuadro Aspect 700/900 series
- Aluminio 6061 DB / Performance geometry
- Horquilla Suntour XCM-HLOHyd. Lockout / 100mm travel
- Direccion Ritchey LOGIC ZERO-OE
- Cambio Shimano Alivio RD-M400027 Speed
- Descarrilador Shimano FD-M3000 / 31.8mm
- Shifters Shimano SL-M2000-9RR-fire plus
- Frenos Shimano BR-M315 / Hidraulicos Rotor / 160F/160R
- Plato palanca Shimano FC-M200040X-30x22
- Caja Shimano BB-UN-26Cartridge Type
- Forma Syncros M3.0 / 720mmblack / 31,8mm / 12mm rise / 9° BS
- Stem Syncros M3.0 / 7° / Black
- Pedales VP VP-536
- Portasilla Syncros M3.027.2mm / 350mm / Black
- Asiento Syncros M3.0
- Maza delantera Shimano HB TX505
- Maza trasera Shimano FH-TX5058
- Cadena KMC X9
- Cassette Shimano CS-HG200-9 / 11-34T
- Rayos 14 G / stainless / black
- Llantas Syncros X-20 Disc32H / black
- Cubiertas Kenda Slant 62.35 / 30TPI
- Peso aproximado KG 14.30
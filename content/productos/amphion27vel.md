---
  title: "Venzo Amphion Rodado 29 - 27 Vel"
  draft: false
  description: "Bicicleta para Mountain Bike con cuadro de aluminio 6061 y componentes de Shimano. Marca: Venzo. Modelo: Amphion."
  imagen: "amphion27vel.jpg"
  weight: -109
  categorias: 
    - "bicicletas"
  marcas: 
    - "venzo"
  date: "2017-10-30"
  mercadolibre_id: "724620277"
  lowest_price_multiplier: 1
---



# CARACTERISTICAS:
- Rodado:29
- Cuadro: AMPHION ALUMINIO LIVIANO
- Transmisión: 27 velocidades FULL SHIMANO
- Frenos: FRENOS A DISCO HIDRAULICOS SHIMANO
- Suspensión: SUNTOUR XCT / VENZO / RST BLAZE con bloqueo (sujeto a disponibilidad del proveedor)
- Llantas: DOBLE PARED LIVIANAS
- Mazas a ruleman
- Cubiertas: MTB 29x 2.0
- Manubrio, Stem, Portasilla, Asiento en combinación con el color del cuadro

# COLORES: 

- Negro con detalles en:
  - celeste y verde
  - rosa
  - naranja
  - amarillo
  - rojo
  - blanco
  
# TALLES:
- S (16")
- M (18")
- L (20")
---
  title: "Venzo Amphion Rodado 29 - 24 Vel"
  draft: false
  description: "Bicicleta para Mountain Bike con cuadro de aluminio 6061 y componentes de Shimano. Marca: Venzo. Modelo: Amphion."
  imagen: "amphion24vel.jpg"
  weight: -110
  categorias: 
    - "bicicletas"
  marcas: 
    - "venzo"
  date: "2017-10-30"
  mercadolibre_id: "641056629"
  lowest_price_multiplier: 1
---


# CARACTERISTICAS:
- Rodado:29
- Cuadro: AMPHION ALUMINIO LIVIANO
- Transmisión: FULL SHIMANO 24 velocidades
- Frenos: FRENOS A DISCO HIDRAULICOS SHIMANO
- Suspensión: SUNTOUR XCT O RST BLAZE (sujeto a disponiblidad del proveedor)
- Llantas: DOBLE PARED LIVIANAS
- Mazas a ruleman
- Cubiertas: MTB 29x2.0
- Manubrio, Stem, Portasilla, Asiento en combinación con el color del cuadro

# COLORES: 

- Negro con detalles en celeste y verde / rosa / rojo / amarillo / naranja / blanco

# TALLES:
- S (16")
- M (18")
- L (20")

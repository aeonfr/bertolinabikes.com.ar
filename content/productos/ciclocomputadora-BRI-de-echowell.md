---
title: "Ciclocomputadora BRI De Echowell"
date: 2017-11-20T21:01:40-03:00
description: "Ciclocomputadora ECHOWELL BRI 10."
cloudinary: "aeonfr"
imagen: "v1511222663/bertolina/ciclocomputadora-bri-ecowell.jpg"
weight: 2
categorias: ["accesorios"]
marcas: ["echowell"]
envio_gratis: false
gallery:
  - cid: "v1511222663/bertolina/ciclocomputadora-bri-ecowell.jpg"
    size: 800x800
  - cid: "v1511222794/bertolina/ciclocomputadora-bri-ecowell-02.jpg"
    size: 343x500
mercadolibre:
  - link: https://articulo.mercadolibre.com.ar/MLA-684379129-ciclocomputadora-velocimetro-bicicleta-bri-10-echowell-_JM
---

# Descripción
Ciclocomputadora ECHOWELL BRI 10

# Funciones

- Velocidad (Actual, Promedio Y Máxima)
- Distancia
- Odómetro
- Tiempo
- Reloj
- Autoscan
- Tiempo total andando
- Speed Pacer
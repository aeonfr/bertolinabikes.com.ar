---
title: "Casco color Rosa"
date: 2017-11-08T23:07:30-03:00
description: "Casco para bicicleta con visera. Apto para ciclismo de ruta, de montaña, o cicloturismo. Color rosa."
weight: -1
imagen: "casco-mujer-rosa (2).jpg"
categorias: ["seguridad", "indumentaria"]
envio_gratis: false
gallery:
  - file: "casco-mujer-rosa (1).jpg"
    size: 1200x900
  - file: "casco-mujer-rosa (2).jpg"
    size: 1200x900
  - file: "casco-mujer-rosa (3).jpg"
    size: 1200x900
---

# Descripción
Apto para ciclismo de ruta, de montaña, o cicloturismo.

* Super ligero
* Regulacion
* Correa ajustable
* Visera regulable
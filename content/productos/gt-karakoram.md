---
  title: "GT Karakoram 2018 Rodado 29 - 20 Vel (Deore) c/Bloqueo Remoto"
  imagen: "Karakoram M.jpg"
  marcas: 
    - "gt"
  categorias: 
    - "bicicletas"
  description: "Bicicleta para Mountain Bike con cuadro GT Karakoram"
  weight: -95
  mercadolibre_id: "721503773"
  date: "2018-04-26"
---


NUEVA GT Karakoram Rodado 29 Talles "S" a "XL" modelo 2018 

# Características
- Cuadro: Aluminio 6061 con frente conico
- Transmision Shimano Deore M6000 2x10 (11-42)
- Suspension Suntour XCM c/ bloqueo remoto
- Ruedas WTB Con mazas novatec
- Frenos hidraulicos SHIMANO Br 396
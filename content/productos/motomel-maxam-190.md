---
  title: "Teknial Motomel Maxam 190 Rodado 29 - Freno a Disco"
  weight: -28
  marcas: 
    - "teknial"
  categorias: 
    - "bicicletas"
  description: "Bicicleta para MTB con cuadro Teknial Motomel Maxam 190."
  imagen: "maxam.jpg"
  mercadolibre_id: "649917270"
  date: "2018-05-30"
---

Bicicleta para MTB con cuadro Teknial Motomel Maxam 190.

# CARACTERISTICAS:
- Rodado:29
- Cuadro: MAXAM 190 ALUMINIO
- Transmisión: 21 Velocidades Shimano
- Frenos: FRENOS A DISCO MECANICOS 
- Llantas: DOBLE PARED LIVIANAS DE ALUMINIO
- Mazas para discos
- Cubiertas: MTB 29x2.0
- Manubrio, Stem, Portasilla, Asiento en combinación con el color del cuadro
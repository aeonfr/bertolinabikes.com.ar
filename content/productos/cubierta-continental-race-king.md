---
  title: "Cubierta Continental Race King 29x2.0 Tubeless Ready"
  weight: -27
  imagen: "cubierta-race-king.jpg"
  mercadolibre_id: "689696010"
  description: "Cubierta para bicicletas de Mountain Bike. Marca Continental, Modelo Race King."
  marcas: 
    - "continental"
  categorias: 
    - "repuestos"
  date: "2018-06-06"
---


Cubierta Continental Rodado 29x2.0

- Aro de Kevlar
- Apta tubeless

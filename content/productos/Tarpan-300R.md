---
  title: "Teknial Tarpan 300r Rodado 29 Modelo 2018 24 Vel"
  date: "2018-08-07"
  mercadolibre_id: "741540289"
  imgUrl: true
  imagen: "http://mla-s2-p.mlstatic.com/741926-MLA27914467291_082018-F.jpg"
  marcas: 
    - "teknial"
  description: "Bicicleta Teknial Tarpan modelo 2018."
  categorias: ["bicicletas"]
  weight: -107
---

Bicicleta Teknial Tarpan 300R Rodado 29 modelo 2018

# Características

- Cuadro: de aluminio 6061 hidroformado 
- Sistema de cambios: SHIMANO ACERA 24V con shifters SHIMANO SL-M310
- Piñón: SHIMANO CS-HG200-8 12-32T
- Plato y palancas: SHIMANO FC-TY301 24/34/42T
- Frenos: A DISCO HIDRAULICOS levas SHIMANO FD-M190
- Horquilla: MODE MD-993AT-L/O/29
- Manubrio: MODE MD-HS127 680mm con elevación de aluminio negro
- Puños: de goma negros con puntas de color en combinación con el cuadro
- Avance/stem: aluminio Ahead MODE MD-HS127
- Mazas: de aluminio JOYTECH ejes con cierre rápido
- Llantas: de aluminio doble pared negras
- Cubiertas: KENDA 29 X 2.1
- Caja pedalera: FEIMIN FP-B902 
- Pedales: aluminio FEIMIN FP-917
- Cadena: KMC Z72
- Collar de asiento: aluminio con cierre
- Caño de asiento: de aluminio con grampa 30.9" x 300mm
- Asiento: Vader en colores combinados con la pintura del cuadro
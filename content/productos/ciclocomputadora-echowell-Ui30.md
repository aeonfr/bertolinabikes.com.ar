---
  title: "Ciclocomputadora Ui30 Echowell con Cadencia, Temp, Altimetro"
  imagen: "echowell.jpg"
  mercadolibre_id: "622552621"
  marcas: 
    - "echowell"
  categorias: 
    - "accesorios"
  description: "Ciclocomputadora Ui30 Echowell Con Cadencia, Temp, Altimetro"
  weight: -10
  date: "2016-06-01"
---


# CARACTERÍSTICAS

Funciones bicicleta:
- Velocidad
- Velocidad promedio
- Velocidad maxima
- Distancia de la salida
- Odometro
- Odometro total (Bici 1 + Bici 2)
- Tiempo de la salida
- Tiempo total
- Tiempo total (Bici 1 + Bici 2)
- Mantenimiento
- Control de velocidad (Speed Pacer)

Funciones de C02

- C02 ahorrado en la salida
- C02 total ahorrado
- C02 total ahorrado (Bici 1 + Bici 2)

Funciones de RPM

- RPM Actual
- RPM Promedio
- RPM Maximo
- Trip Pedal Revolution
- Limite de RPM
- Total Pedal Revolution
- Total Pedal Revolution (Bici 1 + Bici 2)
- Control de RPM (RPM Limit Pacer)

Otras Funciones:

- Cronometro
- Velocidad (Promedio/Máxima)
- Distancia de la salida
- RPM (Promedio/Máxima)
- Pedal Revolution
- Ahorro de CO2
- Temperatura (Máxima/Mínima)
- Información de las vueltas
- Temperatura en el momento
- Temperatura máxima
- Temperatura mínima
- Reloj
- Calendario
- Detecta automáticamente Bici 1 y Bici 2
- Start/Stop Automáticamente
- Recopila información

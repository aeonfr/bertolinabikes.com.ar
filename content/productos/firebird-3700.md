---
  title: "Firebird Rodado 29 - 21 vel Disco Hidráulico"
  weight: -118
  marcas: 
    - "fire-bird"
  categorias: 
    - "bicicletas"
  imagen: "firebird21v.jpg"
  mercadolibre_id: "642109943"
  description: "Bicicleta para Mountain Bike con cuadro Firebird y componentes Shimano Tourney 21 velocidades. Frenos a disco hidráulicos mecánicos"
  date: "2018-05-30"
  lowest_price_multiplier: 0.95
---


# CARACTERISTICAS:
- Rodado: 29
- Cuadro: Aluminio con cableado interno
- Frente: 1 1/8
- Grupo: SHIMANO Tourney 21 Velocidades 
- Frenos: Frenos a disco hidráulicos mecanicos
- Suspensión: SUNTOUR 3030 con regulacion
- Llantas: doble pared WTB
- Cubiertas: 29x2.0 de alambre

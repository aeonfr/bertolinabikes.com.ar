---
  title: "Piñon Shimano Xt M8000 - 11 Vel"
  imgurl: true
  imagen: "http://mla-s2-p.mlstatic.com/833079-MLA27352504471_052018-F.jpg"
  mercadolibre_id: "724532791"
  marcas: 
    - "shimano"
  categorias: 
    - "componentes"
  description: "Piñon Shimano Xt M8000 11 Velocidades 11-46 Dientes"
  weight: -10
  date: "2018-05-12"
---


Piñon Shimano Xt M8000 11 Velocidades 11-46 Dientes

*Estamos trabajando en la descripcion de este producto, podes dejarnos tu pregunta y te ayudaremos a resolver tus dudas*
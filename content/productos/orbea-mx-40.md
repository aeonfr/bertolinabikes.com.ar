---
  title: "Orbea Mx 40 Rodado 29 - 27 Vel Disco Hidráulico"
  imagen: "mx40.jpg"
  marcas: 
    - "orbea"
  categorias: 
    - "bicicletas"
  description: "Bicicleta para Mountain Bike Orbea MX 40 - rodado 29 - frenos hidráulicos - 27 velocidades "
  weight: -99
  mercadolibre_id: "641215229"
  date: "2018-05-30"
---



Bicicleta Orbea MX 40 - rodado 29 - frenos hidráulicos - 27 velocidades

# Características
- CUADRO Orbea MX Alu only disc
- HORQUILLA SR Suntour XCM RL-Coil 100mm QR
- PLATO Shimano MT100 22x30x40t
- DIRECCION 1-1/8" Semi-Integrated
- MANILLAR Orbea OC-I Riser 720mm
- POTENCIA Orbea OC-II
- MANETAS Shimano Altus M2000
- FRENO Shimano M315 Hydraulic Disc
- PIÑON Shimano HG200 11-36 9-Speed
- CAMBIO Shimano Deore M592 Shadow
- DESVIADOR Shimano Altus M2000 31,8mm
- CADENA Shimano HG53 9-Speed
- RUEDA Orbea Black Rock 23c Disc
- CUBIERTA Kenda K1153 2,35" 30TPI
- PEDALES VP-536 Black
- CAÑO DE ASIENTO Orbea OC-I 27.2x400mm
- ASIENTO Selle Royal 2062 HRN

---
  title: "Venzo Stinger Rodado 29 - 27 Vel Disco Hidráulico"
  description: "Bicicleta para Mountain Bike con cuadro de aluminio y componentes Shimano. Marca: Venzo. Modelo: Stinger."
  imagen: "Stinger27v.jpg"
  weight: -116
  categorias: 
    - "bicicletas"
  marcas: 
    - "venzo"
  gallery: 
    - 
      file: "Stinger27v.jpg"
      size: "1200x800"
    - 
      file: "Stinger27v-2.jpg"
      size: "1200x874"
    - 
      file: "Stinger27v-3.jpg"
      size: "1200x879"
    - 
      file: "venzo-stinger-rodado-29-hidraulico-27-velocidades-01.jpg"
      size: "1200x900"
      description: "Venzo Singer rodado 29, 27 velocidades Shimano Altus - Color Naranja"
    - 
      file: "venzo-stinger-rodado-29-hidraulico-27-velocidades-02.jpg"
      size: "1200x900"
      description: "Venzo Singer rodado 29, 27 velocidades Shimano Altus - Color Celeste"
    - 
      file: "venzo-stinger-rodado-29-hidraulico-27-velocidades-03.jpg"
      size: "1200x900"
      description: "Venzo Singer rodado 29, 27 velocidades Shimano Altus - Color Rojo"
    - 
      file: "venzo-stinger-rodado-29-hidraulico-27-velocidades-04.jpg"
      size: "900x1200"
      description: "Venzo Singer rodado 29, 27 velocidades Shimano Altus - Color Rojo - Detalle manubrio"
    - 
      file: "venzo-stinger-rodado-29-hidraulico-27-velocidades-05.jpg"
      size: "1200x900"
      description: "Venzo Singer rodado 29, 27 velocidades Shimano Altus - Color Rojo - Detalle corona"
    - 
      file: "venzo-stinger-rodado-29-hidraulico-27-velocidades-06.jpg"
      size: "1200x900"
      description: "Venzo Singer rodado 29, 27 velocidades Shimano Altus - Color Celeste - Diseño del cuadro"
    - 
      file: "venzo-stinger-rodado-29-hidraulico-27-velocidades-07.jpg"
      size: "1200x900"
      description: "Venzo Singer rodado 29, 27 velocidades Shimano Altus - Color Verde - Diseño del cuadro"
    - 
      file: "venzo-stinger-rodado-29-hidraulico-27-velocidades-08.jpg"
      size: "1200x900"
      description: "Venzo Singer rodado 29, 27 velocidades Shimano Altus - Color Verde"
  date: "2017-11-08"
  mercadolibre_id: "680687710"
  lowest_price_multiplier: 1
---


# Características
- Cuadro: STINGER ALUMINIO LIVIANO
- Transmisión: FULL SHIMANO 27 velocidades
- Frenos: FRENOS A DISCO HIDRAULICOS SHIMANO
- Suspensión: SUNTOUR XCT/XCM o RST BLAZE (sujeto a disponiblidad del proveedor)
- Llantas: WTB DOBLE PARED LIVIANAS
- Mazas a ruleman
- Cubiertas: WTB 29x2.25
- Manubrio, Stem, Portasilla, Asiento en combinación con el color del cuadro

# Colores
- Celeste
- Rojo
- Naranja
- Verde

# Talles
- 16 "S"
- 18 "M"
- 20 "L"

# Rodados
- 29
- Consultar por otros rodados
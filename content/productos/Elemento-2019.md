---
  title: "Cuadro Venzo Elemento Rodado 29 Conico Eje 12 Mm Boost"
  date: "2018-08-07"
  mercadolibre_id: "737629076"
  imgUrl: true
  imagen: "http://mla-s1-p.mlstatic.com/787555-MLA27778869972_072018-F.jpg"
  marcas: 
    - "venzo"
  description: "Nuevo cuadro Venzo Elemento - Para Mountain Bike"
  categorias: ["componentes"]
  weight: -50
---

Nuevo cuadro Venzo Elemento 2019 

# Características

- Rodado: 29
- Frente: Conico
- Eje: 12mm x 148mm (BOOST)
- Cableado interno
- Peso: 1,7kg (talle M)
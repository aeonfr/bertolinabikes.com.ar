---
title: "Horquilla Suspension Rock Shox Thora"
date: 2017-10-30T19:06:02-03:00
description: "Horquilla de suspensión para rueda delantera bicicleta MTB. Marca: Rock Shox. Modelo: Thora."
imagen: "horquilla-rock-shox-thora-r26-02.jpg"
categorias: ["componentes"]
marcas: ["rock shox"]
envio_gratis: true
mercadolibre: 
  - link: "https://articulo.mercadolibre.com.ar/MLA-647139522-horquilla-suspension-rock-shox-tora-rodado-26-mtb-_JM"
    titulo: "Horquilla Suspension Rock Shox Tora Rodado 26"
gallery:
  - file: "horquilla-rock-shox-thora-r26-01.jpg"
    size: "900x1200"
    description: "Horquilla Rock Shox Thora rodado 26"
  - file: "horquilla-rock-shox-thora-r26-02.jpg"
    size: "900x790"
    description: "Horquilla Rock Shox Thora rodado 26"
weight: -1
---

# Descripción
Horquilla de suspensión para rueda delantera para bicicleta MTB.

# Características
- Marca: Rock Shox
- Modelo: Thora
- Peso: 2,2kg
- Tipo: Aceite
- Bloqueo
- Regulación
- Control de rebote 
- Poste cónico (1 1/2)

# Rodados
- 26
- Consultar por otros rodados

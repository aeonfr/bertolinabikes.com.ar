---
  title: "Teknial Tarpan 100r Rodado 29 21 Velocidades Disco"
  date: "2018-08-07"
  mercadolibre_id: "741517119"
  imgUrl: true
  imagen: "http://mla-s2-p.mlstatic.com/927600-MLA27914032667_082018-F.jpg"
  marcas: 
    - "teknial"
  description: "Bicicleta Teknial Tarpan Rodado 29 - Especial para Mountain Bike.. Modelo 2018."
  categorias: ["bicicletas"]
  weight: -106
---

Bicicleta Teknial Tarpan 100R Rodado 29 modelo 2018

# Características

- Cuadro: de aluminio 6061 hidroformado 
- Sistema de cambios: SHIMANO TOURNEY 21V con shifters SHIMANO ST-EF51
- Piñón: SHIMANO CS-HG200-8 12-32T
- Plato y palancas: SHIMANO FC-TY301 24/34/42T
- Frenos: A DISCO MECANICOS levas SHIMANO FD-TZ31
- Horquilla: MODE MD-711X-D-29
- Manubrio: MODE MD-HB023 680mm
- Puños: de goma negros con puntas de color en combinación con el cuadro
- Avance/stem: aluminio Ahead MODE MD-HS078
- Mazas: acero 32H
- Llantas: de aluminio doble pared 32H
- Cubiertas: 29 X 2.125
- Cadena: KMC Z33
- Collar de asiento: aluminio con cierre
- Caño de asiento: de aluminio con grampa 27.2" x 300mm
- Asiento: Vader antiprostatico MTB
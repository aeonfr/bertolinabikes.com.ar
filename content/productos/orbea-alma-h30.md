---
  title: "Orbea Alma H30 Rodado 29 - 22 Vel Rockshox SLX"
  imagen: "alma-h30.jpg"
  marcas: 
    - "orbea"
  categorias: 
    - "bicicletas"
  description: "Bicicleta para MTB Orbea Alma Hydro 2018 con horquilla RockShox."
  weight: -99
  mercadolibre_id: "687200614"
  date: "2018-05-30"
  promo: "casco-orbea"
---


# Características
- CUADRO Orbea Alma Hydro 2018 Triple Butted 4X4 Taper PF Race
- HORQUILLA RockShox 30 Silver TK 100 Air Remote
- PLATO OC-Custom PW Vortex-501-TT 22x36t
- DIRECCION FSA 1-1/8 - 1-1/2" Integrated
- MANILLAR Orbea OC-II Flat 720mm 
- POTENCIA Orbea OC-II
- MANETAS Shimano Deore M6000
- FRENO Shimano M365 Hydraulic Disc 
- PIÑON Shimano HG50 11-36t 10-Speed
- CAMBIO Shimano SLX M670-D SGS Shadow Direct Mount
- DESVIADOR Shimano Deore M6020 31,8mm
- CADENA Fsa Team Issue
- RUEDA Mach1 Traxx 21c Tubeless Ready
- CUBIERTA Maxxis Ikon 2.20" FB 60 TPI Dual
- PEDALES N/A
- TIJA SILLIN Orbea OC-II 27.2x400mm
- SILLIN Selle Royal Seta Sport

# Rodados
- 29
- Consultar por otors rodados
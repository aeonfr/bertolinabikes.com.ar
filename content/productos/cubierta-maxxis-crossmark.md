---
title: "Cubierta Maxxis Crossmark"
date: 2017-10-30T14:53:39-03:00
description: "Cubierta para bicicleta apta para practicar Mountain Bike. Marca: Maxxis. Modelo: Crossmark."
imagen: "maxxis crossmark (1).jpg"
weight: 10
categorias: ["componentes"]
marcas: ["maxxis"]
envio_gratis: false
mercadolibre_id: "675062166"
gallery:
  - file: "maxxis crossmark (1).jpg"
    size: "900x1200"
    description: "Cubierta Maxxis Crossmark 27.5x2.25"
  - file: maxxis crossmark (2).jpg
    size: "900x1200"
    description: "Cubierta Maxxis Crossmark 27.5x2.10"
  - file: "cubiertas-maxxis-crossmark-29x225-tubeless-ready-exo.jpg"
    size: "900x1200"
    description: "Cubierta Maxxis Crossmark 29x2.25 Tubeless Ready Exo"
---

Consultar los rodados disponibles.

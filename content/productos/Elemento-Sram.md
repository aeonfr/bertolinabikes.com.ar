---
  title: "Bicicleta Venzo Elemento Rodado 29 Sram 1x11 Suntour Raidon"
  date: "2018-08-07"
  mercadolibre_id: "628317809"
  imgUrl: true
  imagen: "http://mla-s2-p.mlstatic.com/815199-MLA27915597208_082018-F.jpg"
  marcas: 
    - "Venzo"
  description: "Bicicleta con cuadro Venzo Elemento, rodado 29."
  categorias: ["bicicletas"]
  weight: -99
---

Bicicleta Venzo ELEMENTO Rodado 29 

# Caracteristicas:
- Cuadro de Aluminio 7005
- Peso cuadro: 1,7kg
- Frente: Conico 
- 11 Velocidades SRAM NXI
- Frenos a disco Hidraulicos Shimano
- Suspension de aire con bloqueo remoto Suntour Raidon
- Llantas doble pared de aluminio WTB
- Mazas a rulemanes
- Cubiertas Maxxis Crossmark 29x2.1
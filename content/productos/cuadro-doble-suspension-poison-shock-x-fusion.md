---
draft: true
title: "Cuadro Doble Suspensión Poison Rubidium"
date: 2017-12-03T23:06:57-03:00
description: "Cuadro Poison Rubidium Shox X-Fusion E1. Aluminio 6061. Rodado 29 (consultar por otros rodados)."
cloudinary: "aeonfr"
imagen: "v1512353847/bertolina/cuadro-doble-suspension-rodado-29-poison-shock-x-fusion-1.jpg"
weight: 2
categorias: ["componentes"]
marcas: ["Poison"]
mercadolibre:
  - link: https://articulo.mercadolibre.com.ar/MLA-687526002-cuadro-doble-suspension-rodado-29-poison-shock-x-fusion-_JM
gallery:
  - cid: v1512353847/bertolina/cuadro-doble-suspension-rodado-29-poison-shock-x-fusion-1.jpg
    size: 1200x598
    description: "Cuadro doble suspensión rodado 29 Poison Shock X-Fusion"
  - cid: v1512353847/bertolina/cuadro-doble-suspension-rodado-29-poison-shock-x-fusion-2.jpg
    size: 1200x593
    description: Detalles del cuadro.
---

# Descripción

Cuadro doble suspension Poison Rubidium

- Shox X-Fusion E1
- Aluminio 6061
- Rodado 29 (consultar por otros rodados)
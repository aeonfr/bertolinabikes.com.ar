---
  title: "Scott Aspect 950 Rodado 29 24 Vel Disco Hidraulico"
  date: "2018-08-07"
  mercadolibre_id: "740924085"
  imgUrl: true
  imagen: "http://mla-s2-p.mlstatic.com/971711-MLA27894457241_082018-F.jpg"
  marcas: 
    - "Scott"
  description: "Scott Aspect 950"
  categorias: ["bicicletas"]
  weight: -99
---

Bicicleta Scott Aspect 950

# Características
- Cuadro: Aluminio
- Horquilla: 100mm Suntour XCT-HLO
- Descarrilador trasero: Shimano Altus RD-M2000 24 speed
- Plato y palanca: Shimano FC-TY701 42x34x24 w/CG
- Descarrilador delantero: Shimano FD-TY700-TS6 / 31.8mm
- Shifters: Shimano STEF 505 8R
- Piñon: Shimano CS-HG31-8 / 11-32T
- Cadena: KMC Z-7
- Frenos: Shimano
- Aros: Syncros X-20 Disc 32H / black
- Stem: Syncros M3.0 / 7° / Black
- Manubrio: Syncros M3.0 / 720mm black / 31,8mm / 12mm rise / 9° BS
- Portasilla: Syncros M3.0 27.2mm / 350mm / Black
- Juego de dirección: Ritchey LOGIC ZERO-OE
- Caja pedalera: Shimano BB-UN-100 Cartridge Type
- Asiento: Syncros M3.0
- Pedales: VP VP-536
- Velocidades: Shimano Altus, 24 velocidades
- Peso: 14.30 kg
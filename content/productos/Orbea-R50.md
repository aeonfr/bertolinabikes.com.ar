---
  title: "Casco Orbea R50 Ultra Liviano"
  date: "2018-08-07"
  mercadolibre_id: "731434908"
  imgUrl: true
  imagen: "http://mla-s1-p.mlstatic.com/930817-MLA27676910050_072018-F.jpg"
  marcas: 
    - "Orbea"
  description: "Casco para ciclismo MTB o Ruta - Orbea R50 ultra liviano"
  categorias: ["indumentaria", "seguridad"]
  weight: -99
---

Casco para ciclismo MTB o Ruta - Orbea R50 ultra liviano
---
  title: "Scott Aspect 960 Rodado 29 21 Vel Disco Hidraulico"
  date: "2018-08-07"
  mercadolibre_id: "740924199"
  imgUrl: true
  imagen: "http://mla-s2-p.mlstatic.com/763996-MLA27894453382_082018-F.jpg"
  marcas: 
    - "Scott"
  description: "Bicicleta Scott Aspect 960"
  categorias: ["bicicletas"]
  weight: -99
---

# Características
- Bicicleta Scott Aspect 960 modelo 2018
- Cuadro: Aluminio
- Horquilla: Suntour XCT 100mm travel
- Plato: Shimano FC-TY501 42x34x24 w/CG
- Dirección: Ritchey LOGIC ZERO-OE
- Manillar: HL MTB-AL-312BT / 720mm / black / 12mm rise
- Potencia: TDS-C302-8FOV / 10° / 31.8 / Black
- Caja: Shimano BB-UN-100 Cartridge Type
- Manetas de cambio: Shimano SL-M310-7R R-fire plus
- Manetas de freno: Tektro SCH-F17
- Frenos: Tektro SCH-F17 / Hydr. Disc 160F/160R Rotor
- Piñón: Shimano MF-TZ 21-7 / 14-28T
- Descarrilador Delantero: Shimano FD-TY500-TS6 / 31.8mm
- Descarrilador Trasero: Shimano Tourney RD-TY300 21 Speed
- Maza delantera: Formula DC-19 FQR disc
- Maza trasera: Formula DC-31 RQR disc
- Cadena: KMC Z50
- Pedales: VP VP-536
- Rueda: Syncros X-20 Disc 32H / black
- Cubierta: Kenda Slant 6 2.35 / 30TPI
- Tija Sillín: HL SP C212 27.2mm / 350mm / Black
- Sillín: Syncros M3.0
- Peso: 14,5 kilos
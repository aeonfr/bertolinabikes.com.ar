---
  title: "Casco R10 Tope De Gama"
  date: "2018-08-07"
  mercadolibre_id: "731434898"
  imgUrl: true
  imagen: "http://mla-s1-p.mlstatic.com/862907-MLA27559198134_062018-F.jpg"
  marcas: 
    - "Orbea"
  description: "Casco Orbea R10 para Ciclismo MTB o de Ruta."
  categorias: ["indumentaria", "seguridad"]
  weight: -99
---

Casco Orbea R10 para Ciclismo MTB o de Ruta.
---
  title: "Casco Bluegrass Golden Eyes"
  imgurl: true
  imagen: "http://mla-s1-p.mlstatic.com/645056-MLA27352506582_052018-F.jpg"
  mercadolibre_id: "724532964"
  marcas: 
    - "bluegrass"
  categorias: 
    - "indumentaria"
    - "seguridad"
  description: "Casco de Bicicleta con visera y ventilación para Mountain Bike (Enduro). Marca: Bluegrass."
  weight: -10
  date: "2018-05-12"
---


Casco de Bicicleta con visera y ventilación para Mountain Bike (Enduro).

*Estamos trabajando en la descripción de este producto, podes dejarnos tu pregunta y te ayudaremos a resolver tus dudas*
---
  title: "Venzo Talon Rodado 29 - 24 vel"
  description: "Bicicleta para Mountain Bike con cuadro de aluminio 6061 y componentes de Shimano. Marca: Venzo. Modelo: Talón."
  imagen: "talon24vel.jpg"
  weight: -105
  categorias: 
    - "bicicletas"
  marcas: 
    - "venzo"
  date: "2017-10-30"
  mercadolibre_id: "649915949"
  lowest_price_multiplier: 1
---


# CARACTERISTICAS:
- Rodado: 29
- Cuadro: TALON ALUMINIO LIVIANO
- Transmisión: FULL SHIMANO 24 velocidades
- Frenos: FRENOS A DISCO HIDRAULICOS SHIMANO
- Suspensión: SUNTOUR 3030 
- Llantas: DOBLE PARED LIVIANAS
- Mazas a ruleman
- Cubiertas: MTB 29x2.0
- Manubrio, Stem, Portasilla, Asiento en combinación con el color del cuadro

# COLORES: 
- Negro con detalles en blanco y verde / naranja
- Blanco con detalles en azul

# TALLES:
- S (16")
- M (18")
- L (20")

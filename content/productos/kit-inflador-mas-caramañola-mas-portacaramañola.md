---
title: "Kit Inflador + Caramañola + Portacaramañola"
date: 2017-11-20T19:20:04-03:00
description: "Kit de Inflador + Caramañola + Portacaramañola (aluminio o plastico)."
cloudinary: "aeonfr"
imagen: "v1511216693/bertolina/kit-inflador-caramanola-portacaramanola.jpg"
weight: 2
categorias: ["accesorios"]
mercadolibre:
  - link: https://articulo.mercadolibre.com.ar/MLA-683866753-kit-inflador-caramanola-portacaramanola-bici-spinning-_JM
    titulo: Kit Inflador + Caramañola + Portacaramañola Bici, spinning
gallery:
  - cid: v1511216693/bertolina/kit-inflador-caramanola-portacaramanola.jpg
    size: 1200x900
---

# Descripción
Kit de Inflador + Caramañola + Portacaramañola (aluminio o plastico).

Incluye tornillos para colocar en el cuadro.

Varias opciones de colores.
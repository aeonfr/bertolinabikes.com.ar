---
title: "Juego de Direccion Para Bici a Rulemanes Cónico"
date: 2017-11-09T00:52:07-03:00
description: "Juego de direccion a rulemanes para 1 1/8 - 1 1/2. Incluye pista para horquilla."
imagen: "juego-para-rulemanes.jpg"
categorias: ["accesorios"]
mercadolibre:
  - link: https://articulo.mercadolibre.com.ar/MLA-678984176-juego-de-direccion-para-bici-a-rulemanes-conico-1-18-1-12-_JM
gallery:
  - file: "juego-para-rulemanes.jpg"
    size: 600x300
weight: 10
---

# Descripción
Juego de direccion a rulemanes para 1 1/8 - 1 1/2.
Incluye pista para horquilla.
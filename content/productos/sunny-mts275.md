---
  title: "Sunny MTS Rodado 27.5 - 21 Vel Disco Mecánico"
  marcas: 
    - "teknial"
  categorias: 
    - "bicicletas"
  description: "Bicicleta para MTB con cuadro Teknial Sunny MTS 275."
  weight: -28
  imagen: "sunny.jpg"
  mercadolibre_id: "619664286"
  date: "2018-05-30"
  lowest_price_multiplier: 0.95
  promo: "casco"
---

Bicicleta para MTB con cuadro Teknial Sunny MTS 275.

# CARACTERISTICAS:
- Rodado:27.5
- Talle: "M" 
- Cuadro: SUNNY MTS 275 de alumino
- Transmisión: 21 velocidades
- Frenos: Frenos a disco mecanicos
- Suspensión Sunny
- Llantas: Deoble pared de aluminio
- Cubiertas: MTB 27.5x2.0
- Manubrio, Stem, Portasilla, Asiento en combinación con el color del cuadro

# COLORES: 
- Negro con amarillo

# TALLES:
- S (16")
- M (18") 

---
  title: "Scott Scale 980 Rodado 29 - 1x11 Sram Nx - Rock Shox"
  date: "2018-08-16"
  mercadolibre_id: "741668206"
  imgUrl: true
  imagen: "http://mla-s1-p.mlstatic.com/694280-MLA27918727517_082018-F.jpg"
  marcas: 
    - "scott"
  description: "Bicicleta para Mountain Bike Scott Scale 980, con horquilla Rock Shox y cambio Sram NX1"
  categorias: ["bicicletas"]
  weight: -99
---

Bicicleta SCOTT Scale 980 Rodado 29 modelo 2018

# Características

- Cuadro: Scale Alloy 6061 Custom Butted Tubing
- Tapered HT / BB92 / Bridgeless Seatstays
- Internal Cable Routing / replaceable hanger
- Horquilla: Rock Shox 30 Silver TK Solo Air Tapered Steerer / Reb. Adj. Remote Lockout / 100mm travel
- Sistema de bloqueo remoto: SCOTT RideLoc Remote
- Jgo Direccion: Syncros OE Press Fit / Tapered 1.5" - 1 1/8" OD 50/61mm / ID 44/55mm
- Cambio: Sram NX1 / 11 Speed
- Shifters: Sram NX1 Trigger
- Frenos: Shimano M365 Disc 180/F and 160/Rmm SM-RT10 CL Rotor
- Plato Palanca: Sram NX1 GXP PF 30T
- Caja pedalera: Sram GXP PF integrated / shell 41x89.5mm
- Manubrio: Syncros T-Bar / Alloy 6061 T shape Flat / 9° / 720mm
- Stem: Syncros 6061 Alloy oversize 31.8mm / 1 1/8" / 6° angle
- Pedales: Wellgo M-21
- Portasilla: Syncros / 31.6x400mm
- Asiento: Syncros XR2.5
- Maza delantera: Formula CL51
- Maza trasera: Shimano FH-RM35 CL
- Cadena: Sram PC1110
- Piñones: Sram PG1130 / 11-42 T
- Rayos: Stainless Black 15G / 1.8mm
- Llantas: Syncros X-20 / 32H
- Cubiertas: Maxxis Ikon / 2.2 / 60TPI
- Peso aproximado en KG: 12.60
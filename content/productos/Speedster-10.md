---
  title: "Scott Speedster 10 Ruta Rodado 28 Talle 54 "
  date: "2018-08-07"
  mercadolibre_id: "740923146"
  imgUrl: true
  imagen: "http://mla-s1-p.mlstatic.com/629228-MLA27894386393_082018-F.jpg"
  marcas: 
    - "Scott"
  description: "Bicicleta Scott Speedster rodado 28. Para MTB, con cuadro de aluminio 6061"
  categorias: ["bicicletas"]
  weight: -99
---

Bicicleta SCOTT Speedster

# Características

- Cuadro: Aluminio 6061 D.Butted Alloy 
- Endurance geometry / Headtube integrado, cableado interno
- Horquilla Speedster Carbono 1 1/8 "-1 1/4" Dirección de carbono
- Juego de direccion integrado
- CAMBIO TRASERO Shimano 105 Black RD-5800-GS 
- 22 Velocidad
- DESCARRILADOR DELANTERO Shimano 105 negro FD-5801
- Manijas Shimano 105 Black ST-5800 Dual control 22 Velocidades
- FRENOS Tektro SCBR-525 
- Palancas Shimano FC-RS510 Black Hyperdrive 50x34 T
- BB-SET Shimano BB-RS500-PB
- Manubrio Syncros RR2.0 Comfort 31.8mm
- Stem Syncros RR2.0 31.8mm
- Portasilla Syncros RR2.5 27.2 / 350mm
- Asiento Syncros FL2.5
- Maza (DELANTERA) Equipo de Fórmula 24 H
- Maza (TRASERA) Equipo de Fórmula 28 H
- CADENA KMC X11
- Piñon Shimano CS-5800 11 Velocidad 11-32 T
- RAYOS SHA-DAR Negro 2mm
- LLANTAS Syncros Race 22 
- 24 Delantera / 28 Trasera
- LLANTAS Golondrina Lugano 700x28C

# Peso
- PESO APROXIMADO EN KG 8.85
- PESO APROXIMADO EN LBS 19.51
- PESO DEL SISTEMA 120 kg 
- El peso total incluye la bicicleta, el ciclista, el equipo y posible equipaje adicional.
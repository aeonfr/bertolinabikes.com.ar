---
title: "Luces Silicona"
date: 2017-11-08T20:23:33-03:00
description: "Juego de luces LED de silicona para bicicletas de todo tipo. Se adaptan a varios tipos de cuadro."
imagen: "juego-luces.jpg"
weight: -1
categorias: ["accesorios", "seguridad"]
envio_gratis: false
mercadolibre:
  - link: https://articulo.mercadolibre.com.ar/MLA-685076638-juego-de-luces-led-silicona-para-bicicleta-_JM
    titulo: Juego de luces LED silicona para bicicleta
gallery:
  - file: juego-luces.jpg
    size: 749x538
    description: Juego de Luces
---

Juego de luces LED de silicona para bicicletas de todo tipo. Se adaptan a varios tipos de cuadro.

Incluye dos luces (delantera y trasera).
-Luz delantera color blanco
-Luz trasera color rojo

Silicona de color negro, naranja o verde (sujeto a stock disponible).
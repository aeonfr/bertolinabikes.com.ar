---
  title: "GT Avalanche 2018 Rodado 29 - 27 Vel Disco Hidráulico"
  description: "Bicicleta para Mountain Bike GT Avalanche."
  imagen: "Avalanche M.jpg"
  weight: -95
  mercadolibre_id: "616452562"
  marcas: 
    - "gt"
  categorias: 
    - "bicicletas"
  date: "2018-05-30"
---


# CARACTERISTICAS:
- Rodado:29
- Cuadro: GT AVALANCHE 
- Transmisión: SHIMANO Alivio-Altus 27 velocidades
- Frenos: FRENOS A DISCO HIDRAULICOS SHIMANO
- Suspensión: SUNTOUR XCM con bloqueo 
- Llantas: WTB SX19 doble pared
- Mazas a ruleman NOVATEC
- Cubiertas: MAXXIS CROSSMARK 29x2.1

# TALLES:
- "M" (17)
- "L" (19)

# También disponible
- En [24 velocidades](/productos/avalanche24vel).
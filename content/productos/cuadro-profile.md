---
  title: "Cuadro de Carbono Profile Rodado 29 Talles 15-17-19"
  marcas: 
    - "profile"
  categorias: 
    - "componentes"
  description: "Cuadro de Carbono PROFILE para bicicleta de MTB."
  weight: -10
  imagen: "cuadro-profile.jpg"
  mercadolibre_id: "724532493"
  date: "2018-05-30"
---


Cuadro de carbono marca PROFILE

# Características
- Rodado: 29
- Frente: Conico
- Caja: Press fit (no incluye)
- Eje trasero: 12mmx142 (no incluye)
- Medida portasilla: 27.2
- Peso: 1,3kg
- Tiro descarrilador: Frontal
---
  title: "Scott Aspect 970 Rodado 29 Frenos A Discos 21vel"
  date: "2018-08-07"
  mercadolibre_id: "740923970"
  imgUrl: true
  imagen: "http://mla-s1-p.mlstatic.com/639167-MLA27894440349_082018-F.jpg"
  marcas: 
    - "Scott"
  description: "Bicicleta Scott Aspect 970"
  categorias: ["bicicletas"]
  weight: -99
---

BICICLETA SCOTT ASPECT 970 RODADO 29

# Características
- Cuadro: Aspect 900 series Alloy 6061 / Performance geometry
- Horquilla: Suntour XCT 100mm travel
- Plato: Shimano FC-TY501 42x34x24 w/CG
- Dirección: Ritchey LOGIC ZERO-OE
- Manillar: HL MTB-AL-312BT / 720mm / black / 12mm rise
- Potencia: TDS-C302-8FOV / 10° / 31.8 / Black
- Caja: Shimano BB-UN-100 Cartridge Type
- Manetas: Shimano ST-EF 41 L / 7R EZ-fire plus
- Frenos: Tektro SCM-02 mech. Disc 160F/160R Rotor
- Piñón: Shimano MF-TZ 21-7 / 14-28T
- Descarrilador Delantero: Shimano FD-TY500-TS6 / 31.8mm
- Descarrilador Trasero: Shimano Tourney RD-TY300 21 Speed
- Maza delantera: Formula DC-19 FQR disc
- Maza trasera: Formula DC-31 RQR disc
- Cadena: KMC Z50
- Pedales: VP VP-536
- Rueda: Syncros X-20 Disc 32H / black
- Cubierta: Kenda Slant 6 2.35 / 30TPI
- Tija Sillín: HL SP C212 27.2mm / 350mm / Black
- Sillín: Syncros M3.0
- Peso: 14,5 kilos
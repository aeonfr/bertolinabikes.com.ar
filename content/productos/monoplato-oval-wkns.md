---
  title: "Monoplato Oval WKNS"
  imgurl: true
  imagen: "http://mla-s2-p.mlstatic.com/967442-MLA27352504987_052018-F.jpg"
  mercadolibre_id: "724532885"
  marcas: 
    - "oval"
  categorias: 
    - "componentes"
  description: "Monoplato para bicicletas para Mountain Bike"
  weight: -10
  date: "2018-05-12"
---


Monoplato Oval Wkns Aluminio 34 Dietnes Bcd 104 Shimano Comp

*Estamos trabajando en la descripción de este producto, podes [dejarnos tu pregunta](/contacto) y te ayudaremos a resolver tus dudas*
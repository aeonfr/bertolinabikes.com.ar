---
  title: "Scott Scale 925 Rod 29 Carbono Sram Eagle 1x12 Fox"
  date: "2018-08-07"
  mercadolibre_id: "726821594"
  imgUrl: true
  imagen: "http://mla-s1-p.mlstatic.com/799336-MLA27424527503_052018-F.jpg"
  marcas: 
    - "Scott"
  description: "BICICLETA SCOTT SCALE SCALE 925 CARBONO - Para ciclismo MTB"
  categorias: ["bicicletas"]
  weight: -99
---

BICICLETA SCOTT SCALE SCALE 925 CARBONO - DISPONIBLE EN TALLE "M"

# Características

- CUADRO:Scale 3 Carbon / IMP technology / MF BB92 / SW DM dropouts for Boost 12x148mm SDS2 advanced Shock & Standing Damping System ICCR Cable routing
- SUSPENSION: FOX 32 Float Rhythm Grip 3-Modes / 15x110mm QR axle / tapered steerer Reb. Adj. / Lockout / 100mm travel
- SISTEMA REMOTO: SCOTT RideLoc Technology / below bar remote 3 modes / integ. Grip clamp
- CAMBIO: Sram GX / Eagle 12 Speed
- SHIFTERS: Sram GX Trigger
- FRENOS: Shimano MT500 Disc
- DISCOS: 180/F and 160/R mm SM-RT54 CL Rotor
- PALANCAS: Sram X1 1000 Eagle GXP Boost PF QF 168 / 32T
- CAJA PEDALERA: BB-SET Sram GXP PF integrated / shell 41x89.5mm
- MANUBRIO: Syncros FL2.0 T-Bar / Alloy 6061 T shape Flat / 9° / 720mm
- PUÑOS: Syncros Pro lock-on grips
- STEM: Syncros FL2.0 6° / integrated Spacer & Top Cup 6061 Alloy / 31.8mm / 1 1/8"
- PORTASILLA: Syncros FL2.0 / 10mm offset 31.6 x 400mm
- ASIENTO: Syncros XR2.0 / CROM rails
- MAZAS: (FRONT) Formula CL811 / 15x110mm (REAR) Formula CL14811 / Boost 12x148mm / XD
- CADENA: Sram CN GX Eagle
- PIÑON: Sram GX / XG1275 / 10-50 T
- RAYOS: Stainless Black 15G / 1.8mm
- LLANTAS: Syncros X-25 / 32H / 25mm Tubeless ready
- CUBIERTAS: Schwalbe Rocket Ron EVO / 2.25
- PESO APRIXOMADO: 10.70 kg
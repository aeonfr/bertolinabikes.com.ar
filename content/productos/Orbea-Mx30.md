---
  title: "Orbea Mx 30 Rodado 29 Mtb 30 Vel Deore 2018"
  date: "2018-08-07"
  mercadolibre_id: "741507812"
  imgUrl: true
  imagen: "http://mla-s1-p.mlstatic.com/798724-MLA27913817660_082018-F.jpg"
  marcas: 
    - "Orbea"
  description: "Bicicleta Orbea MX30 Rodado 29 modelo 2018"
  categorias: ["bicicletas"]
  weight: -114
---

Bicicleta Orbea MX 30 Rodado 29 modelo 2018

# Características

- Cuadro: Orbea MX Alu only disc
- Horquilla: SR Suntour XCM RL-Coil 100mm QR
- Plato: Prowheel Marvel-951-TT 22x30x40t
- Dirección: 1-1/8" Semi-Integrated
- Manillar: Orbea OC-I Riser 720mm
- Potencia: Orbea OC II
- Manetas: Shimano Deore M6000
- Frenos: Shimano M315 Hydraulic Disc
- Piñón: Shimano HG50 11-36t 10-Speed
- Cambio: Shimano Deore M6000 SGS Shadow Plus Direct Mount
- Desviador: Shimano Deore M611 31.8mm
- Cadena: Fsa Team Issue
- Rueda: Orbea Black Rock 23c Disc
- Cubierta: Kenda K1153 2,35" 30TPI
- Pedales: VP-536 Black
- Tija Sillín: Orbea OC-I 27.2x400mm
- Sillín: Selle Royal 2062 HRN
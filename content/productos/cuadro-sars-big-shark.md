---
title: "Cuadro Sars Big Shark"
date: 2017-11-08T22:55:31-03:00
description: "Cuadro SARS Big Shark - Aluminio 6061"
imagen: "cuadro-sars-big-shark-04.jpg"
categorias: ["componentes"]
marcas: ["sars"]
envio_gratis: false
weight: -2
mercadolibre:
  - link: https://articulo.mercadolibre.com.ar/MLA-655043464-cuadro-mountain-bike-sars-big-shark-rodado-29-aluminio-_JM
    titulo: "Cuadro Mountain Bike Sars Big Shark Rodado 29 Aluminio"
gallery:
  - file: "cuadro-sars-big-shark-04.jpg"
    size: 1200x1200
  - file: "cuadro-sars-big-shark-01.jpg"
    size: 450x384
  - file: "cuadro-sars-big-shark-02.jpg"
    size: 450x391
  - file: "cuadro-sars-big-shark-03.jpg"
    size: 450x347
---

# Descripción


**Nuevo modelo 2017**.\
Cuadro de aluminio 6061.\
Marca: Sars.\
Modelo: Big Shark.


# Talles disponibles
- 15
- 17
- 21

# Rodados
- 29
- Consultar por otros rodados
---
title: "Casco MET Terra"
date: 2017-11-20T21:37:11-03:00
description: "Casco MET Terra apto para MTB / Allmountain / Enduro."
cloudinary: "aeonfr"
imagen: "v1511224948/bertolina/met-terra-01.jpg"
weight: 3
categorias: ["seguridad", "indumentaria"]
marcas: ["MET"]
envio_gratis: false
mercadolibre:
  - link: https://articulo.mercadolibre.com.ar/MLA-680000190-casco-bicicleta-met-terra-mtb-enduro-allmountain-_JM
gallery:
  - cid: v1511224948/bertolina/met-terra-01.jpg
    size: 1200x900
  - cid: v1511224949/bertolina/met-terra-02.jpg
    size: 1200x900
  - cid: v1511224946/bertolina/met-terra-03.jpg
    size: 1200x900
  - cid: v1511224950/bertolina/met-terra-04.jpg
    size: 1200x900
  - cid: v1511224946/bertolina/met-terra-05.jpg
    size: 1200x900
  - cid: v1511224945/bertolina/met-terra-06.jpg
    size: 1200x900
  - cid: v1511224949/bertolina/met-terra-07.jpg
    size: 1200x900
  - cid: v1511224950/bertolina/met-terra-08.jpg
    size: 1200x900
---

# Descripción
Casco MET Terra apto para MTB / Allmountain / Enduro.

- Talle unico: 54 a 61cm
- Excelente protección y durabilidad.
- Tecnología Ultimalite.
- Fibras de poliéster para evitar el sudor.
- Excelente diseño.
- Gran Volumen.
- Buena ventilación.
- Dispone de logos y pegatinas reflectantes para cuando vas circulando de noche o por zonas oscuras como los túneles. De esta manera aumenta la visibilidad.
- Peso de talla única 260 Gramos.

El casco Terra es sin duda un modelo muy elegante y dinámico, será imposible no pasar desapercibido. Dispone de un sistema ultimalite de gran volumen y buena ventilación.
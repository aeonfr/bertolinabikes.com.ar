---
  title: "Venzo Raptor Rodado 29 - 27 Vel"
  description: "Bicicleta para Mountain Bike con cuadro de aluminio y componentes de Shimano. Marca: Venzo. Modelo: Raptor."
  weight: -52
  imagen: "bicicleta-mtb-venzo-raptor-01.jpg"
  categorias: 
    - "bicicletas"
  marcas: 
    - "venzo"
  gallery: 
    - 
      file: "bicicleta-mtb-venzo-raptor-01.jpg"
      size: "1200x900"
    - 
      file: "bicicleta-mtb-venzo-raptor-02.jpg"
      size: "1200x900"
    - 
      file: "bicicleta-mtb-venzo-raptor-03.jpg"
      size: "1200x900"
    - 
      file: "bicicleta-mtb-venzo-raptor-04.jpg"
      size: "1200x900"
    - 
      file: "bicicleta-mtb-venzo-raptor-05.jpg"
      size: "1200x900"
    - 
      file: "bicicleta-mtb-venzo-raptor-06.jpg"
      size: "1200x900"
    - 
      file: "bicicleta-mtb-venzo-raptor-07.jpg"
      size: "1200x900"
    - 
      file: "bicicleta-mtb-venzo-raptor-08.jpg"
      size: "1200x900"
    - 
      file: "bicicleta-mtb-venzo-raptor-09.jpg"
      size: "900x1200"
  date: "2017-10-30"
  mercadolibre_id: "656127463"
  lowest_price_multiplier: 1
---

# CARACTERISTICAS:

* Rodado: 29
* Cuadro: RAPTOR ALUMINIO LIVIANO
* Transmisión: FULL SHIMANO 27 velocidades - Cambio piñon y cadena ACERA, descarrilador Altus
* Frenos: FRENOS A DISCO HIDRAULICOS SHIMANO
* Suspensión: SUNTOUR XCT O RST BLAZE (sujeto a disponiblidad del proveedor)
* Llantas: WTB DOBLE PARED LIVIANAS
* Mazas a ruleman
* Cubiertas: Maxxis Crossmark 29x2.1
* Manubrio, Stem, Portasilla, Asiento en combinación con el color del cuadro

# COLORES:

* Negro con detalles en verde / naranja / amarillo / rojo / celeste
* Celeste con detalles en verde / amarillo / naranja
* Rojo con detalles en amarillo / celeste
* Naranja con detalles en celeste / verde

# TALLES:

* S (16')
* M (18')
* L (20')
* XL (22')

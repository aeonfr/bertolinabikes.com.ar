---
title: "Caja Herramientas Super-B modelo TBA 500"
date: 2017-10-30T19:14:39-03:00
description: "Kit Caja de Herramientas Super B"
imagen: "kit-caja-herramientas-super-b-01.jpg"
weight: 2
categorias: ["herramientas"]
marcas: ["super b"]
envio_gratis: false
mercadolibre:
  - link: "https://articulo.mercadolibre.com.ar/MLA-608853163-kit-caja-herramientas-para-bicicleta-super-b-ideal-taller-_JM"
    titulo: "Kit Caja Herramientas Para Bicicleta Super B Ideal Taller"
gallery:
  - file: "kit-caja-herramientas-super-b-01.jpg"
    size: "219x360"
    description: "Kit Caja Herramientas Super B"
  - file: "kit-caja-herramientas-super-b-02.jpg"
    size: "360x360"
    description: "Kit Caja Herramientas Super B"
---

# Descripción
Valija de herramientas marca SUPER "B" Modelo TBA 500

# Incluye
- Llave de pedal de 15 mm.
- 2 Destornilladores, tamaño: Phillips 2 y Plano 6.
- Llave hexagonal de 1-8 mm y adaptador de 1/2'.
- 3 Palanca saca cubierta.
- Extractor de caja pedalera para eje cuadrado y octalink
- Juego de llaves allen 2 / 2,5 / 3 / 4 / 5 / 6 mm.
- Desarmacadenas para 8, 9 10 y 11 velocidades.
- Llave doble para 8 / 10 mm.
- Extractor de piñon / discos centerlock .
- Llave de cadena para extraer piñon
- Llaves de conos de 13 / 14mm y 15 / 16mm
- Herramienta para ajustar tapa de palancas Shimano con sistema Hollowtech
- Extractor de caja pedalera Hollowtech II de 34.7mm
- Llave estira rayos para distintas medidas
- Extractor de palancas.
- Kit de reparación de parches autoadhesivos.
- Llave Torx 25.

---
  title: "Andes Rodado 26 21 Vel + Suspension"
  date: "2018-08-07"
  mercadolibre_id: "647543333"
  imgUrl: true
  imagen: "http://mla-s2-p.mlstatic.com/730750-MLA27824363126_072018-F.jpg"
  marcas: 
    - "Venzo"
  description: "Bicicleta marca ANDES rodado 26"
  categorias: [ "bicicletas" ]
  weight: -99
---


Bicicleta marca ANDES rodado 26 - Transmision full Shimano de 21 velocidades 

# CARACTERISTICAS:

- Rodado: 26
- Cuadro: Aluminio 6061
- Grupo: SHIMANO Tourney 21 velocidades
- Frenos: Frenos V-BRAKE 
- Suspensión: Suspension Andes (combinada con el color del cuadro) 
- Llantas: aluminio
- Cubiertas: MTB 26 x 2.0
- Manubrio, Stem, Portasilla, Asiento en combinación con el color del cuadro
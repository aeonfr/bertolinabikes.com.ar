---
  title: "Scott Scale 960 Rodado 29 - 22 Vel (SLX)"
  imagen: "Scale960.jpg"
  marcas: 
    - "Scott"
  categorias: 
    - "bicicletas"
  description: "Bicicleta para MTB Scott Scale 960 Rodado 29 - 22 velocidades con cambios Shimano SLX."
  weight: -100
  date: "2018-05-30"
  draft: true
---


# CARACTERISTICAS:
- Rodado: 29
- Cuadro: Scale 960 aluminio 6061 , poste conico y cableado interno
- Transmisión: SHIMANO Slx 2x11 ( 22 Velocidades)
- Frenos: Shimano Br396 hidrualicos
- Suspensión: Rock Shox Recon 32mm con bloque (2 posiciones)
- Llantas: Syncross
- Mazas: Formula y Shimano
- Cubiertas: Maxxis Ikon kevlar 29x2.2

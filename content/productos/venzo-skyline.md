---
  title: "Venzo Skyline Loki Rodado 29 - 21 Vel"
  draft: false
  imagen: "skylineloki21v.jpg"
  gallery: 
    - 
      file: "skylineloki21v.jpg"
      size: "700x467"
    - 
      file: "skylineloki21v-2.jpg"
      size: "900x572"
    - 
      file: "skylineloki21v-3.jpg"
      size: "900x572"
    - 
      file: "skylineloki21v-4.jpg"
      size: "900x572"
    - 
      file: "skylineloki21v-5.jpg"
      size: "900x572"
    - 
      file: "skylineloki21v-6.jpg"
      size: "900x572"
  marcas: 
    - "venzo"
  categorias: 
    - "bicicletas"
  description: "Bicicleta para Mountain Bike con cuadro de aluminio 6061 y componentes de Shimano. Marca: Venzo. Modelo: Loki."
  weight: -120
  date: "2017-10-29"
  mercadolibre_id: "647382795"
  lowest_price_multiplier: 1
---


# CARACTERISTICAS:
- Rodado: 29
- Cuadro: Skyline ALUMINIO LIVIANO (LOKI 2018)
- Transmisión: FULL SHIMANO 21 velocidades
- Frenos: V-BRAKE PROMAX
- Suspensión: Venzo / con regulación y/o bloqueo (sujeto a disponibilidad del proveedor)
- Llantas: DOBLE PARED LIVIANAS
- Mazas a ruleman
- Cubiertas: MTB 29x2.0
- Manubrio, Stem, Portasilla, Asiento en combinación con el color del cuadro

*EL COLOR DE LA SUSPENSION Y / O RUEDAS PUEDE VARIAR SEGUN DISPONIBILIDAD*

# TALLES:
- S (16")
- M (18")
- L (20")

---
  title: "Caja De Herramientas Super B 27 Piezas"
  mercadolibre_id: "608853163"
  imgUrl: true
  imagen: "http://mla-s2-p.mlstatic.com/761044-MLA27773638319_072018-F.jpg"
  marcas: 
    - "Super B"
  description: "Caja de Herramientas para bicicletas."
  categorias: ["herramientas"]
  weight: -60
---
Caja de herramientas marca SUPER B modelo TBA500 de 27 piezas
 
# Incluye:
- Llave de pedal de 15 mm.
- 2 Destornilladores, tamaño: Phillips 2 y Plano 6.
- Llave hexagonal de 1-8 mm y adaptador de 1/2’.
- 3 Palanca saca cubierta.
- Extractor de caja pedalera para eje cuadrado y octalink
- Juego de llaves allen 2 / 2,5 / 3 / 4 / 5 / 6 mm.
- Desarmacadenas para 8, 9 10 y 11 velocidades.
- Llave doble para 8 / 10 mm.
- Extractor de piñon / discos centerlock .
- Llave de cadena para extraer piñon
- Llaves de conos de 13 / 14mm y 15 / 16mm
- Herramienta para ajustar tapa de palancas Shimano con sistema Hollowtech
- Extractor de caja pedalera Hollowtech II de 34.7mm
- Llave estira rayos para distintas medidas
- Extractor de palancas.
- Kit de reparación de parches autoadhesivos.
- Llave Torx 25.

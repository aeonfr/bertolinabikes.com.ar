---
  title: "Casco Scott Supra Con Regulacion"
  date: "2018-08-07"
  mercadolibre_id: "731434933"
  imagen: "cascos-scott.jpg"
  marcas: 
    - "scott"
  description: "Casco para bicicleta para ciclismo MTB. Modelo Scott Supra. Con regulación."
  categorias: ["indumentaria", "seguridad"]
  weight: -90
---

Casco para bicicleta para ciclismo MTB. Modelo Scott Supra. Con regulación.
---
  title: "Scott Scale 990 Rodado 29 - 20 Vel Deore"
  date: "2018-08-16"
  mercadolibre_id: "741672523"
  imgUrl: true
  imagen: "http://mla-s2-p.mlstatic.com/868997-MLA27918898676_082018-F.jpg"
  marcas: 
    - "scott"
  description: "Bicicleta para Mountain Bike Scott Scale 990 & Shimano Deore"
  categorias: ["bicicletas"]
  weight: -99
---

Bicicleta Scott Scale 990 Rodado 29 modelo 2018

# Características

- Cuadro:Scale Alloy 6061 Custom Butted Tubing Tapered HT / BB92 / Bridgeless Seatstays Internal Cable Routing / replaceable hanger
- Horquilla: Suntour XCR RL-R / Coil Spring 1 1/8" Steerer / Reb. Adj. Remote Lockout / 100mm travel
- Jgo Direccion: Syncros OE Press Fit / Reduce 1.5" - 1 1/8" OD 50/61mm / ID 44/55mm
- Cambio: Shimano Deore RD-M610 SGS Shadow Type / 20 Speed
- Descarrilador: Shimano Deore FD-M6025-H / 34.9
- Shifters: Shimano Deore SL-M6000 / Rapidfire plus 2 way release
- Frenos: Shimano M365 Disc 180/F and 160/Rmm SM-RT10 CL Rotor
- Plato palanca: Shimano FC-MT500-10 36x26 T
- Caja pedalera: Shimano BB-MT500-PA / shell 41x89.5mm
- Manubrio: Syncros T-Bar / Alloy 6061 T shape Flat / 9° / 720mm
- Stem: Syncros 6061 Alloy oversize 31.8mm / 1 1/8" / 6° angle
- Pedales: Wellgo M-21
- Portasilla: Syncros / 31.6x400mm
- Asiento: Syncros XR2.5
- Maza delantera Formula CL51
- Maza trasera Shimano FH-RM35 CL
- Cadena: KMC X10
- Piñon: Shimano CS-HG50-10 / 11-36 T
- Rayos: Stainless Black 15G / 1.8mm
- Llantas: Syncros X-20 / 32H
- Cubiertas: Maxxis Ikon / 2.2 / 60TPI
- Peso aproximado: 13.30 kg
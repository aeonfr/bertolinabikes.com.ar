---
  title: "Teknial Tarpan 300b Rodado 27.5 - 24 Vel"
  imagen: "tarpan300b-24v.jpg"
  marcas: 
    - "teknial"
  categorias: 
    - "bicicletas"
  description: "Bicicleta para Mountain Bike Teknial Tarpan 300b."
  weight: -117
  mercadolibre_id: "685010953"
  date: "2018-05-30"
  lowest_price_multiplier: 0.95
  promo: "casco"
---


# CARACTERISTICAS:
- Rodado: 27.5
- Cuadro: Aluminio
- Transmisión: 24 Velocidades con cambio Acera
- Frenos: Hidraulicos Tektro
- Suspensión Suntour XCT
- Llantas: DOBLE PARED LIVIANAS
- Cubiertas: MTB 27.5x1.95 KENDA
- Puños, manubrio, stem, llantas, asiento en combinación con el color del cuadro

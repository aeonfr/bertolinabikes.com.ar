---
  title: "Scott Scale 935 Carbono 1x11 Sram Talle M"
  date: "2018-08-07"
  mercadolibre_id: "726819980"
  imgUrl: true
  imagen: "http://mla-s1-p.mlstatic.com/880085-MLA27424489006_052018-F.jpg"
  marcas: 
    - "Scott"
  description: "Bicicleta Scott Scale 935 carbono para ciclismo MTB"
  categorias: ["bicicletas"]
  weight: -112
---

SCOTT SCALE 935 CARBONO 

TALLES DISPONIBLES "M" y "L"

# Características

- CUADRO: Scale 3 Carbon / IMP technology / MF BB92 / SW DM dropouts for Boost 12x148mm SDS2 advanced Shock & Standing Damping System ICCR Cable routing
- SUSPENSION: Rock Shox Judy Gold RL Solo Air 15x110mm QR axle / Tapered Steerer / Reb. Adj. Lockout / 100mm travel
- CAMBIO: Sram NX1 / 11 Speed
- SHIFTERS: Sram NX1 Trigger
- FRENOS: Shimano M315 Disc
- DISCOS: 180/F and 160/R mm SM-RT10 CL Rotor
- PALANCHAS Sram NX1 GXP Boost PF 30T
- CAJA PEDALERA: BB-SET Sram GXP PF integrated / shell 41x89.5mm
- MANUBRIO: SyncrosT-Bar / Alloy 6061 T shape Flat / 9° / 720mm
- STEM: Syncros 6061 Alloy oversize 31.8mm / 1 1/8" / 6° angle
- PORTASILLA: Syncros / 31.6x400mm
- ASIENTO: Syncros XR2.5
- MAZA DELANTERA: Formula CL811 / 15x110mm
- MAZA TRASERA: Formula CL1248 / Boost 12x148mm
- CADENA: Sram PC1110
- PIÑON: Sram PG1130 / 11-42 T
- RAYOS: Stainless Black 15G / 1.8mm
- LLANTAS: Syncros X-25 / 32H / 25mm Tubeless ready
- CUBIERTAS: Schwalbe Rapid Rob / 2.25 / 50EPI
- PESO APROXIMADO: 11,60kg
---
  title: "Casco KTM Factory Character - Ventilado Regulable"
  imgurl: true
  imagen: "http://mla-s2-p.mlstatic.com/775609-MLA27352536636_052018-F.jpg"
  mercadolibre_id: "724533595"
  marcas: 
    - "KTM"
  categorias: 
    - "indumentaria"
    - "seguridad"
  description: "Casco para practicar ciclismo de montaña."
  weight: -10
  date: "2018-05-12"
---


Con ventilado regulable.

*Estamos trabajando en la descripción de este producto, podes [dejarnos tu pregunta](/contacto) y te ayudaremos a resolver tus dudas*
---
  title: "Venzo Talon Rodado 29 - 27vel"
  description: "Bicicleta para Mountain Bike con cuadro de aluminio 6061 y componentes de Shimano. Marca: Venzo. Modelo: Talón."
  imagen: "talon24vel.jpg"
  weight: -105
  categorias: 
    - "bicicletas"
  marcas: 
    - "venzo"
  date: "2017-10-30"
  mercadolibre_id: "687652064"
  lowest_price_multiplier: 1
---


# CARACTERISTICAS:
- Rodado: 29
- Cuadro: TALON ALUMINIO LIVIANO
- Transmisión: FULL SHIMANO 27 velocidades con cambio Acera M3000
- Frenos: FRENOS A DISCO mecanicos Shimano 
- Suspensión: Suntour 3030
- Llantas: DOBLE PARED LIVIANAS
- Mazas a ruleman
- Cubiertas: MTB 29x2.0
- Manubrio, Stem, Portasilla, Asiento en combinación con el color del cuadro

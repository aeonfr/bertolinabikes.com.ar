---
  title: "GT Zaskar Rodado 29 Full Slx 22vel Raidon Aire"
  date: "2018-08-07"
  mercadolibre_id: "725103093"
  imgUrl: true
  imagen: "http://mla-s2-p.mlstatic.com/882772-MLA27365600566_052018-F.jpg"
  marcas: 
    - "gt"
  description: "Bicicleta para Mountain Bike rodado 29, con 22 velocidades."
  categorias: ["bicicletas"]
  weight: -99
---

BICICLETA GT ZASKAR Rodado 29 con 22 Velocidades FULL SLX 

# Características

- RODADO 29 
- Cambios: 22 Velocidades Shimano SLX M7000
- Frenos: Shimano SLX M7000
- Suspension: Suntour RAIDON de aire con bloqueo remoto - 100mm recorrido
- Ruedas: WTB con mazas novatec y cubiertas maxxis crossmark
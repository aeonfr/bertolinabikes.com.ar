---
  title: "Scott Spark 740 Rodado 27.5 Sram 11vel Rock Shox"
  date: "2018-08-07"
  mercadolibre_id: "726822471"
  imgUrl: true
  imagen: "http://mla-s2-p.mlstatic.com/867687-MLA27424554271_052018-F.jpg"
  marcas: 
    - "Scott"
  description: "Bicicleta para MTB Scott Spark 740 - rodado 27.5"
  categorias: ["bicicletas"]
  weight: -111
---

BICICLETA SCOTT SPARK 740 RODADO 27.5 DISPONIBLE EN TALLE "M"


# Características
- CUADRO:Spark 700 Alloy SL 6011 custom butted Hydroformed tubes tapered Headtube / BB92 / Boost 12x148mm 
- SUSPENSION: Rock Shox Recon RL Solo Air 15x110mm QR axle / Tapered Steerer Reb. Adj. / Lockout / 130mm travel
- SHOCK: X-Fusion RL Trunnion Lockout / Reb. Adj. Travel 120mm/ 165X45mm
- CAMBIO: Sram NX1 / 11 Speed
- GUIACADENA: SCOTT Chainguide
- SHIFTERS: Sram NX1 Trigger
- FRENOS: Shimano M315 Disc
- DISCOS: 180mm F & R / SM-RT30 CL Rotor
- PALANCAS: Sram NX1 GXP Boost PF30T
- CAJA PEDALERA: BB-SET Sram GXP PF integrated / shell 41x89.5mm
- MANUBRIO: Syncros FL2.0 mini Riser / Alloy 6061D.B. 12mm rise / 9° / 760mm
- STEM: Syncros 6061 Alloy oversize 31.8mm / 1 1/8" / 6° angle
- PORTASILLA: Syncros / 31.6x400mm
- ASIENTO: Syncros XM2.5
- MAZAS: (FRONT) Shimano HB-M6010-B CL / 15x110mm (REAR) Shimano FH-M6010-B CL / Boost 12x148mm
- CADENA: Sram PC1110
- PIÑON: Sram PG1130 / 11-42 T
- RAYOS: Stainless Black 15G / 1.8mm
- LLANTAS: Syncros X-30S / 32H / 30mm / Sleeve Joint Tubeless ready
- CUBIERTAS: Maxxis Rekon / 2.80 x 27.5 / 60TPI Kevlar Bead TR Tubeless ready Dual compound
- PESO APROXIMADO: 14,10 kg
---
  title: "Venzo Zeth Rodado 29 Vel (Deore M6000)"
  description: "Bicicleta para Mountain Bike Venzo Zeth."
  weight: -91
  imgurl: true
  imagen: "http://mla-s2-p.mlstatic.com/880661-MLA27352629031_052018-F.jpg"
  categorias: 
    - "bicicletas"
  marcas: 
    - "venzo"
  mercadolibre_id: "724534983"
  date: "2018-05-30"
  lowest_price_multiplier: 1
---

El cuadro ZETH está diseñado para recibir ruedas 27.5+ y 29er. Esto le da la versatilidad de una MTB y de una FatBike pudiendo el ciclista elegir que rodado es conveniente para la disciplina a realizar. Es compatible con ruedas 27.5 Hasta 27.5”x3.0; y 29er Hasta 29”x2.25.

# Características

* Transmision Shimano Deore M6000 2x10 
* Frenos Hidraulicos Shimano DEORE M6000
* Suspension Suntour Raidon Eje boost 15x110mm - recorrido 120mm - 34mm barral - bloqueo remoto
* Cuadro Aluminio 7005 con cableado interno y frente conico
* Eje trasero Boost 148x12mm

# Rodados

* 27.5 plus
* 29

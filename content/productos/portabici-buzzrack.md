---
  title: "Portabici p/auto Buzzrack Mozzquito - Cap Max 3 Bicicletas"
  imgurl: true
  imagen: "http://mla-s2-p.mlstatic.com/639131-MLA27352194887_052018-F.jpg"
  mercadolibre_id: "674474893"
  marcas: 
    - "buzzrack"
  categorias: 
    - "accesorios"
  description: "Portabicicleta para autoBuzzrack Mozzquito, con capacidad para 3 bicicletas."
  weight: -9
  date: "2017-07-14"
---


Portabicicleta BUZZRACK MOZZQUITO

- Capacidad: 3 bicicletas
- Peso máximo: 45 kg
- Adaptable a cualquier tipo de vehículo
- Material: Acero y plástico de alto impacto
- No se requieren herramientas para su colocación
- Punteras engomadas para evitar rayaduras en la pintura del vehículo
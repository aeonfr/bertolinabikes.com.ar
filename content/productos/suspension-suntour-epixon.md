---
  title: "Suspension Suntour Epixon Rodado 29 - Conica Eje 9mm"
  weight: -27
  imgurl: true
  imagen: "http://mla-s1-p.mlstatic.com/760721-MLA20826614433_072016-F.jpg"
  description: "Suspensión para bicicletas de montaña con poste cónico y eje de 9mm. Marca Suntour, modelo Epixon."
  marcas: 
    - "suntour"
  categorias: 
    - "componentes"
  mercadolibre_id: "628321004"
  date: "2018-06-06"
---


Suspension SUNTOUR Epixon Rodado 29

# Características

- Poste: Conico
- Peso: 1,8kg
- Bloqueo: Remoto
- Barrales: 32mm
- Recorrido: 100mm
- Eje: 9mm
---
  title: "Orbea Mx 50 Rodado 29 Modelo 2018"
  date: "2018-08-16"
  mercadolibre_id: "741681584"
  imgUrl: true
  imagen: "http://mla-s1-p.mlstatic.com/723987-MLA27919421805_082018-F.jpg"
  marcas: 
    - "Orbea"
  description: "Bicicleta Orbea MX50 Rodado 29 modelo 2018"
  categorias: ["características"]
  weight: -99
---

Bicicleta Orbea MX50 Rodado 29 modelo 2018

# Características

- Cuadro: Orbea MX Alu only disc
- Horquilla: SR Suntour XCT HLO Coil w/Preload Adj 100mm QR
- Plato: Shimano TY301 24x34x42t
- Dirección: 1-1/8" Semi-Integrated
- Manillar: Orbea OC Riser 720mm
- Potencia: Orbea OC
- Manetas: Shimano M310
- Frenos: Shimano M315 Hydraulic Disc
- Piñón: Shimano HG200 12-32t 8-Speed
- Cambio: Shimano Altus M310
- Desviador: Shimano Altus M313 31,8mm
- Cadena: KMC X8
- Rueda: Mach1 ER20 19c
- Cubierta: Kenda K1153 2,35" 22TPI
- Pedales: VPE-537 Black
- Tija Sillín: Orbea OC 27.2x400mm
- Sillín: Selle Royal 2073
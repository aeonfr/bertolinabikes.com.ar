---
  title: "Cuadro Venzo Atix Rodado 29 - Cabl Interno"
  weight: -27
  imgurl: true
  imagen: "http://mla-s2-p.mlstatic.com/923781-MLA27359929718_052018-F.jpg"
  description: "Cuadro Venzo Atix 2018 Rodado 29 Aluminio 7005 con Cableado Interno"
  marcas: 
    - "venzo"
  categorias: 
    - "componentes"
  mercadolibre_id: "710435361"
  date: "2018-06-06"
---



Cuadro VENZO ATIX MODELO 2018

# Características
- Rodado: 29
- Aluminio: 7005
- Peso aproximado 1.7kg
- Freno: Apto solo Disco
- Direccion: Conica (1 1/8 - 1 1/2)
- **Cableado Interno** tanto cambios como ducto de freno trasero
- Color:
	- Base Negro con detalles en rojo / celeste / verde / rosa
	- Base Rojo con detalles en gris
	- Base Celeste con detalles en blanco
- Caja pedalera: 34.7 
- Eje: 9mm
- Portasilla : 31.6

#TALLES

- 16
- 18
- 20 

*CONSULTAR POR OTROS RODADOS*
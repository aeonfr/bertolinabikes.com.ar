---
  title: "Venzo Inferno Rodado 20 - Varios Colores"
  marcas: 
    - "venzo"
  categorias: 
    - "bicicletas"
  description: "Bicicleta para BMX/salto Venzo Inferno Rodado 20."
  weight: -28
  imagen: "infernoBMX.jpg"
  mercadolibre_id: "674446536"
  date: "2018-05-30"
  lowest_price_multiplier: 1
---


# CARACTERISTICAS:
- Rodado: 20
- Cuadro: VENZO INFERNO CROMO-ACERO
- Frenos: PROMAX
- Llantas: ALUMINIO doble perfil
- Cubiertas: WANDAKING

# COLORES:
- Naranja con verde
- Rojo con celeste
- Amarillo con azul
- Rojo con blanco
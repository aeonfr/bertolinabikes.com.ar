---
title: "Bolso Portacelular Portaherramientas"
date: 2017-11-08T22:38:54-03:00
imagen: "bolso-portacelular-portaherramientas-01.jpg"
categorias: [ "accesorios" ]
envio_gratis: false
mercadolibre:
  - link: https://articulo.mercadolibre.com.ar/MLA-683865887-bolso-portacelular-y-portaherramientas-bicicleta-mtb-ruta-_JM
    titulo: Bolso Portacelular Y Portaherramientas Bicicleta Mtb / Ruta
gallery:
  - file: "bolso-portacelular-portaherramientas-01.jpg"
    size: 1200x900
    description: "Bolso portacelular + portaherramientas"
  - file: "bolso-portacelular-portaherramientas-02.jpg"
    size: 1200x900
    description: "Bolso portacelular + portaherramientas"
  - file: "bolso-portacelular-portaherramientas-03.jpg"
    size: 1200x900
    description: "Bolso portacelular + portaherramientas"
---

# Descripción
Bolso portacelular y porta herramientas para llevar en el cuadro de la bicicleta.

Compartimiento suficiente para llevar una camara de repuesto y/o herramientas, kit de parches.

Parte superior transparente para poder utilizar las aplicaciones en el celular

Abrozos para agarrar al cuadro y al stem.

Diseño disponible en varios colores:
- Rojo
- Rosa
- Verde
- Celeste
- Naranja
- Blanco
---
  title: "Pedales Exustar - Modelo E-PM215TI"
  imgurl: true
  imagen: "http://mla-s1-p.mlstatic.com/976058-MLA27352767640_052018-F.jpg"
  mercadolibre_id: "724537326"
  marcas: 
    - "exustar"
  categorias: 
    - "componentes"
  description: "Pedales Exustar de titanio 230gr modelo E-PM215TI"
  weight: -10
  date: "2018-05-12"
---


2 Pedales Exustar de Titanio.

- 230gr
- Modelo: E-PM215TI
---
title: "Juego Llantas Alexrim Volar 3.0"
date: 2017-11-08T21:37:41-03:00
description: "Repuesto para bicicletas MTB. Marca: Alexrim. Modelo: Volar"
imagen: "juego-llantas-alexrim-volar.jpg"
weight: 3
categorias: ["repuestos"]
marcas: ["alexrims"]
envio_gratis: false
mercadolibre:
  - link: https://articulo.mercadolibre.com.ar/MLA-648379900-juego-de-llantas-alexrim-volar-30-rodado-29-_JM
    titulo: "Juego de llantas Alexrim Volar 3.0 Rodado 29"
---

# Descripción

Llantas doble pared Rodado 29 - ALEXRIM VOLAR 3.0

- Nuevo modelo de perfil ancho
- Evita el destalonado
- Permite rodar con menor presión
- Peso: 530gr aprox.

![Juego de llantas](/imagenes/juego-llantas-alexrim-volar.jpg)
---
  title: "Venzo Thorn Rodado 29 - 27 Vel Disco Hidráulico"
  description: "Bicicleta apta para Mountain Bike con cuadro de aluminio 6061 con cableado interno. Cuadro Venzo Thorn y accesorios Venzo. Componentes Shimano."
  imagen: "venzo-thorn-r29-24vel-hidr (7).jpg"
  weight: -28
  categorias: 
    - "bicicletas"
  marcas: 
    - "venzo"
  gallery: 
    - 
      file: "venzo-thorn-r29-24vel-hidr (5).jpg"
      size: "1200x900"
      description: "Venzo Thorn Rodado 24 - 24 Vel. - Color Negro y Naranja"
    - 
      file: "venzo-thorn-r29-24vel-hidr (7).jpg"
      size: "1200x900"
      description: "Venzo Thorn Rodado 24 - 24 Vel. - Color Negro y Rosa"
    - 
      file: "venzo-thorn-r29-24vel-hidr (9).jpg"
      size: "1200x900"
      description: "Venzo Thorn Rodado 24 - 24 Vel. - Color Blanco y Rojo"
    - 
      file: "venzo-thorn-r29-24vel-hidr (1).jpg"
      size: "1200x900"
      description: "Venzo Thorn Rodado 24 - Manubrio transparente Venzo"
    - 
      file: "venzo-thorn-r29-24vel-hidr (2).jpg"
      size: "900x1200"
      description: "Venzo Thorn - SuspensiÃ³n Suntour XCT - Frenos a disco mecÃ¡nico Shimano"
    - 
      file: "venzo-thorn-r29-24vel-hidr (3).jpg"
      size: "1200x900"
      description: "Venzo Thorn - Shimano Acera de 24 velocidades"
    - 
      file: "venzo-thorn-r29-24vel-hidr (4).jpg"
      size: "1200x900"
      description: "Venzo Thorn"
    - 
      file: "venzo-thorn-r29-24vel-hidr (6).jpg"
      size: "1200x900"
      description: "Venzo Thorn"
    - 
      file: "venzo-thorn-r29-24vel-hidr (8).jpg"
      size: "1200x900"
      description: "Venzo Thorn"
  date: "2017-11-09"
  mercadolibre_id: "661176096"
  lowest_price_multiplier: 1
---


# Descripción
Bicicleta apta para Mountain Bike con cuadro de aluminio 6061 con cableado interno. Cuadro Venzo Thorn y accesorios Venzo. Componentes Shimano.

# Componentes
- Cuadro de Aluminio 6061 con cableado interno
- 24 y 27 Velocidades Shimano Acera
- Frenos a disco Shimano mecanicos o hidráulicos
- Suspension con bloqueo Suntour XCT 
- Llantas doble pared con mazas a ruleman
- Mazas a rulemanes
- Cubiertas MTB 29x2.0
- Accesorios Venzo en combinación de color
  - Asiento Venzo Antiprostatico

# Rodados
- 29
- Consultar por otros rodados
---
title: Venzo Vulcan
description: "Bicicleta para Mountain Bike con cuadro de aluminio 7005 y componentes de Shimano. Marca: Venzo. Modelo: Vulcan"
date: 2017-10-30T20:12:39.000Z
imagen: venzo vulcan (1).jpg
weight: -9
envio_gratis: true
garantia: '6'
categorias: ["bicicletas"]
marcas: ["venzo"]
mercadolibre:
  - link: https://articulo.mercadolibre.com.ar/MLA-641442087-bicicleta-mtb-venzo-vulcan-rodado-29-deore-20-vel-hidraulico-_JM
    titulo: Venzo Vulcan Rodado 29 - 20/30 vel. Deore - Disco Hidráulico
gallery:
  - file: "venzo vulcan (1).jpg"
    size: "1200x900"
    description: Venzo Vulcan Evo Rodado 29."
  - file: "venzo vulcan (2).jpg"
    size: "1200x900"
    description: Color Rojo y Negro."
  - file: "venzo vulcan (3).jpg"
    size: "1200x900"
    description: 27 Velocidades Shimano"
  - file: "venzo vulcan (4).jpg"
    size: "900x1200"
    description: Frenos HID"
  - file: "venzo vulcan (5).jpg"
    size: "1200x900"
    description: Color Verde y Negro"
  - file: "venzo vulcan (6).jpg"
    size: "1200x900"
    description: Shimano Acera con 27 velocidades"
  - file: "venzo vulcan (7).jpg"
    size: "1200x900"
    description: Asiento Venzo en combinación con el color del cuadro"
  - file: "venzo vulcan (8).jpg"
    size: "900x1200"
    description: Puños en combinación con el color del cuadro"
  - file: "venzo vulcan (9).jpg"
    size: "1200x900"
    description: Color Naranja y Negro"
  - file: "venzo vulcan (10).jpg"
    size: "1200x900"
  - file: "venzo vulcan (11).jpg"
    size: "1200x900"
  - file: "venzo vulcan (12).jpg"
    size: "900x1200"
---

# Descripción
Bicicleta para Mountain Bike con cuadro de aluminio **7005** y componentes de Shimano.

# Colores disponibles
- Celeste
- Rojo
- Naranja
- Verde

# Talles Disponibles
- 16 "S"
- 18 "M"
- 20 "L"

# Componentes
- Cuadro: Aluminio 7005 hidroformado. Con cableado interno. Frente: Cónico. Peso: 1,8kg.
- Grupo:
  - Shimano Altus/Acera 27 velocidades 
  - Shimano Alivio M4000 27 velocidades
  - Shimano Deore 20 velocidades
- Frenos: Frenos a disco hidráulicos Shimano
- Suspensión: Suntour XCM con bloqueo (combinada con el color del cuadro)
- Mazas a rulemanes

- **Las bicicletas con Shimano Altus/Acera vienen con:**
  - Llantas Doble pared WTB
  - Cubiertas: WTB 29x2.25
  - Puños: Giant en combinación con color del cuadro 
  - Accesorios (Manubrio, Stem, Portasilla, Asiento) Venzo en combinación con el color del cuadro
- **Las bicicletas con Shimano Deore vienen con:**
  - Ruedas Tubeless Ready WTB
  - Cubiertas WTB Tubeless
  - Accesorios Venzo en aluminio en combinacion de color con el cuadro
  - Asiento antiprostatico Venzo
  - Pedales de aluminio Wellgo con rulemanes
- **Las bicicletas con Shimano Alivio M4000 vienen con:**
  - Caja pedalera Hollowtech II
  - Llantas doble pared de aluminio
  - Cubiertas MTB 29x2.0
- **A medida**
  - Recuerde que puede consultarnos por el armado de una bicicleta a medida, eligiéndo los componentes que usted desee.

# Rodados
- Rodado: 29
- Consultar por otros rodados


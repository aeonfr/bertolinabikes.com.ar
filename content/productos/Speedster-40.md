---
  title: "Bicicleta Scott Speedster 40 Ruta Rodado 28 Shimano Claris"
  date: "2018-08-16"
  mercadolibre_id: "741679267"
  imgUrl: true
  imagen: "http://mla-s2-p.mlstatic.com/756504-MLA27919292378_082018-F.jpg"
  marcas: 
    - "scott"
  description: "Bicicleta Scott Speedster 40 Rodado 28 modelo 2018"
  categorias: ["bicicletas"]
  weight: -99
---

Bicicleta Scott Speedster 40 Rodado 28 modelo 2018

# Características

- Cuadro: Aleación Speedster 1 1/8" Alloy steerer
- Horquilla: Speedster Alloy 1 1/8" Alloy steerer
- Descarrilador trasero: Shimano Claris RD-R2000-SS 16 Speed
- Plato y palanca: Shimano Claris FC-RS200 50x34T
- Descarrilador delantero: Shimano Claris FD-R2000
- Shifters: Shimano Claris ST-R2000 Dual control 16 speed
- Piñon: Shimano CS-HG50 8 Speed 11-32 T
- Cadena: KMC X8
- Frenos: Tektro Comp SCBR-313 39-51mm
- Aros: Syncros Race 22 24 Front / 28 Rear
- Stem: JD ST123A Alloy 1 1/8" / four Bolt 31.8mm
- Manubrio: Syncros RR2.0 Comfort 31.8mm
- Portasilla: Syncros RR2.5 27.2/350mm
- Juego de dirección: Integrated Steel Cup
- Caja pedalera: Shimano BB-UN26
- Asiento: Syncros FL2.5
- Velocidades: Shimano Claris, 16 velocidades
- Peso: 10.30 kg
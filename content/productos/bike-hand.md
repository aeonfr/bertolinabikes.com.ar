---
title: "Soporte para Bicicleta Bike Hand"
date: 2017-11-08T21:51:19-03:00
description: "Pie de trabajo para bicicleta con altura regulable."
imagen: "bike-hand-01.jpg"
weight: 2
categorias: ["herramientas"]
marcas: ["bike hand"]
envio_gratis: false
mercadolibre:
  - link: https://articulo.mercadolibre.com.ar/MLA-627565126-soporte-pie-trabajo-para-bicicleta-bike-hand-regulable-_JM
    titulo: Soporte Pie Trabajo Para Bicicleta Bike Hand Regulable
---

# Descripción
Soporte/pie de trabajo para Bicicleta Bike Hand.

- Altura regulable con cierre
- Inclinacion regulable con cierre 
- Mordaza con giro de 360° 
- Inlcuye bandeja para herramientas (no incluye herramientas)
- Soporta bicicletas de hasta 20kg
- Peso: 5/6kg



![Bike Hand](/imagenes/bike-hand-01.jpg)
![Bike Hand](/imagenes/bike-hand-02.jpg)
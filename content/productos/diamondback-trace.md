---
  title: "Diamondback Trace Rodado 27.5 - 21 vel (dama)"
  weight: -28
  marcas: 
    - "diamonback"
  categorias: 
    - "bicicletas"
  description: "Bicicleta para MTB Diamondback Trace rodado 27.5, 21 velocidades Shimano. Conjunto diseñado para Dama."
  imagen: "diamondback-trace-dama-21v.jpg"
  mercadolibre_id: "638500106"
  date: "2018-05-30"
---


# CARACTERISTICAS:
- Rodado: 27.5
- Cuadro: DIAMONDBACK TRACE ALUMINIO LIVIANO
- Transmisión: FULL SHIMANO 21 velocidades
- Frenos: V-BRAKE PROMAX
- Suspensión: VZ Bike / WKNS / Diamondback con regulación (sujeto a disponibilidad del proveedor)
- Llantas: DOBLE PARED LIVIANAS
- Mazas a ruleman
- Cubiertas: MTB 27.5x2.0
- Manubrio, Stem, Portasilla, Asiento en combinación con el color del cuadro

# COLORES:

- Base negra con detalles en celeste y rosa
- Base blanca con detalles en celeste y rosa / detalles en celeste y rojo

#TALLES:
- S (16")
- M (18")
---
title: "Adaptador de horquilla cónica a frente recto"
date: 2017-11-09T00:40:38-03:00
description: "Adaptador para horquilla cónica a frente recto 1 1/2 - 1 1/8 , fabricados en aluminio para dirección semi integrada, permiten el uso de horquillas con poste cónico en cuadros con frente recto."
imagen: "adaptador-horquilla-02.jpg"
weight: 2
categorias: ["accesorios", "repuestos"]
mercadolibre:
  - link: https://articulo.mercadolibre.com.ar/MLA-682802211-adaptador-horquilla-conica-a-frente-recto-_JM
gallery:
  - file: adaptador-horquilla-01.jpg
    size: 1200x900
  - file: adaptador-horquilla-02.jpg
    size: 1200x900
---

# Descripción

Adaptador para horquilla cónica a frente recto 1 1/2 - 1 1/8 , fabricados en aluminio para dirección semi integrada, permiten el uso de horquillas con poste cónico en cuadros con frente recto.

Únicos disponibles en el mercado!

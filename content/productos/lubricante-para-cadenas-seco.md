---
title: "Lubricante Para Cadenas Seco"
date: 2017-11-09T00:59:08-03:00
description: "Aceite lubricante Penetrit SECO x 110cm3."
imagen: "lubricante-cadena-seco.jpg"
categorias: ["herramientas"]
gallery:
  - file: lubricante-cadena-seco.jpg
    size: 500x500
weight: 10
---

# Descripción
Aceite lubricante Penetrit SECO x 110cm3

- Forma pelicula seca de protección 
- Reduce la fricción 
- Ideal para condiciones áridas y polvorientas
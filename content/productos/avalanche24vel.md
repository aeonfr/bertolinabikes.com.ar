---
  title: "GT Avalanche 2018 Rodado 29 - 24 Vel Disco Hidráulico"
  description: "Bicicleta para Mountain Bike GT Avalanche."
  imagen: "avalanche24vel.jpg"
  weight: -95
  mercadolibre_id: "649179170"
  marcas: 
    - "gt"
  categorias: 
    - "bicicletas"
  date: "2018-05-30"
---


# CARACTERISTICAS:
- Rodado: 29
- Cuadro: GT Avalanche Sport con frente conico
- Transmisión: Full Shimano con cambio Acera de 24 Velocidades
- Frenos: A Disco Hidraulicos SHIMANO
- Suspensión: SUNTOUR XCM con bloqueo 
- Llantas: Aluminio doble pared ALL TERRA
- Mazas a rulemanes
- Cubiertas: KENDA MTB 29x2.0
- Manubrio, Stem, Portasilla, Asiento en combinación con el color del cuadro

# TALLES:
- S (15")
- M (17")
- L (19")

# También disponible
- En [27 velocidades](/productos/avalanche27).
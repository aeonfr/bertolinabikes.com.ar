---
  title: "Orbea Avant M30 Rodado 28 2018 Carbono 22vel 105"
  date: "2018-08-07"
  mercadolibre_id: "741503152"
  imgUrl: true
  imagen: "http://mla-s1-p.mlstatic.com/802229-MLA27913673290_082018-F.jpg"
  marcas: 
    - "orbea"
  description: "Bicicleta Orbea Avant M30, rodado 28, modelo 2018."
  categorias: ["bicicletas"]
  weight: -113
  promo: "casco-orbea"
---

Bicicleta ORBEA AVANT M30 Rodado 28 MODELO 2018

# Características
- CUADRO Orbea Avant carbon OME, monocoque, tapered 1-1/8" - 1,5", PF 86mm, internal cable routing, EC/DC compatible, 130 rear spacing, 27,2mm seat tube.
- HORQUILLA Avant OME carbon fork, full carbon steerer, tapered 1-1/8" - 1,5", carbon dropouts.
- PLATO FSA Gossamer Compact MegaExo 34x50t
- DIRECCION FSA 1-1/8 - 1-1/2" Integrated Carbon Cup ACB Bearings
- MANILLAR FSA Omega Compact
- POTENCIA Orbea OC-II
- MANETAS Shimano 105 5800
- FRENO Orbea OC-II
- PIÑON Shimano 105 5800 11-32t 11-Speed
- CAMBIO Shimano 105 5800 GS
- DESVIADOR Shimano 105 5801
- CADENA Fsa Team Issue
- RUEDA Vision Team 30 Comp Clincher
- CUBIERTA Kenda Kriterium 700x25 60TPI
- PEDALES N/A
- TIJA SILLIN Orbea OC-II Carbon 27.2x350mm
- SILLIN Prologo K3 STN size 141 mm
- CINTA MANILLAR Bar Tape
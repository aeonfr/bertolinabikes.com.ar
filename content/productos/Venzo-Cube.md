---
  title: "Venzo Cube Rodado 20 - BMX"
  date: "2018-08-07"
  mercadolibre_id: "620596449"
  imgUrl: true
  imagen: "http://mla-s2-p.mlstatic.com/660509-MLA27574589028_062018-F.jpg"
  marcas: 
    - "Venzo"
  description: "Bicicleta Bmx Para Salto Venzo Cube Rodado 20"
  categorias: ["bicicletas" ]
  weight: -99
---

BICICLETA VENZO CUBE RODADO 20" - 

- Cuadro y horquilla de Cromo Molibdeno 
- Palancas 25T - 3 piezas a rulemanes 
- Juego de Direccion Neco Integrado
- Maza trasera Panch Driver de 9
- Pedales Aluminio
- Cubiertas Chaoyang
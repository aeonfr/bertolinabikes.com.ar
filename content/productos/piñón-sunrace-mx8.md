---
  title: "Piñón Sunrace Mx8 - 11 Vel"
  imgurl: true
  imagen: "http://mla-s1-p.mlstatic.com/682184-MLA27352536406_052018-F.jpg"
  mercadolibre_id: "724533586"
  marcas: 
    - "sunrace"
  categorias: 
    - "componentes"
  description: "Piñon Sunrace Mx8 11 Velocidades Shimano Compatible 11-50"
  weight: -10
  date: "2018-05-12"
---


Piñon Sunrace Mx8 11 Velocidades Shimano Compatible 11-50

*Estamos trabajando en la descripcion de este producto, podes dejarnos tu pregunta y te ayudaremos a resolver tus dudas*
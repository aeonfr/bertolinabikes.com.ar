---
  title: "Ruedas Easton Ea70 XCT Rodado 29 - Ejes 15/12 Body Shimano"
  imgurl: true
  imagen: "http://mla-s1-p.mlstatic.com/797335-MLA27352537041_052018-F.jpg"
  mercadolibre_id: "724533672"
  marcas: 
    - "Easton"
  categorias: 
    - "componentes"
  description: "Componente para bicicletas de Mountain Bike."
  weight: -10
  date: "2018-05-12"
---


*Estamos trabajando en la descripción de este producto, podes dejarnos tu pregunta y te ayudaremos a resolver tus dudas*
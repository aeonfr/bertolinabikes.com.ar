---
title: "Plato Palanca FSA Comet"
date: 2017-11-20T20:07:46-03:00
description: "Plato palanca biplato FSA Comet para 10 Y 11 Velocidades - Incluye caja hollowtech."
cloudinary: "aeonfr"
imagen: "v1511219408/bertolina/plato-palanca-fsa-comet.jpg"
weight: 2
categorias: ["componentes"]
marcas: ["FSA"]
envio_gratis: true
gallery:
  - cid: v1511219408/bertolina/plato-palanca-fsa-comet.jpg
    size: 1200x900
---

# Descripción
Plato palanca biplato FSA Comet para 10 Y 11 Velocidades

- Incluye caja hollowtech.
- Araña convertible a Direct Mount
- Relacion: 36-24
- Largo: 175mm
- BCD: 96-68
- Material: Aluminio 6061
- Peso: 900gr aprox
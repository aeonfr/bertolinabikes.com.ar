---
  title: "Venzo Atix Rodado 29 - 20 Vel (Deore M6000)"
  description: "Bicicleta para Mountain Bike con cuadro de aluminio y componentes Shimano."
  imagen: "venzo-atix-rodado-29 (5).jpg"
  categorias: 
    - "bicicletas"
  marcas: 
    - "venzo"
  gallery: 
    - 
      file: "venzo-atix-rodado-29 (5).jpg"
      size: "1200x900"
      description: "Venzo Atix Rodado 29 - Color Negro y Rosa"
    - 
      file: "venzo-atix-rodado-29 (4).jpg"
      size: "1200x900"
      description: "Venzo Atix Rodado 29 - Color Negro y Blanco, con detalles rojos"
    - 
      file: "venzo-atix-rodado-29 (3).jpg"
      size: "1200x900"
      description: "Venzo Atix Rodado 29 - Color Negro y Amarillo"
    - 
      file: "venzo-atix-rodado-29 (6).jpg"
      size: "1200x900"
      description: "Venzo Atix Rodado 29 - Color Negro y Rojo"
    - 
      file: "venzo-atix-rodado-29 (8).jpg"
      size: "1200x900"
      description: "Venzo Atix Rodado 29 - Color Color Negro y Naranja"
    - 
      file: "venzo-atix-rodado-29 (2).jpg"
      size: "1200x900"
      description: "Venzo Atix Rodado 29 - Color Negro y Naranja - Detalle: Diseño del cuadro"
    - 
      file: "venzo-atix-rodado-29 (1).jpg"
      size: "1200x900"
      description: "Venzo Atix Rodado 29 - Color Negro y Naranja - Detalle: Pedal y Corona"
    - 
      file: "venzo-atix-rodado-29 (7).jpg"
      size: "1200x900"
      description: "Venzo Atix Rodado 29 - Color Negro y Naranja - Detalle: Manubrio y Cambios"
  weight: -115
  date: "2017-11-09"
  mercadolibre_id: "665300505"
  lowest_price_multiplier: 1
---


# CARACTERISTICAS:
- Rodado: 29
- Cuadro: ATIX ALUMINIO 7005 LIVIANO
- Transmisión: FULL SHIMANO DEORE M-6000 20 velocidades (NUEVO MODELO)
- Frenos: FRENOS A DISCO HIDRAULICOS SHIMANO M-315
- Suspensión: SUNTOUR XCT O RST BLAZE C/BLOQUEO 
- Llantas: WTB DOBLE PARED LIVIANAS
- Mazas a ruleman NOVATEC
- Cubiertas: MAXXIS CROSSMARK
- Manubrio, Stem, Portasilla, Asiento en combinación con el color del cuadro
- Pedales aluminio WELLGO

# COLORES: 
- Negro con detalles en rojo o verde

# TALLES:
- S (16")
- M (18")
- L (20")
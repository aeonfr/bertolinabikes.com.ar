---
title: "Piñon a cassette Deore M6000"
date: 2017-10-29T19:22:10-03:00
draft: false
description: "Piñón de Shimano, línea 2017."
imagen: "pinon-shimano-deore-m-6000.jpg"
weight: 1
categorias: ["componentes"]
marcas: ["shimano"]
envio_gratis: false
mercadolibre_id: "689377258"
gallery:
  - file: pinon-shimano-deore-m-6000.jpg
    size: 1000x1000
    description: Piñón Deore
---

# Características
- 10v
- Desarrollo 11-42T
- Peso: 430g

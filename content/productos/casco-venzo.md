---
title: "Casco Venzo"
date: 2017-11-08T20:09:29-03:00
description: "Casco Venzo para ciclismo de montaña o de ruta. Con Regulación y Visera. Modelos Unisex."
imagen: "casco-venzo-06.jpg"
weight: -1
categorias: ["seguridad", "indumentaria"]
marcas: ["venzo"]
envio_gratis: false
mercadolibre:
  - link: https://articulo.mercadolibre.com.ar/MLA-654622476-casco-bicicleta-venzo-c-regulacion-y-visera-dama-hombre-_JM
    titulo: "Cascos Venzo con regulación y visera"
gallery:
  - file: "casco-venzo-01.jpg"
    size: 1200x900
    description: "Todos los modelos disponibles"
  - file: "casco-venzo-02.jpg"
    size: 1200x900
  - file: "casco-venzo-03.jpg"
    size: 1200x900
  - file: "casco-venzo-04.jpg"
    size: 1200x900
    description: "Color Verde"
  - file: "casco-venzo-05.jpg"
    size: 1200x900
    description: "Color Amarillo"
  - file: "casco-venzo-10.jpg"
    size: 1200x900
    description: "Color Rojo"
  - file: "casco-venzo-07.jpg"
    size: 1200x900
    description: "Color Azul"
  - file: "casco-venzo-09.jpg"
    size: 1200x900
  - file: "casco-venzo-08.jpg"
    size: 1200x900
    description: "Color Rosa"
---

# Descripción
Casco Venzo para ciclismo de montaña o de ruta. Con Regulación y Visera. Modelos Unisex.

# Colores
- Verde
- Azul
- Rojo
- Amarillo
- Rosa

# Talles
- "M" (53 a 62cm)
- "L" (55 a 64cm)
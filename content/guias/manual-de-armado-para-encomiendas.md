---
title: "Manual de Armado de Bicicleta"
date: 2017-10-16T23:34:14-03:00
draft: false
description: "Las bicicletas que despachamos viajan desarmadas, de forma compacta, para evitar excesivos gastos de envío. Esta guía explica cómo poner cada parte en su lugar, y dejar tu bici lista para su uso final."
thumb: "/recursos/imagenes/guias/armado-00a-encomienda.jpg"
---

Habiendo recibido su bicicleta por encomienda, deberá realizar una serie de pasos para dejarla lista para su uso 👌.

---

# Requisitos

- Llaves allen Nº 4, 5 y 6. 
- Llave Nº 15. 
- Tijera (para romper los precintos).

{{<figure src="/recursos/imagenes/guias/armado-01-herramientas.svg" caption="Imagen 1: Herramientas necesarias" alt="Llaves Allen y una Llave de Ajuste Nº 12">}}

# 1. Retirar Embalajes

Antes de comenzar el proceso de armado, saque las partes de la bicicleta del paquete. Rompa el precinto que une la palanca con el manubrio y los precintos que unen la rueda delantera con el cuadro.

# 2. Colocar la rueda delantera

Junto con los pedales encontrará el **cierre para la rueda**, como el que se muestra en la imagen 2.


{{<figure alt="Imagen 2: Cierre de la rueda." src="/recursos/imagenes/guias/armado-02-cierre-de-la-rueda.svg" caption="Imagen 2: Cierre de la rueda.">}}

Coloque el cierre de forma tal que la **traba del cierre** quede del lado izquierdo de la bicicleta (el lado opuesto a la cadena). Los dos **resortes del cierre** deben quedar apuntando hacia el centro (imagen 3).

{{<figure alt="Imagen 3: Posicionamiento del cierre de la rueda." src="/recursos/imagenes/guias/armado-03-posicionamiento-cierre-de-la-rueda.svg" caption="Imagen 3: Posicionamiento del cierre de la rueda.">}}

# 3. Colocar el manubrio

Con ayuda de una llave allen N° 4 desajuste los cuatro tornillos del **soporte del manubrio** (imagen 4). Retire la **placa delantera del soporte** y coloque el **manubrio** de forma que quede centrado. Vuelva a colocar la placa delantera del soporte del manubrio, ajustando con fuerza los cuatro tornillos.

{{<figure alt="Imagen 4: Manubrio colocado." src="/recursos/imagenes/guias/armado-04-manubrio.svg" caption="Imagen 4: Manubrio colocado.">}}

# 4. Colocar pedales

Encuentre las letras indicadoras «L» y «R» (left & right) en la rosca de cada pedal (imagen 5).

{{<figure alt="Imagen 5: Ubicación de la letra indicadora del pedal." src="/recursos/imagenes/guias/armado-05-ubicacion-letra-indicadora-pedal.svg" caption="Imagen 5: Ubicación de la letra indicadora del pedal.">}}

El pedal «R» se coloca en el lado derecho de la bicicleta (el lado de la cadena) y su rosca ajusta en el sentido de las agujas del reloj (imagen 6).

{{<figure alt="Imagen 6: Dirección de ajuste del pedal derecho." src="/recursos/imagenes/guias/armado-06-ajuste-pedal-derecho.svg" caption="Imagen 6: Dirección de ajuste del pedal derecho.">}}

Por el contrario, el pedal «L» se ubica en el lado izquierdo de la bicicleta y la rosca ajusta en el sentido anti-horario.

El primer ajuste del pedal se puede realizar con la mano, luego, ayúdese de la llave Nº15 para dar un ajuste final. Utilize mucha fuerza para que quede bien ajustado, de lo contrario se puede dañar la rosca del pedal.

# 5. Colocar el asiento junto con el portasilla

Primero desajuste el **tornillo del portasilla** usando la llave Allen Nº 6 (imagen 7).

{{<figure src="/recursos/imagenes/guias/armado-07-tornillo-portasilla.svg" alt="Imagen 7: Unión del asiento con el portasilla. Ubicación del tornillo del portasilla." caption="Imagen 7: Unión del asiento con el portasilla. Ubicación del tornillo del portasilla.">}}

A continuación, enganche el asiento por su **riel**, sin sobrepasar las medidas máximas señalizados en el mismo riel (imagen 8a).

{{<figure src="/recursos/imagenes/guias/armado-08a-riel-del-asiento.svg" alt="Imagen 8a: Riel del asiento." caption="Imagen 8a: Riel del asiento." >}}

Una vez colocado el asiento, ajuste el **tornillo del portasilla** para mantener la posición, pero no del todo (el ajuste final se hace una vez que el portasilla esté colocado en el cuadro, en el paso siguiente).

# 6. Colocar el portasilla en el cuadro

Con ayuda de la llave Allen Nº5, desajuste la rosca del **collar del asiento** (imagen 8b) y coloque el portasilla (con el asiento ya colocado) en el cuadro de la bicicleta.

{{<figure src="/recursos/imagenes/guias/armado-08b-collar-del-asiento.svg" alt="Imagen 8b: Collar del asiento." caption="Imagen 8b: Collar del asiento." >}}

Ajuste el collar del asiento para que el portasilla quede anclado firmemente al cuadro.
Una vez que la unión del cuadro con el portasilla sea firme, realice un ajuste final para unir el asiento con el portasillas. El asiento debe quedar paralelo al piso (imagen 9).

{{<figure src="/recursos/imagenes/guias/armado-09-asiento-paralelo-al-piso.svg" alt="Imagen 9: Asiento paralelo al piso." caption="Imagen 9: Asiento paralelo al piso." >}}

{{< info >}}

<img style="width:35%;float:right;min-width:6rem" class="ml1" src="/recursos/imagenes/guias/armado-10-posicion-pedaleo.svg" alt="Posición de pedaleo">

<p>El portasilla no debe quedar muy pegado con respecto al marco, de forma que se pueda pedalear manteniendo la cadera fija, pero tampoco debe quedar muy alejado del mismo, para evitar la necesidad de sobreextender la pierna. La posición correcta para pedalear es con el cuerpo inclinado a 45º y apoyándose en el asiento en el área indicada en la imagen.</p>


{{</info>}}

# 7. ¡Listo!

Su bicicleta está lista para ser usada. No dude en contactarse con nosotros si necesita ayuda  para armar su bicicleta.

**Algunos consejos de seguridad:**

- Por su seguridad, maneje siempre con casco y luces. 
- Si su bicicleta posee dos frenos de mano, utilice ambos al mismo tiempo. El uso excesivo del freno delantero puede causar que la rueda trasera se levante del terreno. Regule los frenos para el tipo de uso que le va a dar a su bicicleta.
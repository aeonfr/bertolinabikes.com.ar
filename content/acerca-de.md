---
title: "Acerca de nosotros"
---

Somos una empresa ubicada en Córdoba Capital que vende y distribuye bicicletas y equipamiento para ciclismo en Argentina.

Contamos con un espacio en Pueyrredón al 1506, a sólo 20 cuadras de Plaza España.

Vendemos [productos](/productos) de última gamma, y las [marcas](/marcas) más reconocidas a precios competitivos; lo que explica por qué usuarios de todo el país nos siguen eligiendo ♥.
---
aliases:
    - /meta/manual-de-identidad/
    - /manual-de-identidad
title: "Manual de Identidad"
date: 2017-08-19T14:17:19-03:00
---

# Manual de Identidad
---

# Colores

La paleta de colores se inspiró en la energía y juventud del rojo combinada con contrastes entre negro y blanco. Los colores secundarios de la identidad son grises y tierras: inspirados en la madera y el metal.

## Colores de identidad

<table role="presentation" class="mb2">
<tr>
<td>
    <div class="p2 bg-rojo"></div>
    rojo
    <br>#FB0410
    <br>cmyk(0,95,100,0)
    <br>Pantone 485C
</td>
<td>
    <div class="p2 bg-gris-claro"></div>
    gris-claro
    <br>#b3b3b3
    <br>hsl(0,0,65)
    <br>cmyk(0,0,0,35)
</td>
<td>
    <div class="p2 bg-negro"></div>
    negro
    <br>#000000
    <br>cmyk(0,0,0,100)
</td>
</table>

## Otros colores

<div class="flex flex-wrap lh-copy nl">
<div class="nowrap flex my1">
    <div class="dib p2 bg-negro mx1"></div>
    negro
    <br>#000000
    <br>cmyk(0,0,0,100)
</div>
<div class="nowrap flex my1">
    <div class="dib p2 bg-gris-oscuro mx1"></div>
    gris-oscuro
    <br>#4d4d4d
    <br>hsl(0,0,35)
    <br>cmyk(0,0,0,65)
</div>
<div class="nowrap flex my1">
    <div class="dib p2 bg-gris mx1"></div>
    gris
    <br>#808080
    <br>hsl(0,0,50)
    <br>cmyk(0,0,0,50)
</div>
<div class="nowrap flex my1">
    <div class="dib p2 bg-casi-blanco mx1"></div>
    casi-blanco
    <br>#e6e6e6
</div>
<div class="nowrap flex my1">
    <div class="dib p2 bg-madera-claro mx1"></div>
    madera-claro
    <br>#eac06e
</div>
<div class="nowrap flex my1">
    <div class="dib p2 bg-madera-oscuro mx1"></div>
    madera-oscuro
    <br>#a95c0c
</div>
<div class="nowrap flex my1">
    <div class="dib p2 bg-rojo-oscuro mx1"></div>
    rojo-oscuro
    <br>#810000
</div>
</div>

## Recomendaciones de uso del color

El color del texto sobre fondo blanco para la impresión, es un “casi negro”, ya que está comprobado el negro puro es menos legible (y además el "casi negro" tiene un acabado estético más suave). El “casi negro” no se usa como fondo y mucho menos en el medio digital, ya que queda indefinido y en algunos _displays_ puede ser confundido con marrón.

No es sugerido utilizar los colores más saturados de la paleta (rojo y rojo oscuro) como fondos.

Las combinaciones sugeridas son:

<section class="texto-l">
<div class="bg-negro blanco p1">Blanco sobre Negro</div>
<div class="bg-blanco rojo p1">Rojo sobre Blanco</div>
<div class="bg-blanco p1">Negro sobre Blanco</div>
<div class="bg-blanco gris-oscuro p1">Gris Oscuro sobre Blanco</div>
<div class="bg-gris-oscuro blanco p1">Blanco sobre Gris-oscuro</div>
<div class="bg-casi-blanco negro p1">Negro sobre Casi-blanco</div>
<div class="bg-madera-claro negro p1">Negro sobre Madera-claro</div>
<div class="bg-madera-oscuro blanco p1">Blanco sobre Madera-oscuro</div>

</section>

---

# Tipografía

## Tamaños de títulos y de texto de párrafo

El texto de párrafo o "tamaño de la tipografía base" es el valor del texto de corrido en piezas comunicacionales que contienen mayormente texto (libros, informes, manuales, contratos, afiches, infografías, etc.). Este tamaño es variable, por ejemplo en este sitio web varía de acuerdo al ancho del dispositivo (si estás en una computadora, podés experimentarlo cambiando el tamaño de la ventana del navegador).

Los títulos tienen un tamaño relativo al de la tipografía base. Para la web se recomienda usar al menos `16px` de tamaño y para impresión, al menos `10pt`.

La variable “tamaño de texto” en total tiene 5 posibles valores, expresados aquí como “rems”, donde 1rem equivale a una vez el tamaño tipográfico del texto base. Los nombres están designados de acuerdo al nombre que se les dió en el código fuente del sitio web.

**texto-xxl** (Título 1): 1.5rem\
**texto-xl** (Título 2): 1.25rem\
**texto-l** (Título 3): 1.125rem\
**texto-m** (Título 4 y Texto Base): 1rem\
**texto-s** (Título 5 y 6): 0.75rem

El texto no va por debajo de 0.75rem bajo ningún concepto. Alternativamente el texto puede tener de tamaño 0.875rem cuando se quiera utilizar un nivel extra de graduación.

# Título 1

Versalitas y 150% el tamaño de fuente del texto base.

## Título 2

Versalitas y 125% el tamaño de fuente del texto base.

### Título 3

Versalitas y 112,5% el tamaño de fuente del texto base.

#### Título 4

Versalitas y 100% el tamaño de fuente del texto base.

##### Título 5

Todo mayúsculas y 75% tamaño de texto base.

###### Título 6

Todo mayúsculas y 75% tamaño de texto base.

## Sobre el uso de títulos.

**No es recomendable** utilizar mucha jerarquía de títulos. De hecho, en la mayoría de las piezas de comunicación se intenta usar dos o tres títulos distintos, no más que eso. Por ejemplo, con el título 1 y el título 2 alcanza para este manual, incluso aunque cuenta con varias secciones.

## Familias tipográficas

La tipografía utilizada para el texto base es la **DIN Pro**, sin embargo para la web se reemplaza por la muy similar —pero gratuita y lista para ser insertada en una página web—, **[Roboto]** (de Google). Esta tipografía viene por defecto en celulares Android, por lo tanto los usuarios de móbiles (que son quienes más probablemente tienen una conección lenta de internet) no deben descargarla para ver el sitio. Los usuarios de Windows y otros sistemas operativos si deberán descargarla, pero solamente la primera vez que abran el sitio.

Se utilizan sólamente las variantes **regular** (400) y **bold** (700). **No** se utiliza en **medium** (500) ni en **black** (900), ni tampoco se utiliza la versión **itálicas** siempre que pueda evitarse.

Para los **títulos**, la tipografía elegida es la **DIN Pro** en estilo Versalitas (<span class="small-caps nowrap">Small Caps</span>) y Bold, pero se reemplaza en la web por la **Roboto Condensed**, así mismo en estilo Versalitas y Bold.

Se recomienda el uso de la tipografía **Russo One** para destacar el texto de forma artística.

![](http://www.dafont.com/img/preview/r/u/russo_one0.png "Russo One")
![](http://www.dafont.com/img/preview/r/o/roboto14.png "Roboto")

Ambas tipografías se pueden descargar para la PC y usar para la web directamente desde **Google Fonts**.

## Interlineado

El interlineado del texto es siempre equivalente al 150% del tamaño del texto. Por ejemplo, si el tamaño del texto base es de 10pt, el interletrado será de 15pt.

El interlineado en títulos puede dejarse al 100% o en modo automático, o puede seguirse con al convención de usar un 150% del tamaño (por ejemplo un título de 15pt puede tener interlineado de 15pt o de 22.5pt).

## Márgenes o "espacio posterior"

La distancia que separa un párrafo del que le sigue es de 100% del tamaño de texto base (`1rem`).

En caso de no saber cómo especificar la opción "space after" o "espacio posterior" en el software que usas, **no** simular este espacio introduciendo dos enters en vez de uno después de cada párrafo. Esto no es recomendado porque trae problemas en la maquetación, sobretodo cuando hay saltos de página y el espacio luego de un párrafo aparece en el principio de la página. En ese caso, es mejor usar un `tab` al inicio de cada párrafo para que los párrafos se distingan entre sí.

---

# Retícula

Se utiliza una retícula cuadrangular cuyo módulo equivale a una vez el tamaño de la tipografía base, es decir, 1rem.

Para configurar esta retícula es recomendado cambiar el formato de unidad de medida en el programa de diseño que se utilize por la unidad `pt` (puntos). Luego al configurar el tamaño de la Grilla, el programa nos dejará poner un valor expresado en puntos, tal como el que se utiliza para la tipografía. 

{{< figure src="/recursos/imagenes/meta/reticula.jpg" title="Ejemplo de texto a 12pt en retícula de 12pt. El interletrado de 1.5rem, —que equivale a 18pt—, produce el efecto de que en cada línea impar, el texto “encaja” en la retícula. (Se utilizó zoom para mostrarlo en detalle)." >}}

Cuando sea necesario limitar el tamaño de la caja tipográfica y dividirla en columnas, el espacio intercolumna es de 1rem.

---

# Íconos

Los íconos deberán tener un estilo consistente entre sí. Esto se dificulta muchas veces al incluir isotipos de otras marcas como íconos (ejemplo: el ícono de WhatsApp, de Facebook,…).

Para asegurar consistencia se eligió el sistema de íconos de Google ([Material Design Icons]). La filosofía de estos íconos se basa en la simplicidad, lo que garantiza que sean leídos incluso a escalas muy pequeñas. Las formas son gruesas, geométricas y planas.

La guía de Diseño Material de Google ([Material Design Guideline](https://material.io/guidelines/style/icons.html#icons-system-icons)) establece dimensiones para el espacio en blanco alrededor de un ícono que equivalen a la mitad de su ancho ó alto. Para ello hay que tener en cuenta que las dimensiones del ícono son mas grandes que las de la forma, porque el ícono de por sí incluye un margen interno.

Las dimensiones del ícono (incluyendo su margen interno) es igual a uno de los posibles valores para el tamaño de la tipografía (150%, 125%, etc. en relación al tamaño del texto base). Por lo general el ícono tiene el mismo color del texto, ya que si se utilizase en color rojo, por ejemplo, se confundiría con un link. Al usarse el ícono a mayores tamaños se considera como un gráfico y ya no es necesario seguir respetando estas especificaciones.

Para encontrar íconos de marcas de terceros (WhatsApp, Instagram, Facebook) reinventados utilizando los parámetros de la Material Design Guideline, se recomienda visitar [icons8](https://icons8.com/) y realizar una búsqueda dentro del set “Material”. Ahí podemos ver inspiración y ejemplos de como aplicar correctamente los bordes, terminaciones y redondeados según lo indicado por la guía del diseño Material.

{{< figure src="/recursos/imagenes/meta/iconos.png" title="Ejemplo del posicionamiento de íconos." >}}

{{< figure src="/recursos/imagenes/meta/alineacion-de-iconos.png" title="Se prestó especial cuidado en que los íconos no estén flotando en el texto sino que formen parte del mismo y por lo tanto, que su tamaño influya en el centrado del mismo. Para ello, a la hora de alinear se trabajó cada línea como un objeto separado. Se centró el conjunto “ícono+texto”. El uso de íconos en la web hace que este trabajo sea mucho más simple, porque el ícono se inserta y comporta como un caracter más." >}}

---

# Isologotipo

El símbolo de la corona, identificante del <abbr title="Mountain Bike">MTB</abbr>, es el fondo y el "marco" del símbolo, que adentro contiene una montaña enmarcando el nombre de la empresa. Bertolina Bikes, como el ciclismo de montaña, es una forma más de "apuntar" hacia las montañas, una mira y un marco en el cual encontrarse con ese espacio. El símbolo-sello está pensado para lograr identificación con la marca a través de su rubro, su actividad.

La tipografía que se utiliza en el logotipo se llama Russo One (descargable desde [Google Fonts](https://fonts.google.com/specimen/Russo+One), [dafont.com](https://www.dafont.com/es/russo-one.font)).

## Versiones permitidas y no permitidas

{{<figure src="/recursos/imagenes/meta/isologotipo-versiones-permitidas.svg" title="Versiones permitidas del isologotipo">}}

## Área mínima de reserva

Área donde se estipula que no deben entrar otros signos marcarios ni texto. Determinada por la altura de la letra B.

![Área mínima de reserva.](/recursos/imagenes/meta/area-minima-reserva.jpg)

---

# Descargas

## Logotipo

El logotipo en formato vectorial se puede encontrar en distintas versiones.

![Tres isologos](/recursos/imagenes/meta/tres-isologos.jpg)

[Sobre un círculo negro.](https://drive.google.com/file/d/0B6OGX_7-Xa0HOGdiUDV6YkZHejg/view?usp=sharing)

[Sobre un círculo blanco (más apto para ahorrar tinta en impresiones y para fondos oscuros).](https://drive.google.com/file/d/0B6OGX_7-Xa0HUlFud2R2bGFQUEE/view?usp=sharing)

[Una versión monocromática (version “sello”).](https://drive.google.com/file/d/0B6OGX_7-Xa0HaFpLbER1b29mU2c/view?usp=sharing)

## Recursos gráficos

![Recursos gráficos](/recursos/imagenes/meta/recursos-graficos.jpg)

Se puede ver y descargar una serie de recursos gráficos en formato vectorial desde [este link][recursos graficos de la marca].

Tres íconos de bicicletas se realizaron inspirándose en el estudio sobre íconos de bicicletas de icons8. Los íconos se pueden ver y descargar en formato vectorial desde [este link][iconos bicicletas]

## Texturas

La madera sin acabado y la tierra son dos elementos claves para unificar la identidad en sus distintas aplicaciones.

La madera se encuentra en forma de textura gráfica en el cartel principal y en papelería, mientras que en el espacio comercial esta textura se encuentra en el mobiliario, que está hecho de palets.

{{<figure src="/recursos/imagenes/meta/folleto.png" title="La textura de madera utilizada para este folleto es gratuita con licencia CC0.">}}

Descargar [textura madera].

La madera del aglomerado, característica de los “palets” de madera, inspiró este patrón que se creó pensando en la web (aunque se puede usar a cualquier tamaño ya que es vectorial). Se puede descargar esta renderización a 256x128px a 96ppp.

{{<figure src="/recursos/imagenes/texturas/textura-aglomerado-256x128.png" title="Textura de aglomerado. Se puede descargar usando la función 'guardar imagen como' del navegador.">}}

La tierra está representada en imagenes de ciclistas. Los caminos de tierra y madera son típicos para quienes quieren practicar esta disciplina de forma competitiva.

Otra textura significativa es el metal propio de ciertas instalaciones industriales. Representa el ambiente de última tecnología en el cual se desarrollan los productos relacionados con el ciclismo. A su vez comunica solidez y durabilidad.

<div class="p2 tc mb2" style="background-image:url(/recursos/imagenes/meta/textura_metal.png)">
    Ejemplo
</div>
{{<figure src="/recursos/imagenes/meta/textura_metal.png" title="La textura de metal. 94x94px. Se puede descargar usando la función 'guardar imagen como' del navegador.">}}

{{<figure src="/recursos/imagenes/texturas/textura-metal-128x128.png" title="Otra versión de la textura de metal. Renderizada en 128x128px. Se puede descargar usando la función 'guardar imagen como' del navegador.">}}

---

Una versión resumida de este manual se puede descargar en formato <abbr>PDF</abbr>:

<https://drive.google.com/file/d/0B6OGX_7-Xa0HTDN5Q2paMUtNU00/view?usp=sharing>

---

Feedback en <francanobr@outlook.es>

Copyright Bertolina Bikes 2017.

[recursos graficos de la marca]: https://drive.google.com/file/d/0B6OGX_7-Xa0HVFRwWVYyY1BHenM/view?usp=sharing
[iconos bicicletas]: https://drive.google.com/file/d/0B6OGX_7-Xa0HVUI2RkhvQVMwY0E/view?usp=sharing
[textura madera]: https://drive.google.com/file/d/0B6OGX_7-Xa0HeUFoNFI2Um5IMkE/view?usp=sharing
[Roboto]: https://fonts.google.com/specimen/Roboto?selection.family=Roboto:400,700
[Material Design Icons]: https://material.io/icons/
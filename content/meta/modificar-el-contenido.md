---
title: "Modificar el Contenido"
date: 2017-08-28T19:07:05-03:00
toc: true
---

# Edición asistida por una interfaz amigable

Para que usuarios menos técnicos puedan editar el contenido se isntaló el gestor de contenidos online "Netlify CMS". Para entrar a este gestor, hay que tipear [bertolinabikes.com.ar/admin](/admin) en la URL e inciar sesión con una cuenta. Por favor, si corresponde, pedir la creación de una cuenta por mail a francanobr@outlook.es

Nótese que esta opción es lenta y los cambios en el sitio web no se pueden ver hasta pasados los 5 minutos después del guardado. Además, si hubiere un error y el sitio no compilare, sería imposible saberlo. Por eso se insiste en probar la alternativa de abajo, que si bien requiere que el editor sepa utilizar "Git", es una alternativa mucho más confiable y rápida.


# Requisitos

- Nociones de cómo funciona un **sistema de control de versiones** (en este sitio se utiliza **Git**).
- Nociones básicas de cómo usar la consola del sistema operativo.

---

# Configurar el ambiente de desarrollo


## 1. Instalar **Git** y **Gulp**.


1-1- Instalar Git de la 
[Página oficial de descargas de Git](https://git-scm.com/downloads) - Nota: El instalador debería tener checkeada la opción para "agregar Git a las variables de entorno del sistema", si no viene checkeada por defecto, marcarla.

1-2- Instalar Node de la
[Página oficial de Node.js en español](https://nodejs.org/es/).


{{% info %}}
Desde que está disponible la opción de administrar las imagenes en la nube, ya no es necesario tener instalado **Node** ni **Gulp**. Podés saltear este paso. Recordá que a la hora de "linkear" imágenes, vas a tener que usar el método descripto en [este artículo](/meta/trabajando-con-cloudinary-para-imagenes)
{{% /info %}}

Para comprobar que está instalado, abrir una consola (**PowerShell** o **cmd**, o similar) y tipeá el comando:

```
npm
```

Si todo sale bien, luego de unos segundos deberías ver algo como esto...

```
> npm
Usage: npm <command>

where <command> is one of:
    access, adduser, bin, bugs, c, cache, completion, config,
    ddp, dedupe, deprecate, dist-tag, docs, edit, explore, get,
    help, help-search, i, init, install, install-test, it, link,
    list, ln, login, logout, ls, outdated, owner, pack, ping,
    prefix, prune, publish, rb, rebuild, repo, restart, root,
    run, run-script, s, se, search, set, shrinkwrap, star,
    stars, start, stop, t, team, test, tst, un, uninstall,
    unpublish, unstar, up, update, v, version, view, whoami

...(continúa)
```

1-3- Una vez que Node.js está instalado, podemos instalar gulp usando el comando:

```
npm install gulp --global
```

## 2. Autenticate en Git

Autenticate sando el nombre de usuario **bbikes** y la contraseña \*\*\*\*\*\*\* (pedir contraseña). Para ello, usá la consola  y escribí los comandos:

```
git config --global user.name "bbikes"
git config --global credential.helper wincred
```

El último comando debería abrir la utilidad para almacenar la contraseña de la cuenta de Git de forma segura.

Alternativamente, puedes utilizar una cuenta de GitHub propia que ya tengas o que quieras crear, dándote permisos de colaborador para el repositorio.

## 3. Obtener el código fuente

Ahora podrías ser capás de copiar el repositorio en una carpeta de tu sistema operativo. Para ello abre la consola y navega hasta la ubicación que quieras.

3-1- En mi caso voy a cambiar de disco "C:" al "F:" y clonaré en la carpeta "F:\git\".

```
F:
cd F:\git\
```

3-2- Luego clonaré el código fuente del sitio en la carpeta "F:\git\bertolina"

```
git clone https://github.com/bbikes/hugo-migration.git bertolina
```

3-3- El código fuente del sitio se debería encontrar en la carpeta "bertolina" y debería tener acceso para realizar commits en el sitio.


## 4. Instalar Hugo

Descargar la versión 0.30 de Hugo para windows desde alguno de estos links:

- [hugo_0.30.2_Windows-32bit.zip](https://github.com/gohugoio/hugo/releases/download/v0.30.2/hugo_0.30.2_Windows-32bit.zip)
- [hugo_0.30.2_Windows-64bit.zip](https://github.com/gohugoio/hugo/releases/download/v0.30.2/hugo_0.30.2_Windows-64bit.zip)
- Consultar la página de [releases](https://github.com/gohugoio/hugo/releases) para obtener la última versión.
- Consultar la guía de instalación (en inglés) si tu sistema operativo es distinto.

4-1- Una vez descargado el archivo (y extraído), una opción es abrir la consola y navegar a la ubicación completa de la descarga, luego ejecutar el archivo `hugo.exe`:

```
cd C:\Users\francisco\Desktop\hugo_0.30.2_Windows-64bit
hugo.exe
```

4-2-  Otra opción es ubicar el archivo en una carpeta del sistema operativo, como puede ser:

```
F:\Apps\Hugo\bin
```

(es importante que la ruta de la carpeta no tenga espacios, por eso no uso la carpeta `Archivos de Programa`)

4-3- Ubicar el contenido descargado en esa carpeta y agregar el ejecutable `hugo.exe` a las varibables de entorno de tu sistema (la variable `PATH`). Para esto hay que usar el panel de control, el proceso varía de acuerdo a cada sistema operativo pero deberían encontrarse muchas guías en internet.


4- Comprobar que Hugo esté instalado y se haya agregado a la variable de entorno `PATH` de tu sistema, para ello:

4-1- tipear el comando

```
hugo env
```

Deberías obtener esta respuesta:

```
> F:\git\bertolina
> hugo env
Hugo Static Site Generator v0.30.2 windows/amd64 BuildDate: 2017-10-31T11:27:26-03:00
GOOS="windows"
GOARCH="amd64"
GOVERSION="go1.9.1"
```

4-1- Para evitar errores, el número de versión de Hugo debe coincidir con el contenido del archivo `netlify.toml` (este archivo le dice al servidor --netlify-- con qué versión de hugo actualizar el sitio).

`netlify.toml`
```
[context.production.environment]
  HUGO_VERSION = "0.30"
```

En el caso de que no coincidan, es recomendado que lo corrigas y hagas un commit.

---

# Empezar el ambiente de desarrollo

## Opción 1: Automático

Automático: Dentro del repositorio (el código fuente descargado por Git), abrir el archivo "serve.bat" haciéndo doble click en el mismo. Luego de unos segundos, el sitio debería poder abrirse desde tu navegador entrando a http://localhost:1313

## 2. Opción 2: Manual

### 2.1. Empezar hugo

Hugo es el generador de sitios estáticos. El comando `serve` sirve un sitio en http://localhost:1313 y al mismo tiempo vigila el contenido de las carpetas dentro del repositorio, actualizándo el sitio web cuando se actualiza el contenido.

```
hugo serve
```

Deberías obtener un _output_ como este:

```
> F:\git\bertolina
> hugo serve
Started building sites ...

Built site for language en:
0 of 1 draft rendered
0 future content
0 expired content
15 regular pages created
42 other pages created
0 non-page files copied
18 paginator pages created
8 categorias created
7 marcas created
total in 576 ms
Watching for changes in F:\git\bertolina\{data,content,layouts,static}
Serving pages from memory
Running in Fast Render Mode. For full rebuilds on change: hugo server --disableFastRender
Web Server is available at http://localhost:1313/ (bind address 127.0.0.1)
Press Ctrl+C to stop
```

Para terminar el script, presionar Ctrl+C

### 2.2. Empezar gulp

{{% info %}}
Desde que está disponible la opción de administrar las imagenes en la nube, ya no es necesario empezar **Gulp**. Podés saltear este paso. Recordá que a la hora de "linkear" imágenes, vas a tener que usar el método descripto en [este artículo](/meta/trabajando-con-cloudinary-para-imagenes)
{{% /info %}}

Gulp es un automatizador de tareas que vigilará el contenido de la carpeta `../static/imagenes/` y generará "thumbnails" (miniaturas) de dichas imágenes, que se suben a la carpeta `.../static/thumbnails/`. Al mismo tiempo, Gulp se utiliza para optimizar algunos archivos, como la hoja de estilos CSS y las librerías JS. (Si no sabés lo que significa CSS o JS, no importa, no es necesario).

En otra ventana de la consola, tipear el comando `gulp` para comenzar el proceso de gulp. Deberías obtener un *output* similar a este:

```
> F:\git\bertolina
> gulp
[10:38:37] Using gulpfile F:\git\bertolina-hugo\gulpfile.js
[10:38:37] Starting 'image-folder-clean'...
[10:38:37] Starting 'images'...
[10:38:37] Starting 'js'...
[10:38:37] Finished 'js' after 19 ms
[10:38:37] Starting 'watch'...
[10:38:38] Finished 'watch' after 402 ms
[10:38:40] Finished 'image-folder-clean' after 2.37 s
[10:39:28] Finished 'images' after 51 s
[10:39:28] Starting 'default'...
[10:39:28] Finished 'default' after 60 μs
```

### 2.3 Actualizar el sitio en el servidor

Ya puedes hacer cambios en los archivos del repositorio y subir imágenes. Los cambios en la página web http://localhost:1313/ sólo serán visibles en tu computadora.

Es necesario que tengas algo de conocimiento previo en Git para actualizar los cambios en el sitio en el servidor.

Para empezar es recomendado abrir la interfaz gráfica integrada de Git. A través de la misma se puede actualizar el contenido del repositorio (realizar un `commit`), así como sincronizar los _commits_ realizados por tí o por otros en otras computadoras, en una interfaz amena.

```
git gui
```

![](https://git-scm.com/book/en/v2/images/git-gui.png)

Hay muchos tutoriales en google y youtube que te pueden ayudar a entender los conceptos básicos de Git como sistema de control de versiones.

---

# Crear o modificar contenido

## Estructura de carpetas

Hugo es un sistema que genera el contenido de las carpetas `content` y `static`. El contenido de estas carpetas el lo único que se debería modificar por el editor del contenido.

Por lo tanto, simplificamos la estructura de carpetas así:

```
- content
  - guias
  - meta
  - novedades
  - productos
- static
  - imagenes
  - logos
  - recursos
  - thumbnails
```

## La carpeta "static"

Dentro de la carpeta `static` se sube todo el contenido estático que se quiera subir al sitio: imágenes, descargables, etc.\
La carpeta `logos` se creó para mantener un repositorio de fotos de los logotipos e isologotipos de las marcas que se trabajan.\
La carpeta `recursos` está reservada para archivos que hacen que el sitio funcione, como hojas de estilo, texturas, librerías _javascript_, entre otros.\
La carpeta `imagenes` contiene las imágenes en alta calidad (1200px de anchura o altura) que se quieran usar para los productos u otras publicaciones. Es importante aclarar que no se pueden crear sub-carpetas dentro de esta carpeta.\
La carpeta `thumbnails` contiene contenido generado automáticamente por Gulp. Se trata de los archivos .jpg, .png y .gif de la carpeta `imagenes` que son redimensionados a un tamaño de no más de 360px de altura.

Para subir un archivo a la carpeta `static/imagenes` primero hay que redimensionarlo a un tamaño óptimo para la web: no mayor a los 1200px de ancho y/o alto. Para ello se creó un programa que se puede utilizar desde el navegador que puede realizar esta tarea en muchas imágenes de forma simultánea.

[Redimensionador de imágenes](/meta/imgrsizr/)

## La carpeta "content"

Dentro de la carpeta `content` se encuentra el contenido del sitio, en forma de archivos `.md` o `.html`. Cada sección del sitio tiene su sub-carpeta, incluida la carpeta "meta" que contiene información extra que no es subida a los motores de búsqueda.

En la próxima sección se explicará cómo modificar cada tipo de contenido.

# Agregar o editar productos

Los productos, ubicados en la carpeta `content/productos/`, son archivos `.md` o `.html` con una serie de **metadatos** que permiten formalizar una serie de datos sistemáticos que se repiten, como lo son las marcas, o los links a mercadolibre.

## Crear un producto
Para crear un producto vamos a aprovechar el comando `hugo new` que nos permite generar archivos para contenido y que se llene automáticamente una serie de datos necesarios.

```
hugo new productos/nombre-del-producto.md
```

Navegamos hasta `content/productos/nombre-del-producto.md` y lo abrimos con un editor de texto, como **bloc de notas** para Windows (se recomienda usar **Visual studio code** o **sublime text**, ambos se pueden descargar gratuitamente). Veremos el siguiente contenido:

```
---
title: "Nombre Del Producto"
date: 2017-10-31T12:39:08-03:00
description: ""
imagen: "thumb.jpg"
categorias: ["categoria1", "2"]
marcas: ["venzo", "shimano"]
envio_gratis: false
---

# Descripción


# Características
- ...

# Colores
- ...

# Rodados
- ...
```

Vamos a guardar el archivo como está, y comprobamos que la ventana del navegar web se actualiza. Tu producto ya estará disponible para ser visitado en el link: http://localhost:1313/productos/nombre-del-producto/

## Los metadatos

Los metadatos son una serie de variables en formato "yaml".

Para entender los metadatos y cómo funcionan, prueba cambiar el título del producto y fíjate cómo se acutualiza.

```
---
title: "Nuevo título"
...(el resto de los metadatos)
```

También puedes hacer que el producto desaparece del listado de productos, usando la variable "draft" (borrador). Al declararar esta variable, le decimos a Hugo que nuestro producto aún no está listo para ser publicado, por lo tanto simplemente lo ignora y no lo enlista junto con los otros productos.

```
---
title: "Nuevo título"
draft: true
...(el resto de los metadatos)
```

### El metadato "description"

Este es un metadato común a todos los tipos de contenido y determina lo que google mostrará en su carta de resultado como la "descripción" del contenido.

![](http://www.openvine.com/images/small-business-internet-blog/meta-description-example.jpg)

Un ejemplo:

```
description: "Bicicleta para Mountain Bike con cuadro de aluminio 6061 y componentes de Shimano. Marca: Venzo. Modelo: Amphion."
```

### El metadato "imagen"

{{% info %}}
Desde que está disponible la opción de administrar las imagenes en la nube, este paso se puede realizar de otra forma. [Más información en este artículo](/meta/trabajando-con-cloudinary-para-imagenes)
{{% /info %}}

Este metadato se comporta de forma especial en los productos. Debemos introducir el nombre de un archivo que se encuentre bajo la carpeta `static/imagenes/`. **Debe existir un thumbnail válido con el mismo nombre de archivo en la carpeta `static/thumbnails/`**.\
Este metadato controla la imagen de fondo arriba del título del producto ("imagen de portada"). Es opcional (si no hay ninguna imagen disponible, se puede eliminar).

En este ejemplo se asocia a los archivos `static/imagenes/thumb.jpg` (grande, se ve en la página del producto) y `static/thumbnails/thumb.jpg` (chico, se usa para la página del listado de productos):

```
imagen: "thumb.jpg"
```

### El metadato "weight"

Representa el peso relativo del producto, lo cual influye en el orden en el que el mismo se muestra. Mientras menos peso tenga, primero se mostrará. Este parámetro precede en orden de importancia a la fecha, que también es importante.

En los productos existentes, para determinar el valor de `weight`, se utilizó la cantidad de ventas de cada producto, en mercadolibre, como valor negativo de weight. De esta forma los productos más vendidos aparecen primero.

### La taxonomía "categorias"

En esta parte se pueden introducir las categorías (existentes o nuevas), como un listado de ítems en el formato `[ "categoria1", "categoria2" ]`...

Para cada categoría, se creará una nueva página que agrupa los productos con dicha categoría, como por ejemplo: [/categorias/bicicletas/](/categorias/bicicletas/).

Es importante no repetir categorías de forma innecesaria para no crear contenido duplicado. Por ejemplo, por convención todas las categorías se escriben en plural, lo cual ayuda a que no creemos dos categorías para lo mismo ("bicicleta" y "bicicletas").

Un producto puede tener tantas categorías como hagan falta, sin embargo en la página del producto se podrían mostrar sólamente las primeras tres, debido a que el espacio y la atención del usuario son recursos valiosos que se aprovechan al máximo.

Ejemplo:

```
categorias: ["bicicletas"]
```

### La taxonomía "marcas"

Similar a la taxonomía "categorías", permite una lista de una o más marcas que están relacionadas con el producto. Esta taxonomía le permite a los usuarios navegar y encontrar productos con marcas iguales a las del producto que está viendo. También se usa para generar una lista de marcas en [/marcas/](/marcas/).

Al igual que con las categorías, puede haber tantas marcas como sea necesario, o puede obviarse por completo y borrar este metadato. En la página del producto se mostrarán sólo las primeras tres.

Ejemplo:

```
marcas: ["venzo", "shimano"]
```

### El metadato "envio_gratis"

Este metadato se usa de forma opcional para indicar si el envío se realiza de manera gratuita.

Al estar en `false` o al no estar presente, el sitio muestra los métodos de envío normalmente.

Al estar en `true`, el sitio muestra "Envío gratis a la sucursal de OCA más cercana - Envío a domicilio por $300."

Ejemplo:

```
envio_gratis: true
```

### El metadato "garantía"

Se debe incluir entre comillas el número de la cantidad de meses por los cuales la garantía es válida para que al final de la publicación se añada el escudo de "Garantía oficial X meses".

Ejemplo:

```
garantia: "6"
```

### El metadato "mercadolibre"

Este metadato opcional se utiliza para _linkear_ una publicación a un producto de mercadolibre, por su **link** y su **título**.

```
mercadolibre: [ {link: "link", titulo: "Título"} ]
```

Sin embargo el mismo se puede utilizar para **linkear un único producto a varias publicaciones de ML**.

Ésto es un poco complejo porque introducimos la idea de una lista de listas. Hay que tener cuidado al trabajar con metadatos porque un error de sintaxis podría hacer que el sitio no se actualice.

Para mostrar el correcto formato que tiene que adquirir esta lista de datos, no hay mejor forma que con un ejemplo:

```
mercadolibre: [ {link: "link1", titulo: "Título1"}, {link: "link2", titulo: "Título2"} ]
```

Un formato alternativo permitido (se usan dos espacios para expresar jerarquía):

```
mercadolibre: 
  - link: "link1"
    titulo: "Título del link 1"
  - link: "link2"
    titulo: "Título del link 2"
  - link: "link3"
    titulo: "Título del link 3"
```

### El contenido

El contenido del producto, es decir, todo lo que compete a su descripción (sus características, los rodados disponibles y las imágenes del mismo), se escribe abajo de los metadatos en formato MARKDOWN (si la extensión del archivo es `.md`) o HTML (si la extensión del archivo es `.html`).

### Algunos apuntes sobre Markdown

- Los títulos están precedidos por un hashtag y un espacio.
- Las lístas se pueden empezar con un guión y un espacio, un asterisco y un espacio, un más y un espacio, entre otros.
- Para introducir un salto de línea, usar "DOS ENTERS". Un <kbd>enter</kbd> sólo no crea un salto de línea.
- Las comillas se convierten a "comillas angulares" y se crean automáticamente fracciones cuando los números están divididos por una barra, como el 1/2.
- El texto entre dos asteriscos se muestra en negrita y el texto entre un par de asteriscos se muestra en cursiva. Se pueden reemplazar los asteriscos por guiones bajos.
- Se pueden usar etiquetas HTML.

```

Texto de párrafo.
"Esto está en comillas" y esto es fracción de 1/2.

Otro parrafo (separado por dos enters).

- elemento de lista
- elemento de lista 2

Texto en **negrita**, *cursiva*.

Se pueden usar etiquetas<sup>[1]</sup>
<abbr title="Hypertext Markdown Language">HTML</abbr>.

<small>[1] Las etiquetas permiten personalizar completamente el formato del contenido.</small>

```


Texto de párrafo.
"Esto está en comillas" y esto es fracción de 1/2.

Otro parrafo.

- elemento de lista
- elemento de lista 2

Texto en **negrita**, *cursiva*.

Se pueden usar etiquetas<sup>[1]</sup> <abbr title="Hypertext Markdown Language">HTML</abbr>.

<small>[1] Las etiquetas permiten personalizar completamente el formato del contenido.</small>


### Introducir una galería de imágenes

{{% info %}}
Desde que está disponible la opción de administrar las imagenes en la nube, este paso se puede realizar de otra forma. [Más información en este artículo](/meta/trabajando-con-cloudinary-para-imagenes)
{{% /info %}}

Se puede usar un metadato llamado galería para generar la galería en cuestión. Las imágenes se suben a la carpeta "static/imagenes" y la tarea "gulp" debería generar automáticamente un thumbnail (versión más pequeña) de las mismas, de lo contrario habría que generarlas de forma manual.

A tener en cuenta:

- Siempre debes anotar el tamaño de las imágenes que quieres introducir. Una imágen de 1200px de ancho y 900px de alto es "1200x900".
- El atributo `size` en `{{%/* galleryimage */%}}` contiene el tamaño de la imagen. Es muy importante, si no está especificado, cuando agrandemos la imágen se tildará el navegador web y será necesario actualizar la página para seguir navegando. **No olvidar este atributo y probar siempre las galerías antes de subir los cambios al servidor**.
- El atributo `description` es opcional, sin embargo es recomendado porque provee un indicador de que hay una imagen cuando --por cualquiera sea la razón--, el thumbnail no se descarga correctamente, permite a usuarios de tecnologías asistivas como los **lectores de pantalla** el navegar por la web como cualquier otro usuario y además permite a los buscadores indexar el contenido de la imágen en sus motores de búsqueda de imágenes.


```
gallery:
  - file: "maxxis crossmark (1).jpg"
    size: "900x1200"
    description: "Cubierta Maxxis Crossmark 27.5x2.25"
  - file: maxxis crossmark (2).jpg
    size: "900x1200"
    description: "Cubierta Maxxis Crossmark 27.5x2.10"
  - file: "cubiertas-maxxis-crossmark-29x225-tubeless-ready-exo.jpg"
    size: "900x1200"
    description: "Cubierta Maxxis Crossmark 29x2.25 Tubeless Ready Exo"
```

El thumbnail y el archivo original tienen el mismo nombre, pero una está en la carpeta "static/thumbnails" y la otra en la carpeta "static/imagenes".
Nótese que al referenciar la imagen, sólo debes hacerlo por el nombre del archivo (omitir el "static/imagenes/").

Si en cambio quieres incluir la imagen en el cuerpo de la publicación, debes incluir la ruta completa, pero se omite de la ruta la carpeta "static". Esto puede ser útil para productos que tengan una sóla imagen, donde no es necesario incluir el widget de galería (que requiere cargar galerías externas y hace la página más pesada).

```
![Piñón Deore](/imagenes/pinon-shimano-deore-m-6000.jpg)
```

# Agregar o editar "guías" o "novedades"

Si bien la cantidad de metadatos disponibles se reduce, los metadatos que sí funcionan lo hacen de la misma forma.

Para la sección "Guías" y "Novedades" sólo se puede utilizar el metadato `description` con funcionamiento exactamente igual al de la sección productos.

El metadato `imagen` también se puede utilizar, sin embargo es obligatorio realizarlo mediante el método de insersión de imágenes mediante Cloudinary, documentado extensivamente en [este artículo](/meta/trabajando-con-cloudinary-para-imagenes/). Por lo tanto el metadato `imagen` siempre estará acompañado por el metadato `cloudinary`.

## Agregar o editar Novedades

Esta guía se movió a una URL propia: [click aquí](/meta/diseño-de-contenido-para-novedades/)


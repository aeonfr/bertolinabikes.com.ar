---
title: "Categorías de Productos"
date: 2017-11-20T14:25:13-03:00
draft: false
---

# Categorías de Productos

Los productos tienen una clasificación taxonómica llamada "Categorías". Inicialmente se copiaron las mismas categorías taxonómicas que se usaban en MercadoLibre (de donde se migró el contenido), sin embargo resultó evidente que no era un sistema coherente ni confiable. Por ejemplo, las luces de la bicicleta, los cascos, los cuadros y herramientas específicas para el taller como el "bike hand" o el lubricante de cadenas, quedaban todos en la misma categoría "accesorios". Otra categoría que causó confusión es la de "repuestos". Para unificar las categorías se diseñó una forma de clasificar que considera la limitación del cerebro de retener sólo entre 5 y 9 unidades de información (Miller, 1956[^ley de miller]).

{{% info %}}
Este límite no imposibilita que utilices otra categoría, sólo es una recomendación.
{{% /info %}}

Se definieron 6 categorías:

- **Bicicletas**
- **Herramientas** (todo lo relacionado con el taller, desde aparatos para sostener la bicicleta, como el bike hand, hasta insumos que van a ser almacenados allí, como pueden ser la grasa para cadena, parches para rueda, llaves allen, etc.)
- **Componentes** (todas aquellas partes indispensables para que la bicicleta ande: cuadro, rueda, manubrio, etc.)
- **Indumentaria** (incluye cascos y lentes)
- **Accesorios** (todos aquellos elementos que se anexen a la bicicleta o se usen para salir a pedalear, pero que no entren directamente en la categoría "indumentaria" ni en la categoría "componentes", como pueden ser: cantimploras, bolsos que se anexen a la bicicleta, kits portables de mantenimiento de la bicicleta, ciclocomputadores, entre otros)

Si bien cada producto debería entrar en una de estas categorías ya mencionadas, se pensó también en categorías adicionales, en productos que tendrán seguramente más de una categoría. Por ejemplo, el casco va en la categoría "indumentaria" y también en la categoría "seguridad"
- **Seguridad** (todo lo relacionado con la seguridad, como cascos, luces, etc.)
- **Repuestos** (todo lo que deba ser cambiado más o menos de forma regular, como líquidos para el mantenimiento, cadenas, cámaras, luces que funcionen a pilas, etc.)

[^ley de miller]: George A. Miller, psicólogo de la cognición, publicó en 1956 el libro "The Magical Number Seven, Plus or Minus Two: Some Limits on Our Capacity for Processing Information", un estudio sobre los límites de la memoria a corto plazo. https://en.wikipedia.org/wiki/The_Magical_Number_Seven,_Plus_or_Minus_Two
---
title: "Proyecto manual de uso bicicletas y sitio web"
date: 2017-08-19T19:50:10-03:00
draft: false
---

# TODO

- sacar foto de día del manual de armado y del cartel de horarios
- escribir la conclusión
- hacer la biografía


<div style="page-break-after:right"></div>

# Acerca de

Este proyecto se presentó en noviembre del 2017 como trabajo final para la Tecnicatura Superior en Diseño Gráfico y Publicitario.

**Universidad Provincial de Córdoba**\
**Facultad de Arte y Diseño**\
Carrera: **Diseño gráfico y Publicitario**\
Materias: **Proyecto Final**, **Tecnología del Diseño** y **Gestión del Diseño**\
Profesoras/es: **Pía Reynoso**, **Belén Voget** y **Martín Mazzei**\
Alumno: **Francisco Cano**

Un especial agradecimiento a todos los profesores y al comitente (Eugenio Bertolina) y los empleados de la empresa (Nicolás y Maximiliano), que ayudaron y aportaron ideas en todas las etapas de este proceso e hicieron el proyecto posible.

<div style="page-break-after:right"></div>
# Introducción

> “El diseño consiste en hacer filosofía con las manos.”
>
> — Allan Chochinov (2014)[^wendt].

Este proyecto espera ser tanto una demostración del campo de acción del diseñador gráfico como la demostración de los aspectos pragmáticos que se pueden extraer de diversas filosofías, métodos y herramientas de investigación y testeo para proyectos de diseño.

Al aplicar contenidos teóricos a la práctica, se espera también que el resultado de la *praxis* nutra a la teoría. La división entre teoría y práctica suele dar la idea de un espacio puro de afirmaciones “universales”, y otro espacio donde esas universalidades se expresan en lo particular. Esta división no existe en la realidad, por lo tanto, no se puede hablar de lo universal sin ejemplificarlo en lo particular (Thomas Wendt, 2005).

Los métodos aplicados y las conclusiones obtenidas en este trabajo no serán formulas universales, sino más bien recomendaciones que podrían ser aptas para casos similares a éste. Es en este sentido que este proyecto, espero, sea de interes para todos aquellos que quieran aproximarse al diseño desde métodos similares a los usados acá, o para problemas similares.

Este proyecto está estructurado de acuerdo a la metodología del **Design Thinking**, filosofía de diseño que tiene como eje el correcto planteamiento del problema y de la solución.

Principalmente se derivan dos métodos de esta filosofía: el diseño centrado en el usuario (*<span class="nowrap">Human-Centered</span> Design*, o <abbr>HCD</abbr>, por sus siglas en inglés) y el modelo de doble diamante <span class="nowrap">divergente-convergente</span><sup id="norman1"></sup>.

El <abbr>DT</abbr> primero investiga al público del producto o servicio y busca las causas primeras de sus problemas o necesidades, el último *por qué* en la cadena de <span class="nowrap">causa-consecuencia</span>. Antes de concentrarse en una solución, busca la mayor cantidad de alternativas posibles para compararlas. No se busca una solución “correcta” porque no existe tal cosa, pero sí existirá una solución mejor comaprada con el resto de las alternativas.

El **modelo de doble diamante** divide al proceso en dos grandes pasos: encontrar el problema (correcto), y encontrar la solución (correcta). Este proceso se puede graficar como dos diamantes en donde se convergen y divergen alternativas del problema y de la solución.

<svg aria-label="Gráfico de doble diamante. El primer diamante dice: 'Encontrar el problema correcto'. El segundo dice: 'Encontrar la solucion correcta'. El diamante representa una primera etapa en la que se convergen en alternativas (tanto en la parte del problema como de la solución surgen muchas alternativas posibles) y en la que luego se diverge en una única alternativa (un único problema, el problema 'base', y una única solución, la que es mejor a todas las demás)." width="100%" viewbox="-20 -30 220 140" style="break-inside-avoid break-before-avoid">
    <text font-size="8" x="50" y="-10" text-anchor="middle" style="bolder">Encontrar el Problema</text><text font-size="8" x="150" y="-10" text-anchor="middle" style="bolder">Encontrar la Solución</text><path d="M 0 40 L 50 0 L 100 40 L 150 0 L 200 40 L 150 80 L 100 40 L 50 80 L 0 40" stroke="black" fill="transparent" /><text font-size="7" x="-40" y="-7.5" transform="rotate(-90)" text-anchor="middle">Alternativas</text><path d="M -2.5 0 L -2.5 80" fill="transparent" stroke-width="0.5" stroke="grey" /><path d="M -5 75 L -2.5 80 L 0 75" fill="transparent" stroke-width="0.5" stroke="grey" /><path d="M -5 5 L -2.5 0 L 0 5" fill="transparent" stroke-width="0.5" stroke="grey" /><text font-size="7" x="25" y="100" text-anchor="middle">Convergencia</text><text font-size="7" x="75" y="100" text-anchor="middle">Divergencia</text><text font-size="7" x="125" y="100" text-anchor="middle">Convergencia</text><text font-size="7" x="175" y="100" text-anchor="middle">Divergencia</text>
</svg>

> **El Modelo de Diseño de Doble-Diamante.** Empieza con una idea, y a través de la investigación en diseño inicial, se expande el pensamiento para explorar los problemas estructurales subyacentes. Sólo entonces es tiempo de converger en el problema real. Similarmente, se usan técnicas de investigación en diseño [*Design Research*] para explorar una amplia variedad de soluciones antes de converger en una específica.
>
> — Don Norman[^norman1]

A pesar de que el doble diamante permite entender visualmente el proceso del *Design Thinking*, puede causar la impresión errónea de que éste es un método lineal y se llega a un resultado concreto. En cambio, el proceso es más bien circular, iterativo. Por eso se lo llegó a describir como un espiral que se expande, representando que en cada nueva iteración se puede expandir el conocimiento del problema y llegar más cerca de la solución deseada.

El **Diseño Centrado en el Usuario** (<abbr>HCD</abbr>) es otro método derivado del <abbr>DT</abbr> que describe más en detalle estas cuatro fases de convergencia/divergencia. Don Norman (2013) nombra estas fases o etapas como “Observación”, “Generación de Ideas”, “Prototipado” y “Testeo”; mientras que en el libro Designpedia (2015) se mencionan con los verbos "Mapear", "Explorar", "Construir" y "Testear". Son también las cuatro grandes etapas que estructuran este proyecto:  la Etapa de Investigación, la Etapa Creativa, la Etapa Ejecutiva y los Resultados y Conclusiones.


<div style="page-break-after:right"></div>

# Etapa de Investigación

<div class="page-break"></div>

## Bertolina Bikes

**Bertolina Bikes** es un negocio dedicado a la **venta de equipamiento para ciclismo en Argentina**. Ubicado en la ciudad Capital de Córdoba, en Barrio Observatorio. El comitente es el dueño y jefe del negocio, donde también trabajan dos empleados.

Dentro de los productos para ciclismo que se pueden encontrar en el negocio, hay una mayor cantidad para el **ciclismo de montaña**, si bien se venden componentes que sirven para todo tipo de ciclistas (incluso para usuarios de la bicicleta como medio de transporte).

El negocio cuenta con un espacio comercial y una **tienda online** que funciona por MercadoLibre. Además cuentan con presencia en las redes sociales, tales como **Instagram** y **Facebook**. Las redes sociales, por su espontaneidad, sirven como medio de venta y son óptimos como medio de comunicación. 

El **espacio comercial**, sobre la Avenida Pueyrredón al 1506, está alquilado desde octubre del 2015, momento en que el modelo de negocio cambió: se pasó de la venta exclusivamente online a la venta tanto online como presencial.

<svg class="break-before-avoid break-inside-avoid" viewbox="0 0 100 50" width="100%">
<image xlink:href="/proyecto/2017-08-08-logotipo.svg" x="0" y="0" width="25" height="25" />
<image xlink:href="/proyecto/2017-07-11-frente-local.jpg" x="25" y="0" width="75" height="50" />
</svg>

## Estudio de mercado

Como forma de aprender más sobre la empresa, sus clientes y su competencia, se realizó un estudio de mercado, con un especial incapié en la definición del nicho y del nicho ideal. Entender las motivaciones, deseos y necesidades de los clientes de la empresa es un foco importante en esta instancia, por lo tanto se incluyó un análisis sobre la disciplina del ciclismo de montaña, las distintas formas de realizar esta actividad y los distintos tipos de bicicleta que pueden usarse.

### Público

El público consiste en todas las personas que tengan o desean tener una bicicleta. De los que ya tienen bicicleta se pueden dividir en quienes estén buscando renovarla como quienes estén buscando renovar un componente de la misma o añadirle una parte. También debemos tener en cuenta a aquellos que quieren comprar bicicletas o productos de ciclismo como regalo hacia un ser querido. Finalmente, es también parte del público aquellas personas que compran bicicletas o productos de ciclismo para revenderlos.

El público utiliza la bicicleta de forma variada: para practicar **ciclismo de montaña**, practicar **otras formas de ciclismo**, de forma recreativa o **turística**, o como **medio de transporte**. Cada uno de estos usos se analiza a continuación.

#### El ciclismo de montaña

![imagen ilustrativa del ciclismo de montaña. Un ciclista saltó de una rampa y está en el aire, donde realiza una maniobra](/proyecto/mountain-bike.jpg)

El ciclismo de montaña (<abbr>MTB</abbr>) es un deporte de riesgo que se practica al aire libre. Engloba diversas formas de fluir sobre la bicicleta en un ambiente natural, o “semi-natural” (el espacio natural se amplía con artificios tales como rampas de madera, senderos de tierra, rampas de tierra, etc.).

Se subdivide en disciplinas más específicas, como el *Cross-Country*, *Uphill*, *Downhill*, *Enduro*, etc[^wikiCiclismoMontania]. En cada sub-disciplina las exigencias, tipos de terreno e intensidad son distintas, por lo tanto se requiere de equipamientos distintos. Sin embargo para quienes empiezan a practicar cualquier tipo de <abbr>MTB</abbr>, una bicicleta rígida (las más “comunes”[^bicicletas-mas-comunes]) es la mejor opción considerando la relación costo-calidad.

[^bicicletas-mas-comunes]: Las bicicletas más comunes serían bicicletas rígidas con freno a disco, cuadro de aluminio (el cuadro puede también llamarse “marco”), suspensión delantera, ruedas con llanta doble pared y cambios para 27 velocidades. Según lo que más se vende y lo que más buscan los clientes en el espacio comercial. Información obtenida por observación directa y conversación con el comitente.

De acuerdo a cada subdisciplina, y a la exigencia que se le va a dar, las **bicicletas** necesarias para practicar el deporte pueden variar entre estos dos tipos:

- **Rígidas**. Bicicletas que incluyen suspensión delantera (“horquilla delantera”).
- **De doble suspensión**. Bicicletas que además de contar con horquilla delantera tienen amortiguador detrás. A pesar de ser más pesadas y difíciles de mantener, proveen mayor seguridad y comodidad al descender o ascender en la montaña.

Otra variable importante es el grosor de la llanta de las ruedas y el material del marco (que determinará el peso)[^subdisciplinas1].

<figure>
<svg class="break-after-avoid break-inside-avoid" viewbox="0 0 118 50">
<image x="0" y="0" width="50" height="50" xlink:href="/proyecto/marco-venzo-amphion.jpg" /><image x="50" y="0" width="68" height="50" xlink:href="/proyecto/marco-venzo-exceed-pro.jpg" />
</svg>
<figcaption class="break-before-avoid"><p>El marco de la Venzo Amphion, a la izquierda, para una bicicleta rígida. A la derecha, el marco de la Venzo Exceed, de doble suspensión. Imágenes obtenidas de <a href="http://venzoargentina.com">venzoargentina.com</a></p></figcaption>
</figure>

#### Otros tipos de ciclismo

Si bien el local se especializa en ciclismo de montaña, también hay productos y bicicletas para usuarios que practican **ciclismo de ruta** y **<abbr>BMX</abbr>**.

La diferencia fundamental entre el ciclismo de montaña y el de ruta es que en el segundo la bicicleta está preparada especialmente para andar a gran velocidad en la ruta, con llantas más finitas y con un manubrio que exige una posición distinta del usuario.

El **ciclismo de ruta** o ciclismo de carretera se practica de forma profesional y competitiva (como en el *Tour de France*). Para competir es necesaria una bicicleta de ruta (“bici rutera”). Cuando el deporte se practica de forma no competitiva (como forma de recreación y esparcimiento), el tipo de bicicleta que se utilice no pasa a ser tan importante: es posible practicar ciclismo de ruta con una bicicleta de <abbr>MTB</abbr>. También se da el caso de bicicletas “híbridas” que tienen algunas caracterísiticas de las bicicletas para <abbr>MTB</abbr> y otras caracterísitcas de bicicletas ruteras.

Otro tipo de ciclismo es el ciclismo <abbr>BMX</abbr>, que requiere de un tipo de bicicleta más pequeña y más ligera, que se puede maniobrar fácilmente y permite la realización de “trucos” o maniobras.
Este tipo de ciclismo se puede practicar en pistas especiales (similares a las de patinaje) o, en su defecto, en obstáculos urbanos (plazas, parques).

Los componentes de las bicicletas (manubrio, casco, pedal...) son compartidos —en la mayoría de los casos— por todos los tipos de ciclismo, por eso incluso los usuarios de bicicletas genéricas o no profesionales pueden encontrar productos útiles en el comercio.

<figure>
<img src="/proyecto/tipos-de-bicicleta.svg">
<figcaption>
<p>De izquierda a derecha: bicicleta para <abbr>BMX</abbr>, bicicleta “rutera” y bicicleta para <abbr>MTB</abbr>.</p>
</figcaption>
</figure>

#### Cicloturismo: el ciclismo como fenómeno turístico

Existe un fenómeno muy interesante en donde el ciclismo pasa a ser una actividad recreativa, saludable y ecológica, que combina la actividad física y el turismo[^wikiCicloturismo].

El cicloturismo consiste en realizar un circuito a lo largo de varias poblaciones, donde se eligen principalmente caminos de tierra. Los circuitos largos generalmente duran varios días y requieren mayor organización, por eso entre quienes practican esta forma de ciclismo es común encontrar que todos los fines de semana salen a hacer circuitos cortos que duran un día, pero realizan escapadas de varios días con una frecuencia mucho menor.

Por ejemplo, un cliente comentó en una entrevista informal que los circuitos que realizan todos los sábados (son un grupo que varía de entre 10 a 20 ciclistas) duran desde las 2 hasta las 6:30 de la tarde. Ese mismo cliente realizaba escapadas de 2 o 3 días con una frecuencia mucho menor: una o dos veces al mes. En los circuitos de varios días, hay quienes deciden llevar elementos para acampar y otros que prefieren contratar un alojamiento para pasar la noche y descansar.

En la plataforma es.wikiloc.com usuarios de toda Argentina publicaron distintas rutas bajo la categoría “cicloturismo”, que sólo dentro de la provincia de Córdoba varían entre rutas de 17km (se puede hacer en una tarde) hasta rutas de 179km (recorrido donde hay que acampar, 4 días)[^wikilockCicloturismo1]. Esto nos indica que el cicloturismo es un fenómeno diverso y que requiere una investigación de mayor profundidad para ser abarcado en su totalidad.

<svg viewbox="0 0 100 30">
<image xlink:href="/proyecto/ruta-cicloturismo-17km.png" x="0" y="0" width="58" height="30" />
<image xlink:href="/proyecto/ruta-wikiloc-cicloturismo-foto1.jpg" x="59" y="0" width="41" height="30" />
</svg>
<p class="f5 px2"><em>Ruta de 17km que llega hasta la cima de El Condor (de 465 a 2146 metros de altura). La foto de la derecha fue subida por el autor de la publicación (@PSK74), que realizó el recorrido en octubre de 2015. “Mucho frío, nevadas y lloviznas hasta subir a El Condor”[^wikilocRuta17km].</em></p>

> “Los motivos que impulsan a la práctica del cicloturismo son variados, pero, en general, es buscar un ‘cable a tierra’ a la exigida vida actual. 
>
> Y el andar en bicicleta, con mayor o menor esfuerzo, se disfruta. Sobre todo en grupo de amigos y en contacto con la naturaleza. Es que la bicicleta para muchos representa hermosos recuerdos de la infancia y según el recorrido es pródiga en adrenalina.”
>
> — Gustavo Scarpettta (2014)[^blogvoydeviaje]

[^blogvoydeviaje]: Gustavo Scarpettta (2014), “Apasionados por el cicloturismo”, Blog Voy de Viaje, Recuperado de <http://www.voydeviaje.com.ar/cordoba/apasionados-por-el-cicloturismo>

#### Moverse en bicicleta

Si bien no es técnicamente una forma de ciclismo ya que no es considerado un deporte, el uso de la bicicleta como medio de transporte también es importante a tener en cuenta. La tendencia parece indicar que esta práctica va a ser cada vez más común.

Los últimos datos que se recolectaron en Argentina indican que un <span class="nl">6%</span> de la población se mueve en bicicleta y un <span class="nl">4%</span> la utiliza para ir a trabajar (datos de marzo del 2017 y octubre del 2016, respectivamente)[^estadisticasArg1].

En las grandes ciudades el uso de bicicletas es cada vez más una política de estado. En Capital Federal se registró un alza del <span class="nl">749%</span> en la cantidad de viajes en bicicleta, en el período entre 2007 y 2013[^estadisticasArg2]. Durante este período hubo una gran ampliación del sistema de bicisendas y se implementó un sistema de alquiler de bicicletas públicas. Rosario es otra ciudad grande que también cuenta con un sistema de alquiler de bicicletas públicas y las calles principales de la ciudad se cierran los domingos. Estas políticas buscan aumentar la calidad de vida de los ciudadanos al promover el ejercicio y reducir la contaminación, aunque también tienen efectos secundarios positivos, como fomentar el turismo.

A pesar del camino que se empezó a transitar en Buenos Aires y en Rosario, el colectivo y el auto particular siguen dominando las calles. ¿Estos datos representan una tendencia sólida, duradera, o son el producto de una moda pasajera? Todo parece indicar que la tendencia es estable y seguiría habiendo cada vez más gente que opte por este medio de transporte.

### Público Objetivo

El público objetivo consiste en personas que utilizan la bicicleta principalmente para practicar: ciclismo de montaña, de ruta o cicloturismo.

#### Perfil demográfico

Para la obtención de datos demográficos se utilizaron los datos de la ubicación de los visitantes a publicaciones de mercadolibre, mediante el rastreo de su ubicación por IP, método que si bien es fácil de eludir y muy poco confiable, puede arrojar datos lo suficientemente fiables como para hacer un estimativo. Se encontró que los usuarios de esta plataforma eran mayormente de Buenos Aires, llegando a superar el 20% de las visitas totales, mientras que le seguían Córdoba (9%) y Neuquén (6%). El resto de las visitas se originó de localidades diversas que abarcan casi todas las provincias de Argentina.

{{< figure src="/proyecto/mapa-2.jpg" title="Datos geográficos, visitas a publicaciones de MercadoLibre desde el 6/08/2017 hasta el 12/08/2017" >}}

Otro medio de recolección de datos utilizado fueron las redes sociales. **Instagram** nos brinda estadísticas interesantes gracias al servicio que brinda a sus anunciantes. La cuenta todavía es relativamente nueva al momento de captura de los siguientes datos (12/08/2017).

{{< figure src="/proyecto/datosinstagram.jpg" title="Datos obtenidos de Instagram, 12/08/2017" >}}

#### Perfil psicográfico

Describir el público objetivo psicográficamente no es una ciencia exacta. Se puede decir que son personas que buscan una serie de valores en sus productos: calidad, performance (buscan bicicletas a las que se les pueda “exigir”), durabilidad, entre otras cosas. Estas personas saben por lo que están pagando, están informados sobre la gama de productos —, sus funciones, sus colores, etc.— y sobre las distintas marcas.

<!-- No va .... A veces en una misma marca hay líneas de productos distintos con conceptos distintos: Shimano Alivio es la línea de productos centrado en la potencia –orientada a ciclistas de <abbr>MTB</abbr> que lo hacen de forma recreativa–, Shimano Acera es otra línea que se centra en el estilo y Shimano <abbr>DXR</abbr> promete la calidad idónea que requieren los atletas olímpicos[^shimano]. -->

Como estas personas utilizan también la indumentaria del ciclista, se podría afirmar que el estilo es realmente importante: dentro de los diversos componentes de la bicicleta, la indumentaria y los accesorios debe existir una unidad y coherencia. Este lenguaje estético tiene que ver con el aerodinamismo, la potencia, el impacto y la velocidad. Se ve en la morfología de los volúmenes de los cambios y demás componentes, las terminaciones cromadas y la decoración con colores fuertes (a veces flúo) que suelen tener diseños de simil-flechas con tensión hacia adelante.

<figure>
<svg viewbox="0 2.5 100 94.5">
<image x="0" y="2" width="50" height="34" xlink:href="/proyecto/mtb-aesthetics-1.jpg" />
<image x="50" y="0.75" width="50" height="34"  xlink:href="/proyecto/mtb-aesthetics-2.jpg" />
<image x="0" y="34" width="50" height="39" xlink:href="/proyecto/mtb-aesthetics-3.jpg" />
<image x="50" y="34" width="50" height="40" xlink:href="/proyecto/mtb-aesthetics-4.jpg" />
<image x="0" y="72.5" width="22" height="25" xlink:href="/proyecto/mtb-aesthetics-5.jpg" />
<image x="22" y="72.5" width="37.1" height="25" xlink:href="/proyecto/mtb-aesthetics-6.jpg" />
<image x="59" y="72.5" width="18.5" height="25" xlink:href="/proyecto/mtb-aesthetics-7.jpg" />
<image x="77.5" y="72.5" width="22.5" height="30" xlink:href="/proyecto/mtb-aesthetics-8.jpg" />
</svg>
<figcaption class="f5 px2 break-before-avoid"><em>Fuente: Mountain Bike Aesthetic (consultado el 8 de agosto de 2017), <a href="http://mbaesthetic.tumblr.com/">http://mbaesthetic.tumblr.com/</a></em></figcaption>
</figure>

### Nicho y Nicho ideal

El nicho ideal es se define como la fracción del público objetivo que tiene capacidad de compra y ganas de comprar. En definitiva son los clientes de la empresa y serán quienes la sustenten a lo largo de su ciclo de vida.

Se caracterizan por ser mayoritariamente hombres mayores de 25 años habitantes de Córdoba que salen a andar en bicicleta en las afueras de la ciudad de forma regular, principalmente como forma de recreación y para pasear y conocer pueblos y ciudades cercanas a Córdoba (cicloturismo).

Se sospecha que existen en Buenos Aires clientes de caracterísiticas similares, que compran por MercadoLibre, sin embargo fue imposible realizar un diagnóstico sobre esta parte del público, y es un ítem que quedará pendiente para una próxima iteración en el proceso de investigación.

## Omnicanalidad y Marketing de Contenido

La multicanalidad es el concepto que remite a un negocio que trabaja con muchos canales de comunicación y venta alternativos, mientras que la **omnicanalidad** es un concepto que remite a una estrategia de gestión de los canales para integrar y homogeneizar el flujo del usuario a lo largo de varios canales. El objetivo de la omnicanalidad es que el usuario sea capaz de lograr las metas que se propuso (ya sean estas: asesorarse, comprar, informarse) por cualquier canal con el que se contacte (Deloite, 2016, “En busca de la omnicanalidad”, Recuperado de https://goo.gl/TBPCFT).

Si bien el negocio actualmente utiliza una multiplicidad de canales, no hay una estrategia para unificarlos, es decir, no hay omnicanalidad.

Por otro lado, el **marketing de contenido** es una estrategia de marketing por la cual la empresa emite contenido relevante (información relevante para sus consumidores) y lo distribuye generalmente por la web, para que luego personas que encuentren ese contenido lleguen a conocer la empresa. Por ejemplo: cuando la bicicletería sube información sobre el mantenimiento de bicicletas, usuarios que encuentren ese contenido pueden llegar a aprender más sobre la empresa. El objetivo de esta estrategia es la de captar mayor cantidad de usuarios y generar mayor tráfico en los canales de comunicación existentes (Wikipedia, 05/12/2017, “Marketing de Contenido”, Recuperado de https://goo.gl/Th2n9M).

Como la empresa cuenta con varios canales de comunicación (facebook, instagram) y quiere expandirse a otros (youtube, página web propia), una de las formas más efectivas de distribuir el contenido relevante sería mediante su adaptación a varios canales. Por lo tanto se plantea como estrategia la creación de contenido relevante y su adaptación a varios canales (estrategia que Noah Kagan llama **multiplicación de contenido**, 9/02/2017, “Biggest Marketing Strategies for 2017”, Recuperado de https://youtu.be/7KTm6uIi4T0). De esta forma se puede llegar a un público más amplio y lograr efectivamente los objetivos de la **omnicanalidad** y del **marketing de contenido**.

### Canales de comunicación y de venta

Los usuarios pueden optar por comunicarse por varias vías: ya sea por teléfono, por celular o *Whatsapp*, por *Facebook* o *Instagram*, o directamente en el local. Además de ser canales para *chatear*, tanto *Facebook* como *Instagram* permiten a los usuarios seguir novedades sobre productos y ofertas que la empresa publica de forma ocasional.

{{< figure src="/proyecto/2017-08-10-facebook-screenshoot.png" title="Facebook (captura del 10 de agosto de 2017)" >}}

En el momento de la venta hay dos canales disponibles: la venta en el espacio comercial y la venta mediante *MercadoLibre* (<abbr>ML</abbr>).

La publicación por <abbr>ML</abbr> tiene la desventaja de tener un costo, cobrado por la plataforma como parte de su servicio. Además, dentro de la descripción de una publicación no es posible incluir *links*, direcciones ni teléfonos: ningún dato personal que permita una comunicación por fuera de la plataforma[^mercadopoliticas].

Muchas veces estos canales de venta funcionan de forma coordinada: un usuario encuentra el producto que quiere por <abbr>ML</abbr> y busca la empresa en *Google*. Encuentra información de la empresa en *Google My Business* o en *Facebook*, para luego concretar la compra directamente desde la red social o mediante otro de los medios de comunicación más directos que se proveen de forma accesible: teléfono fijo, celular o *Whatsapp*, o visitando el local. Este tipo de flujos de usuario se mapea más adelante mediante un *journey map*.

<div class="break-inside-avoid flex">

<img src="/proyecto/google-my-business.jpg" />

<p style="max-width:463px"  class="texto-s px2"><em>“Carta” de Google My Business, tal como se generó al buscar “Bertolina Bikes” en Google el 8 de agosto de 2017.</em></p>
</div>

<div style="page-break-after:right"></div>

# Etapa creativa

<div class="page-break"></div>

> “Creatividad es inteligencia aplicada.”\
> — Daniel Wolkowicz (2017)

Para cerrar la etapa de investigación se redactaron objetivos para verbalizar los resultados hacia los que se quiere llegar con las soluciones que se van a explorar.

Los **objetivos generales** son:

1. Ampliar el público (incorporar nuevos mercados o ampliar los ya existentes).
2. Incentivar la fidelización de los usuarios.

Los **objetivos específicos** son:

1. Articular estrategias de comunicación homogéneas.
2. Unificar las expresiones de la identidad visual.
3. Mejorar la experiencia de la compra, haciendo un incapié en la experiencia post-compra.


## Definición del Problema

Dentro de la etapa de investigación se encontraron varios puntos que se pueden considerar como problemáticos.

1. **Falta de homogeneidad comunicativa en los medios de comunicación y venta.**\
No existen análisis ni estrategias que integren la variedad de medios de comunicación y venta que la empresa maneja. Esto se traduce en una serie de contradicciones (no intencionales). Ejemplos: el logotipo tal como se presenta en *Whatsapp* no coincide con el que se ve en el cartel principal del local; en MercadoLibre se produce contenido que no se puede enlazar a ningún otra plataforma (debido a sus políticas de publicación).
2. **Carencia control sobre la información en MercadoLibre y Redes sociales.**\
La información de los productos de MercadoLibre se encuentra muchas veces repetida y dispersa en gran cantidad de publicaciones que son difíciles de actualizar y mantener. La información es presentada rodeada de enlaces hacia productos de la competencia, y el diseño de la página está dictaminado por MercadoLibre. Similarmente, en las redes sociales las publicaciones se encuentran organizadas según el criterio de algoritmos que sirven en función de objetivos ajenos a los de la bicicletería. Además, las publicaciones en las redes sociales aparecen mezcladas en los "feeds" de los usuarios junto con todo tipo de contenido que —generalmente— no tiene ningúna relación con la empresa o su actividad.
3. **Mercado altamente competitivo. Necesidad de destacar.**\
La importancia que se le da al ciclismo como medio de transporte desde los medios de comunicación y las políticas públicas que empiezan a promoverlo podrían estar causando una expansión del mercado. Si esta hipótesis es correcta, existe una oportunidad para absorver una mayor porción del mercado. Sin embargo una de las características de este mercado es que es altamente competitivo, lo que causa una constante necesidad de destacar y presentar estrategias novedosas para atraer y fidelizar clientes.

Sintetizando, el problema de comunicación que se intentará solucionar desde el diseño es el siguiente:

<div class="f4 px2">

Falta de homogeneidad en la información que emite la empresa para promocionarse a sí misma, y falta de control sobre el contexto en que dicha información es distribuida; en un contexto donde es necesario presentar información cualitativa de forma novedosa para atraer la atención de los usuarios.

</div>


## Soluciones posibles

Como resultado del reelevamiento se concluye que es necesario un medio para que la empresa se promocione a si misma sin competencia interna. Además, es necesario favorecer la homogeneidad en las comunicaciones, incluso cuando fueran emitidas desde distintos medios.

La solución para este problema, algo que fue bastante evidente incluso antes de la etapa de investigación, es la creación de un sitio web.

Pero aunque el sitio web sea la solución elegida, aún debe haber una búsqueda para encontrar: (1) el lenguaje gráfico del sitio web; (2) de qué forma el sitio web se integrará (a) con otras piezas gráficas, (b) con otros canales de comunicación, y (c) en la experiencia de compra; y (3) qué contenido se creará para hacer que los usuarios vuelvan a visitar el sitio —porque, mientras que en las redes sociales el tráfico es orgánico, en un sitio web es necesario generarlo.

## Lenguaje gráfico

Los cambios que se realizaron en la identidad fueron paulatinos y asincrónicos. Nunca hubo un proceso de revisión importante para unificar las expresiones de la identidad gráfica. Para expandir el "aparato comunicacional" de la empresa era necesario realizar una instancia de conceptualización para terminar de definir y homogeneizar la identidad.

Para ello se planteó como objetivo expandir el lenguaje gráfico y flexibilizarlo (adaptarlo tanto para las piezas impresas como digitales). Luego, el lenguaje se documentó en un manual de identidad.

Las primeras iteraciones sobre este lenguaje provinieron de piezas gráficas manuales. Las gráficas que se muestran en la imagen siguiente se realizaron con la consigna de utilizar diversos recursos compositivos y técnicas de producción.

![](/recursos/imagenes/proyecto/by-me/01-collage-digital.gif)
![](/recursos/imagenes/proyecto/by-me/02-dibujo-lapizcolor.jpg)
![](/recursos/imagenes/proyecto/by-me/03-collage-manual.jpg)
![](/recursos/imagenes/proyecto/by-me/04-paper-craft-(1).jpg)
![](/recursos/imagenes/proyecto/by-me/04-paper-craft-(2).jpg)
![](/recursos/imagenes/proyecto/by-me/05-dibujo-birome.jpg)
![](/recursos/imagenes/proyecto/by-me/06-3d-origami-marcas-(1).jpg)
![](/recursos/imagenes/proyecto/by-me/06-3d-origami-marcas-(2).jpg)
![](/recursos/imagenes/proyecto/by-me/07-web-caleidoscopio-1.jpg)
![](/recursos/imagenes/proyecto/by-me/07-web-caleidoscopio-2.jpg)
![](/recursos/imagenes/proyecto/by-me/08-letra-corporea.jpg)
![](/recursos/imagenes/proyecto/by-me/09-paper-craft-local.jpg)


<!-- folleto -->

Para continuar este proceso, se recopiló una gran cantidad de fotografías en alta calidad sobre el Mountain Bike, recurriendo a bancos de imágenes CC0 (es decir, imágenes sin copyright, que su uso comercial y modificación está permitida sin atribución) Además, se recopilaron texturas relevantes. Las imágenes fueron seleccionadas principalmente del banco de imágenes CC0 Pixabay (www.pixabay.com) y subidas a una carpeta de Google Drive.

[imagenes de carpetas de drive]

En esta instancia de búsqueda de recursos también se recopilaron logotipos de las marcas de los productos que la empresa vende.

La búsqueda de un lenguaje visual continuó mediante la realización de una pieza gráfica de baja complejidad: un folleto de una sóla cara en formato A6 (10x15cm). Si bien el folleto nunca fue impreso de forma masiva, el proceso de búsqueda fue enriquecedor. Se partió de tres alternativas de baja calidad pero muy distintas. Luego sobre la alternativa elegida se realizaron iteraciones hasta llegar a un resultado satisfactorio.

[imagen iteraciones del folleto]


[imagen folleto final]

El folleto sirvió para definir una serie de recursos de identidad.
Se eligió trabajar con la madera con la textura, ya que es coherente con los muebles de madera del espacio comercial. Estos muebles hechos de palets, con tablas y planchas de aglomerado a la vista, muestran una madera sin barnizar, rústica. Esa misma idea se intentaría transmitir mediante la textura de la madera.

En el folleto esta idea de "rusticidad" se refuerza mediante la fotografía. Lo rústico es un concepto coherente con la estética del <abbr>MTB</abbr>. Los caminos de tierra que se usan para practicar *downhill* o *senderismo*, las rampas de madera improvisadas, las inclemencias del clima... todo esto hace que el Mountain Bike esté ligado con lo rústico.

<!-- MOODBOARD -->

Para seguir esta exploración se utilizó la técnica del *moodboard*. Un moodboard es un collage que puede contener todo tipo de recursos que existen en la identidad gráfica: colores, texturas, formas, fotografías, etc. Mediante esta técnica se puede explorar el sentimiento y el "estado anímico" que se genera mediante la identidad, aquel que se quiere transmitir. Este recurso fue útil para tener presente lo que se busca transmitir con el sitio web y cómo se puede transmitir.

[moodboard]


<!-- manual de identidad -->

<!--  -->

## Manual de Armado de Bicicleta

Esta pieza gráfica se añadió como una forma de solucionar varios problemas: por un lado, es un contenido cualitativo para el sitio web; por otro lado, ayuda a que los usuarios del servicio tengan una experiencia más grata, favoreciendo su fidelización; y por otro lado, se puede aprovechar como estrategia de marketing de contenido para que nuevos clientes conozcan la empresa.

Surgió esta oportunidad para crear contenido cualitativo para el sitio web cuando el comitente planteó la inquietud de que sus bicicletas no venían con un manual de armado. Al ser enviadas por encomienda, es imposible venderlas armadas, ya que el tamaño de la caja debe ser lo más reducido posible. El proceso de armado puede resultar complejo para algunos usuarios y por eso cometían errores comunes: por ejemplo, al no ajustar bien los pedales, era común que se rompan luego de un uso muy corto.

La creación de un manual como contenido para el sitio web serviría para generar tráfico y ampliar la cantidad de gente que puede llegar a conocer la empresa (ya que Bertolina Bikes no es la única empresa que vende bicicletas desarmadas de esta forma, y sin embargo será la única que puede proveer a sus clientes con un manual).

Pero este manual también debería llegar de forma impresa a los usuarios del servicio de encomiendas, para que sea más accesible en el momento de armar la bicicleta.


## Sitio web

El sitio web es un espacio análogo a un local, pero digital. Presenta la información relacionada a la empresa de forma clara y accesible. Además transmite la identidad visual y ayuda en el posicionamiento de la empresa en otros canales (Facebook, Instagram, etc.).

Para el diseño de un sitio web, que es un sistema de alta complejidad, no basta con tratar cada página como una pieza distinta. Debe existir una coherencia entre las partes y se debe estudiar cuidadosamente la forma en la que las mismas se relacionan entre sí.

Una abstracción para lograr la coherencia es entender un sitio no como un conjunto de páginas, sino como un conjunto de *vistas*.

Las **vistas o layouts** de un sitio están formadas por un conjunto de componentes que están en relación entre sí. Sobre estas vistas es que se vuelca el contenido para generar las páginas. Por ejemplo: la vista del “producto” determina cómo se ve la información de todos los producto. Para mantener la coherencia entre contenidos del mismo tipo, numerosas páginas se generan a partir de una sóla vista. 

Las vistas contienen **componentes**, que son secciones que se repiten muchas veces a lo largo de varias vistas. Por ejemplo: el encabezado y el pie de página, una galería de imágenes, un botón, un input, etc.

Los componentes están formados por varios **"parámetros de identidad"**:estilos individuales que se mantienen constantes (un color, una tipografía, una retícula, un borde, un ícono, etc.). Pero los componentes incluso pueden contener otros componentes.

Para explicarlo mejor con una analogía, se podría decir que cada **parámetro de identidad** es un átomo, y que juntos forman componentes, que son como moléculas. Los componentes simples (un botón) podrían ser moléculas de baja complejidad, como el H<sub>2</sub>O. Los componentes complejos, como el encabezado de un sitio (que contiene varios botones y puede reaccionar al scroll del usuario), es como una molécula compleja, como puede ser la Glucosa o el ADN.

### Style tile y Wireframes

El style tile o "baldosa de estilos" (según su traducción literal) es una forma de sintetizar los estilos y componentes que hacen a un sitio web en una sola imagen. Se utilizó este recurso para mostrar el resultado de la búsqueda tipográfica, de colores y texturas, y su aplicación a la web. Mediante este recurso se puede observar cómo se verán en conjunto los componentes del sitio (botones, texto, títulos), y a partir de eso se puede evaluar si son coherentes entre sí y transmiten lo que se quiere transmitir. Este es el style tile luego de un par de iteraciones.

[ imagen style tile ]

Como último recurso previo al momento de traducir el sitio a código, se crearon una serie de *wireframes* de la página de inicio. Los wireframes muestran cómo va a estar ocupado el espacio en el sitio, una idea básica de cómo va a ser el encabezado y cómo va a ser la navegación.

[ imagen wireframes ]

Las distintas vistas que formarían el sitio se bocetaron como forma de obtener un pantallazo general de la extensión del mismo.

[ imagen bocetos ]

# Etapa de prototipado y testeo

> “El sistema es más que la suma de sus partes”
>
> — Karl Gerstner (1964)

El prototipado de las piezas tiene una instancia de testeo intermedia antes de que el prototipo se defina como la versión final. Para simplificar este libro se incluyen en la misma sección las etapas de prototipado y testeo, porque ambas etapas están en relación estrecha entre sí.

La etapa de prototipado es la etapa en la cual se exploran distintas alternativas de solución para luego ser testeadas.

El testeo es el momento en que se analiza la propuesta de solución como forma de resolver el problema, y en relación a los objetivos planteados.

Recordemos que según el método del Diseño Centrado en el Usuario no existe una solución correcta y otras incorrectas, simplemente una solución que es la mejor entra todas las alternativas.
El método, por lo tanto, está vinculado con la **iteración**: hacer una y otra vez lo mismo, pero introduciendo cambios graduales. El resultado del proceso iterativo suele llevar a un diseño que, efectivamente, es "mejor entre todas sus alternativas", siempre y cuando los cambios que se hayan introducido en el proceso tengan un justificante válido.

En este capítulo se definen en detalle las propuestas de solución para todas las piezas gráficas que se plantearon, pero para ello en muchos casos fue necesario volver a realizar investigaciones y exploraciones creativas.

## Sistema gráfico

Una identidad visual es un sistema gráfico complejo que puede contener uno o varios sub-sistemas (como lo puede ser un sitio web o un libro). Habiendo definido las piezas gráficas que se iban a prototipar en el proyecto, pareció pertinente realizar un análisis de estas piezas como partes de este sistema.

Como el proyecto está relacionado con la fidelización de los usuarios, se dividieron las partes del sistema gráfico según el momento en el cual la función de la pieza es relevante en el "recorrido" del usuario:

1. Un primer momento de **pre-venta**, el momento en el que el comprador se informa y realiza la decisión.
2. El momento de la **venta**, que puede transcurrir en el espacio comercial o en un sitio web, o incluso puede concretarse por teléfono.
3. El momento de la **post-venta**, momento donde el consumidor puede volverse cliente (esto se da cuando disfruta de su experiencia, gana una imagen positiva de la empresa y decide volver a comprar en el futuro en el mismo lugar).

Desde este punto de vista, se clasificaron las piezas gráficas de acuerdo al momento en el cual su función es más relevante para mejorar la experiencia del usuario y lograr que se vuelva en un cliente regular.

[ analisis del sistema gráfico ]

## Parámetros de identidad

Para unificar la identidad se designó una tipografía para títulos y texto, un valor para interlineado y para la separación entre párrafos, una paleta de colores, un sistema de retícula, entre otras variables.

Algunas de estas variables ya habían sido determinadas anteriormente (en el folleto o en piezas gráficas ya existentes). Sin embargo muchas de las exigencias de la web (como formato), hicieron que este lenguaje deba ser adaptado. Además, en el proceso de traducir este lenguaje al formato web y documentarlos, se añadieron nuevos parámetros y se revisaron los existentes. 

Para el sitio web, el cambio más grande se introdujo en la tipografía. También se realizaron otros ajustes: se intentó buscar un estilo visual con menos texturas y se utilizaron paletas con fondos negros, aprovechando la mejor definición y menor gasto al usar este color en el sistema <abbr>RGB</abbr>.

### Tipografía

Incluir numerosas tipografías en una página web puede resultar detrimental para la experiencia del usuario, debido a que debe descargarlas, muchas veces en conexiones muy lentas. Con la irrupción de los teléfonos inteligentes se empezó a prestar aún más atención al tiempo de carga de los sitios web, ya que las conecciones móviles generalmente son inestables y lentas. Mientas más tipografías y texturas se usen, mayor será el tiempo de carga, y peor la experiencia del usuario.

Teniendo en cuenta estos límites, se reemplazó la tipografía "DIN Pro" por la "Roboto" para el sitio web.

Uno de los obstáculos a la hora de usar la familia "DIN Pro" en la web es obtener su licencia (mientras que en el diseño editorial nadie se podría enterar que se utiliza una tipografía sin tener su licencia, en un sitio web esto sería imposible de "disimular"). Considerando que el precio de la licencia es de 95 dólares para cada variante (Regular, Bold, Itálica, etc.) y que, si se llegaran a pasar las 10.000 visitas mensuales en el sitio, abría que extender la licencia, la "DIN Pro" no se presentó como una opción sustentable.

Pero el precio de la licencia no fue la única razón para realizar el cambio. La familia de reemplazo, "Roboto", no sólo es una familia muy similar a la "Din Pro", además tiene la ventaja de venir instalada por defecto en los celulares que tengan el sistema operativo Android. En Argentina este sistema operativo es utilizado por la mayoría de los usuarios: en 2015 estaba instalado en el 75% de los celulares del país (Infobae, "*Argentina, el país con menos usuarios de iPhone de la región*", 2015[^celularesArgenta]). Al elegir esta tipografía, en la mayoría de los teléfonos móviles el usuario no tiene que descargarla, reduciendo de esta forma el tiempo de carga total del sitio y permitiéndole ahorrar una pequeña cantidad de "datos móviles". Los usuarios de computadoras, de iPhones u otros dispositivos, aún tienen que descargarla, por eso la tipografía web se insertó en el sitio de forma cuidadosa para que no afecte la usabilidad: si no se descarga rápidamente, se verá el sitio con una tipografía de reemplazo (Helvética, Arial u Open Sans, según disponibilidad del dispositivo) durante el tiempo en el cual la tipografía se esté descargando.

[^celularesArgenta]: https://www.infobae.com/2015/04/27/1725074-argentina-el-pais-menos-usuarios-iphone-la-region/

[ muestra tipográfica Roboto. Tabla comparativa con DIN Pro ]

### Manual de Identidad

La búsqueda de un tono propio desde el aspecto tipográfico, desde la retícula, colores, y texturas se realizó en paralelo con las primeras versiones de la hoja de estilos (<abbr>CSS
</abbr>) del sitio web. Estos parámetros fueron documentados en una página web (https://bertolinabikes.com.ar/meta/manual-de-identidad), donde se pueden también encontrar links hacia colecciones de imágenes y texturas relevantes. Luego se creó un <abbr>PDF</abbr> con el mismo contenido pero resumido (no tiene detalles técnicos relativos al funcionamiento y diseño del sitio web, como la nomenclatura de las variables de color) y con ejemplos del uso de la tipografía.

[ imagenes PDF manual de identidad y manual de identidad web ]

## Desarrollo del sitio web

El sitio web como el que se plantea para este trabajo tiene la capacidad para expandirse e incluir más páginas en el futuro, ya que las mismas son generadas de forma "programática".

La "generación programática" —generación mediante un programa—, es el proceso en el cual:

1. Un programa informático o software (que fue hecho por un programador o un colectivo de personas) que actúa como “sistema de gestión de contenido”...
2. ...recibe el contenido (texto, imágenes, videos, etc., almacenados en una base de datos o como archivos de un repositorio) que proporciona un operador de dicho programa (que suele ser llamado editor de contenido o content manager)...
3. ...y lo convierte en un sitio web de acuerdo a un layout, plantilla o tema, previamente diseñado (los diseñadores que se dedican a esta tarea suelen llamarse diseñadores de interacciones).

### Arquitectura de la información

La **arquitecturización de la información** es el acto de decidir en qué orden las partes del todo pueden ser arregladas con el objetivo de comunicar el sentido que se le quiere dar a los usuarios [^abbyTheIa]. Por lo tanto la arquitectura de la información es una forma de organizar las partes de algo para hacer que sea más entendible como un todo.

[^abbyTheIa]: Abby Covert, How to Make Sense of Any Mess, 2014. Libro digital. La definición de "Arquitecturización de la Información" se obtuvo del diccionario de términos incluido como parte del libro. El link hacia la definición es: http://www.howtomakesenseofanymess.com/term/noun/information-architecture/

Se decidió una arquitectura de la información del sitio con distintas **secciones** a las que se accede desde el encabezado del sitio. Las secciones son: "Inicio", "Productos", "Guías" y "Contacto".

Algunas secciones son más bien dinámicas, es decir, están formadas por diversas **publicaciones** que se van añadiendo y modificando periódicamente. Un producto, entonces, es una publicación. Una guía (como la guía de armado de la bicicleta), es otra publicación. Además, en la página de inicio se van subiendo "novedades", que también son publicaciones.

Por lo tanto hay distintos **tipo de contenido**, con propiedades distintas (por ejemplo, un producto tiene propiedades como "imagen de portada", marcas, categorías, etc.).

Algunas secciones y contenidos son estáticos y no representan un tipo de contenido (con características homogéneas), sino que son sólamente textos aislados. El contenido estático es: la sección "Contacto" y el componente "Acerca de" (que se ubica en la página de inicio).

[ esquema "AI" ]

### El contenido

Para crear la sección de productos se utilizó contenido ya existente: la empresa previamente documentó descripciones e imágenes de productos para MercadoLibre. En el proceso se organizó la información de forma tal en el que se repita lo menos posible. Por ejemplo, muchas publicaciones de MercadoLibre se trataban de la misma bicicleta pero en distinto rodado, por lo tanto la descripción de las mismas era similar. Estos productos se unificaron como uno sólo.

Al vincular un producto a un link de mercadolibre, se puede consultar su precio actualizado y otros datos del mismo. Para esto se utiliza Para esto se utiliza la <abbr>API</abbr> pública de MercadoLibre (por simplicidad vamos a definir una <abbr>API</abbr> como una herramienta que crean los programadores de una aplicación para que otros programadores y/o desarrolladores de software puedan interactuar con la aplicación).

El contenido además tiene como propiedades dos taxonomías: "marcas" y "categorías". Las taxonomías se relacionan con el producto de forma **heterárquica**, es decir, en forma de etiquetas o "hashtags". Este tipo de relación permite que un sólo producto pueda pertenecer a varias marcas o a varias categorías. Es más flexible que la jerarquía, su contraparte, y se adapta mejor a casos extremos (por ejemplo: una publicación de un kit de productos podría estar relacionado a varias marcas).

Otra propiedad de los productos son las imágenes. Cada producto tiene una imagen principal, la que se muestra en el listado, y varias imágenes adicionales que se muestran en una galería.

Para el contenido del tipo "guías", se creó una publicación que contiene el manual de armado de la bicicleta, y se espera llenar con más contenido en el futuro: consejos de seguridad, guías de mantenimiento, etc.

Este contenido tiene una función estratégica. Tal como se describió en la etapa de investigación, esto se conoce como estrategia de marketing de contenido (al crear contenido relevante para los ciclistas y usuarios de bicicletas, muchos de ellos podrían encontrar el contenido mediante una búsqueda o mediante la recomendación de amigos en redes sociales; y hay posibilidades de que también se interesen sobre la empresa, y por lo tanto hay una posibilidad de que se conviertan en clientes y compren algún producto. El marketing de contenido debe entenderse como un proceso contínuo, ya que implica buscar necesidades e inquietudes reales de los usuarios.

El contenido del tipo “novedades”, mientras tanto, se creó con el propósito de que se publiquen: ofertas, información sobre nuevos productos, noticias sobre la empresa o noticias de interés para usuarios.

### Sistema de gestión de contenido

El **sistema de gestión de contenido** elegido es Hugo (https://gohugo.io/), un programa que compila a partir de un repositorio (conjunto de archivos) un sitio web. La principal ventaja de este programa es la simplicidad con la que se pueden generar gran cantidad de tipos de contenido y la velocidad con la cual se puede generar y actualizar el sitio.

Hugo es un **generador de sitios estáticos**, lo que significa que genera un conjunto de archivos "estáticos" que se pueden ver directamente en el navegador, sin necesidad de compilar un código fuente o consultar una base de datos.

¿Por qué un sitio estático? Los sitios estáticos funcionan más rápido que los llamados sitio web dinámicos, porque no necesitan ejecutar un código desde el lado del servidor (como <abbr>PHP</abbr>), ni tampoco necesitan consultar una base de datos. El cliente (el navegador web) simplemente descarga los archivos necesarios, tal cual se encuentran dispuestos. Si bien se logra mayor velocidad, se pierde la posibilidad de crear una versión personalizada del sitio a cada usuario. Tampoco se podrían crear interacciones del tipo: cuentas para cada usuario, sistemas de comentarios, etc. Estas funciones sólo se podrían lograr recurriendo a widgets de terceros (ejemplo: comentarios de Facebook o Disqus, inicio de sesión con Facebook, Twitter o Google, Formularios de Google, etc.).

El sitio web generado es un sitio web estático, pero esto no significa que el contenido no cambiará nunca. A la hora de actualizar el contenido Hugo genera, a partir de una publicación, todas las páginas derivadas (por ejemplo: crea la página de la publicación en sí, actualiza los listados donde esa publicación aparece, crea o actualiza páginas de taxonomías, etc.). El software genera el sitio entero tardando apenas unos cuantos milisegundos por cada publicación. El contenido y los layouts se ubican en carpetas separadas, lo que significa que es imposible que un cambio en el contenido modifique el aspecto visual del sitio, mucho menos que “rompa” algún elemento del diseño.

[ imagen HUGO ]

El servidor que se eligió para alojar el sitio es Netlify (http://netlify.com/) porque es un servidor exclusivamente dedicado a sitios web estáticos que cuenta con numerosas ventajas, entre ellas: es totalmente gratuito, funciona con los servidores de Amazon (una de las redes de distribución de contenido más robustas y rápidas del mundo) y provee la instalación de certificados <abbr>SSL</abbr> gratuitos (certificados que hacen que el sitio pueda ser visitado con el protocolo "HTTPS", es decir, con una conexión encriptada).

[ imagen Netlify ]

Esta forma de desarrollar sitios web tiene una curva de aprendizaje alta. Se requiere la instalación previa de varios programas, algunos de ellos requieren cierto conocimiento "técnico" para ser usados[^programas requeridos] y conocimientos sobre el uso de la consola del sistema operativo y el uso de sistemas de gestión de versiones (para sincronizar los cambios con el servidor). Pero una vez superada esta instancia, el diseño y la actualización del contenido es relativamente simple.

[^programas requeridos]: Para diseñar y actualizar en el servidor el sitio web se usaron los programas mencionados a continuación. De forma indispensable se necesita: (1) el sistema de control de versiones Git, para descargar el código fuente y actualizar los cambios en el servidor; (2) el lenguaje de programación Go para que Hugo funcione y (3) el programa Hugo. Adicionalmente, para realizar tareas de diseño y manejar los recursos, es necesario: (1) un editor de código, como Sublime Text o Visual Studio Code; (2) software de edición de gráficos, como Photoshop; y opcional pero recomendado, (3) automatizadores de tareas, programas que concatenen y minimicen los archivos <abbr>CSS</abbr> y <abbr>JS</abbr> para que sean más livianos, redimensionen imágenes automáticamente, etc. (yo utilizé Gulp, que es una librería del entorno Node, pero también se puede usar la librería Grunt, o, para el usuario menos técnico, se puede recurrir a utilidades online).

No se puede comparar la simpleza de WordPress o Wix con este método, pero teniendo en cuenta que el sitio generado es gratis, seguro (no se puede hackear), puede utilizar una arquitectura de la información personalizada y es servido de forma casi instantánea por una red de distribución de última tecnología, es un método que produce mayores ventajas tanto para el comitente como para el usuario.

Si en el futuro se quisiera migrar el contenido a otro sistema que permita una edición más amigable, esto se podría realizar fácilmente, porque el contenido (las publicaciones) se encuentra organizado en carpetas totalmente aislado del código para generar los layouts del sitio. Estos layouts también podrían ser migrados a otro sistema de gestión de contenido, siempre que el sistema permita la creación de temas mediante código fuente, como Joomla o Wordpress — es decir, se descartan los editores <abbr>WYSIWYG</abbr>[^WYSIWYG] como Wix. En este caso el diseñador web debería tener conocimiento sobre la creación de temas en el sistema en cuestión.

[^WYSIWYG]: <abbr>WYSIWYG</abbr> es un acrónimo de "What You See Is What You Get" (Lo que vez es lo que obtienes). Un editor de este tipo es un editor donde el resultado final que se mostrará al usuario es igual al que se le presenta en la pantalla al usuario. Entre los editores de sitios web <abbr>WYSIWYG</abbr> tenemos: Wix, Squarespace, Blockity, etc. La principal desventaja de estos editores suele ser que tienen herramientas precarias para formar arquitecturas de la información que se salgan de lo estándar. Wikipedia, WYSIWYG, editado por última vez el 4 de noviembre de 2017, https://es.wikipedia.org/wiki/WYSIWYG

Esta aclaración se realiza porque luego de un par de cambios en el sitio quedó en evidencia que este método no es el ideal para mantener grandes bases de datos con muchas imágenes. Si bien se tuvo presente que existen soluciones que sincronizan los datos desde Éxcel y permiten la edición simultánea en MercadoLibre y en un sitio web, estas opciones suelen conllevar un mayor período de desarrollo. El objetivo inmediato era desarrollar una web con un estilo visual propio, que funcione en todo tipo de dispositivos (optimizada para móviles) y que funcione con conecciones lentas. Cuando se empieze a generar contenido en mayor cantidad podría ser necesario migrar tanto el tema como el contenido a otro sistema más complejo.

### El diseño

Al diseñar un tema para el sistema Hugo, los wireframes y bocetos realizados en la etapa creativa se utilizaron como guía. La tarea de desarrollo consiste en codificar estos diseños en un formato que pueda ser entendido por un navegador web (generar layouts usando archivos de código en formato: <abbr>HTML</abbr>, <abbr>CSS</abbr> y <abbr>JS</abbr>).

Armar un sitio web usando un tema ya creado con anterioridad podría permitir un desarrollo más rápido, pero se pierde control sobre aspectos importantes, como la forma en que está estructurada la hoja de estilos y los estilos por defecto que se aplican a las tipografías, las variables de color, etc. Los resultados de construir un sitio web "desde cero" son más originales y permiten un control más granular sobre todos los aspectos del sitio en cuestión.

Los objetivos, como ya se mencionó, eran crear un sitio con un estilo visual propio de la empresa, optimizado para la mayor cantidad posible de dispositivos (móviles, tablets y computadoras de escritorio) y optimizado para conecciones lentas.
Para ello se debieron diseñar distintas vistas o layouts:

1. Página de inicio
2. Página de contacto
3. Sección de novedades:
  3.1. Listado de novedades
  3.2. Novedad (singular)
4. Sección de Guías:
  4.1. Listado de guías
  4.2. Guía (singular)
5. Sección de productos:
  5.1. Listado de productos
  5.2. Producto (singular)
  5.3. Marcas (taxonomía):
    5.3.1. Listado de marcas
    5.3.2. Marca (singular), con listado de productos de dicha marca
  5.3. Categorías (taxonomía):
    5.3.1. Listado de categorías
    5.3.2. Categoría (singular), con listado de productos de dicha categoría

Cada una de estas vistas debe tener un diseño más o menos único, pero a la vez coherente con el resto del sitio. En el listado de productos, cada elemento de la lista representa una página de un producto; por lo tanto en cualquier parte del sitio en el que aparezca un link hacia un producto, debe verse de la misma forma. Esta forma de reciclar componentes sirve para mantener la coherencia, lo que les permite a los usuarios familiarizarse con el sitio y navegar por el mismo más rápido.

Por lo tanto el diseño del sitio empieza con la creación de estos componentes.

[ componentes: del boceto a la ejecución ]

El proceso de trabajo sobre estas vistas fue iterativo, se fueron resolviendo problemas y tomando nuevas decisisiones a medida que se iba ampliando el contenido y que el sitio era probado en dispositivos distintos.

[ vista: página de inicio, iteraciones ]

Con el enfoque mobile-first, en la primera iteración de cada vista se realizó un diseño apto para dispositivos móviles. Estos diseños muchas veces en las pantallas de PC parecen "vacíos" de contenido. Cuando era posible, se volvió a iterar sobre el diseño para aprovechar el espacio extra para pantallas de PC. En la vista "listado de productos", este espacio se llenó con algunas de las marcas y categorías más importantes, mientras que en componente "producto" este espacio extra se usó para poner cierta información sobre el producto y mostrar productos relacionados. En la versión móvil del sitio esta información simplemente se deja para el final, por lo tanto las dos columnas que se ven en la PC de escritorio son apiladas una abajo de la otra para las pantallas más pequeñas.

[ vista: listado de productos, iteraciones ]

#### Detalles de la vista de producto simple

- Un botón de compartir al lado del título revela una barra con accesos directos a los métodos para compartir más usados.
- Las imágenes de la galería pueden ser abiertas en modo "pantalla completa" y compartidas. Se utilizó la librería de Javascript **photoswipe.js** (http://photoswipe.com/), que es una galería responsiva, que permite gestos táctiles y que le permite al usuario compartir las imágenes.
- Los productos tienen como propiedad un listado de links hacia publicaciones de MercadoLibre.  Mediante la API pública de MercadoLibre se consultan imágenes, estado del stock, tipo de envío y precios. De esta forma siempre se mantienen actualizados.
- La información sobre el envío y los métodos de pago se repiten en cada producto, si bien en algunos productos cuentan con "Envío Gratis", mientras que en otros productos no se aclara.
- Lo que en pantallas grandes se acomoda a la derecha del producto, en móviles es empujado hacia el final de la página.

[ vista de producto simple: screenshots ]

#### Detalles de la página de inicio

- Las novedades se muestran como un “carrusel” de imágenes. Es lo primero que se muestra porque es lo que van a buscar los usuarios regulares del sitio. Al clickear en una imágen el usuario es dirigido hacia la publicación (vista novedad simple). Se utilizó la librería **siema.js** (https://github.com/pawelgrzybek/siema) porque es liviana y tiene incorporados gestos táctiles.
- La página de inicio tiene una sección con productos, casi idéntica en diseño a la vista  “listado de productos”, aunque, a diferencia de ésta, se muestan solamente una selección de los productos más vendidos en MercadoLibre, con un link abajo que invita al usuario a seguir explorando (“Todos los productos”).


### Sobre la edición del diseño y contenido

El funcionamiento y la edición del sitio, como ya se mencionó, requieren ciertos programas y ciertos conocimientos técnicos. Para que se pueda editar por diseñadores, desarrolladores de software y/o programadores, se documentó la información necesaria para su funcionamiento en una guía que se ubica en la <abbr>URL</abbr> https://www.bertolinabikes.com.ar/meta/modificar-el-contenido/

### Testeo del sitio

Hubo varias instancias de pruebas, que determinaron muchos cambios del diseño. Para mantener la brevedad sólamente se mencionan: se reorganizaron las categorías del sitio, se agrandaron algunos botones de navegación que eran difíciles de encontrar, se hizo que al clickear en la foto de portada de un producto se abra esa foto en grande (mucha gente intentaba clickearla para agrandarla, sin ningún resultado), se movieron las categorías del sitio a la izquierda del listado de productos, etc.

La técnica más usada para realizar estas pruebas fue la observación directa. Se pone a un usuario en un contexto y se le pide que cumpla una meta ("Estás buscando un producto X para tu bicicleta, y llegás a esta página..."), para luego observar cómo navega en el sitio (¿Cuánto tardó? ¿Tuvo que hacer pasos innecesarios? ¿La disposición de elementos le facilitó la tarea?).

Uno de los descubrimientos más significativos fue que a la hora de vender **las imágenes importan**. Esta es la misma conclusión a la que llegaron los desarrolladores de Cars.com (una página que sirve a millones de usuarios diariamente) después de hacerse la pregunta: ¿Comprarías un auto sin tener una oportunidad de verlo[^refCarsDotCom]? Esta necesidad de obtener fotografías de calidad es en parte suplida el diseñador gráfico con herramientas de edición, para la “puesta a punto” de dichas fotos.

[^refCarsDotCom]: Esta pregunta se hizo Nate Johnson, ingeniero de software, en el artículo "Serving the Right Content at the Right Time… Quickly" (21/07/2016). El artículo original contiene información acerca de los desafíos con los que se encontró el equipo de Cars.com para servir imágenes en su sitio, se fue recuperado de https://tech.cars.com/serving-the-right-content-at-the-right-time-quickly-4e507e3ab199 

Inicialmente se recolectaron las imagenes de MercadoLibre, pero pronto se encontraron demasiadas inconsistencias en la forma en que la marca de agua estaba añadida a estas imágenes. En el listado de productos era muy notorio cómo entre imágenes similares la marca de agua estaba puesta en formas diversas, como: tapando todo el producto, repetida varias veces, en color negro, en color blanco, etc. Por lo tanto luego de que se lograra recuperar imágenes de los productos sin marca de agua se creó un *script* (programa) que posiciona la marca de agua de acuerdo a un tamaño que, en relación al tamaño de la imagen, se mantiene constante. Este script además incorpora un "filtro" que añade contraste y brillo a las imágenes, ya que las originales aparecían muy oscuras. Este fue un facilitador para editar las más de 150 fotografías que se subieron al sitio luego de migrar aproximadamente  el 25% de las publicaciones de MercadoLibre (datos calculados del 1ro de Diciembre).


## Piezas de pre-venta: Redes sociales

La gráfica para redes sociales consisitó en una serie de piezas gráficas que promocionaban el lanzamiento del sitio web y una serie de productos en oferta debido al segundo aniversario de la empresa.

La empresa renovó por segunda vez su contrato para alquilar el espacio comercial, y querían lanzar promociones con esa consigna. Estas promociones, además, sirven para fomentar las ventas en una época del año (diciembre) donde se suele vender menos.

A la hora de diseñar la página de inicio se tuvo en cuenta la necesidad regular de actualizarla con novedades, por lo tanto el carrusel de imágenes que se muestra al principio contiene imágenes que ya están en un formato apto para ser fotos de portada en Facebook. Por lo tanto algunas gráficas de portada podrían subirse tanto a la página como al Facebook sin inconvenientes en el formato.

Se creó un álbum de ofertas desde una misma plantilla, que se entregó como un archivo <abbr>PSD</abbr> al comitente. Esta plantilla tiene como texto la palabra “Ofertas”, ubicado arriba para que sea lo primero que se vea y que llame la atención.

## Piezas gráficas de Venta: Carteles para el local

Para las piezas gráficas de venta se decidió usar un modo de impresión no convencional. Como la madera ya era un elemento distintivo del local (todos los muebles están hechos a medida con “palets” de aglomerado y vidrio), se utilizó esta textura pero simulándola mediante una impresión en transfer sobre una plancha de MDF.

Las primera prueba de color sirvió para ver cómo influían las tintas y el color del soporte sobre la impresión. Se confirmó que el negro es una tinta sólida que tapa el color del sustrato, mientras que el blanco se atransparenta. Sabiendo esto se reemplazó el negro por marrón oscuro. Otra de las cosas que se descubrió es que el rojo de la impresión no es muy fuerte. Como el cartel de “Abierto” debe llamar la atención desde lejos (ejemplo: lo tiene que poder ver alguien que pasa manejando), esta técnica no resultó la ideal para esa pieza.

Se utilizó un formato B5 (similar al de este libro) porque permite imprimir en una hoja A4 con sangrado. El original de impresión figura invertido y tienen líneas de corte. Se aprovecharon los sobrantes de la impresión para imprimir URLs del sitio web (se pueden transferir en cuadernos, remeras, etc.).

### Ficha técnica cartel de horarios

[ ficha técnica cartel de horarios ]

Dimensiones: B5 (250x176mm)

Sustrato: MDF

Espesor: 3mm

Impresión: (indirecta) papel transfer A4 impreso en láser 4/0, transferido al MDF con una plancha casera
Proveedor: Soluciones Gráficas. Obispo Trejo 295, Córdoba, Argentina. Tel: 54 351 4240611. www.solucionesgraficas.com.ar

Cantidad: 1

Acabado: el MDF es perforado con dos “chinches”, a las que se les ata un hilo de tanza de 30cm (aprox.) que contiene una ventosa con agujero transversal (para pegar el cartel en puerta de vidrio)

Precio de la impresión: $ 24.00

Precio de la madera: $ 15.00

Precio de los materiales necesarios para el acabado: $ 3.00

Precio del trabajo manual: lleva aproximadamente 10 minutos, razón por la cual se cobra $15.00 (1/6 del precio de la hora de gestión)

Precio total: $ 57.00

## Piezas gráficas de post-venta

El sistema de piezas de post-venta consiste en una serie de piezas gráficas que acompañan los productos enviados por encomienda:

- Manual de armado de bicicleta
- Sobre contenedor (estas piezas están presentes cuando se envían bicicletas).
- Sticker que va adherido al producto (presentes siempre que se envíe un marco de bicicleta, un casco, entre otros).
- Información de remitente y destinatario en el lado exterior de la caja.

### Manual de armado de la bicicleta

Para diseñar el manual, destinado a ir en las bicicletas que se envían por encomienda, fue necesario investigar sobre las dificultades que tenían los usuarios durante el armado de su bicicleta.

La primera versión del manual se realizó como un documento de Word, utilizando texto y fotografías armadas por un empleado de la empresa. Consistía en 5 pasos. Se editó el texto para llevarlo a una voz neutra y se buscaron los nombres "técnicos" de los componentes y herramientas mencionados.

Para testearlo fui personalmente a la empresa e intenté armar una bicicleta. Me dí cuenta que muchos pasos necesitaban más detalles al contrastar mi propia idea de lo que había que hacer con lo que en realidad había que hacer (un empleado de la empresa me ayudó con las partes que no sabía). De esta forma se agregaron nuevas partes al manual, para lo que hubo que sacar otras fotografías y ampliar algunos pasos.

La mayoría de las ilustraciones se realizaron mediante el calcado de las fotografías con software de edición de gráficos vectoriales. Para realizar la ilustración de la posición correcta para andar en bicicleta se calcó otra ilustración obtenidas de internet, y para la ilustración de la bicicleta vista desde frente se realizó una técnica mixta: algunas partes se calcaron de una fotografía y otras de ilustraciones de internet, y luego se unieron las partes. Se usaron gráficos vectoriales CC0 de bicicletas vectorizadas.

[ comparación foto/ilustración ]

Este estilo de ilustración lineal, inspirado en el dibujo técnico, favorece la comprensión de lo que se quiere mostrar. Es muy usado en manuales de uso, y por esa razón podría hacer que los usuarios entiendan más fácilmente la función de la pieza. Además de esto, posibilita que la pieza se imprima en blanco y negro, lo cual ahorra tinta.

Luego de recopilar los pasos de armado y realizar las ilustraciones necesarias, el contenido del manual entró perfectamente en dos páginas A5. Se utilizó una tipografía tamaño 10 puntos y las páginas A5 se dividieron en dos columnas: una para texto y otra para imágenes. En el texto, en negrita se mencionan partes de la bicicleta y se referencian imágenes por número. Las imágenes contienen algunas flechas que explican ciertos movimientos, así como referencias hacia las partes de la bicicleta mencionadas en el texto. La redacción es en tono neutro, formal y sin evitar la redundancia, para favorecer la máxima comprensión.

Se designó que una parte del manual sería una pieza publicitaria. De caracter tanto informativo como persuasivo, esta pieza debería ayudarle al usuario a reforzar su desición de compra y evitar que se arrepentienta. Como una bicicleta no es siempre una compra barata para los usuarios (sobretodo si se la compara con el precio de una "bicicleta de súper"), es importante —para que logren una conección emocional con el producto—, recordarles que la compra debe ser vista como una inversión: da sus frutos al largo plazo, ya que requerirá menos mantenimiento, menos arreglos y durará más. Además de esto, se le provee información sobre la empresa al usuario que puede considerar realizar otra compra en el futuro, o recomendarla a colegas.

Se decidió utilizar el formato A4 con un doblez al medio. En este formato de folleto se puede incluir tanto el contenido del manual, del lado interno, como información publicitaria, del lado externo.

[ bocetos del manual: posición del doblez, etc. ]

Esta pieza gráfica, por lo tanto, se imprime en full color del lado "publicitario", y a una sóla tinta del lado de adentro, que es donde va el manual. Se eligió un papel obra de 120 gramos porque es un material reciclable y resistente que absorve bien las tintas de la impresión. Si bien este material es "casi una cartulina" en grosor (es bastante grueso), se pensó el manual en el contexto donde se iba a utilizar: un taller o garage, con herramientas, por usuarios que podrían tener las manos con grasa, y por estas razones se decidió optar por un papel que resista más al "maltrato".

[ ficha técnica manual de armado ]

Para la versión digital del manual se tradujo el contenido a una página web ubicada en la <abbr>URL</abbr> https://www.bertolinabikes.com.ar/guias/manual-de-armado-para-encomiendas/. Al no haber un límite de espacio a ocupar, se utilizó tipografía grande (valor de 16px en móviles y 18px en tablets y computadoras de escritorio) que favorece la lectura sin que el usuario tenga que usar el zoom. Además, las ilustraciones se muestran en tamaño considerablemente más grande.

[ imagen manual de armado en la web ]


### Sobre

El manual está contenido en un sobre de tamaño un poco más grande que una hoja A5. Para abaratar costos se buscó un troquel ya hecho, que esté disponible en la imprenta donde se iba a imprimir el trabajo. En la imprenta Lenzioni nos presentaron un troquel para un sobre donde el manual entraba perfectamente.

También se eligió el papel obra de 120 gramos porque es un material grueso, casi del grosor de una cartulina. El papel obra, a diferencia del ilustración, no se arruga tanto cuando se le aplica cola vinílica.

El diseño de la parte exterior del sobre no tiene ninguna referencia al contenido interior: podría usarse para enviar el manual de uso o para enviar casi cualquier cosa. De esta forma, este mismo sobre puede ser utilizado para que la empresa envíe por correo cualquier tipo de carta, sin limitar la función de la pieza.

Luego de varias alternativas de diseño y un par de iteraciones para conseguir el color óptimo, se definió un diseño que contiene una fotografía con un filtro de "mapa de degradado". De un lado se ve a todo color y del otro se ve sobrio y serio, en color blanco, con sólamente el isologotipo y los datos de la empresa. Si se quisiera utilizar para un envío por correo, en el espacio en blanco se pueden escribir los datos del destinatario y queda lugar para que la oficina de correo pegue las estampillas necesarias.

[ ficha técnica sobre ]

### Certificado de Garantía

[ ficha técnica certificado de garantía ]

### Sticker

[ ficha técnica sticker]

# Presupuesto

[ presupuesto ]

# Conclusión

Este libro empezó con una introducción donde se explicaba el método de diseño conocido como "Human Centered Design" (<abbr>HCD</abbr>, Diseño Centrado en el Usuario), por eso me pareció pertinente terminar con una pequeña reflexión sobre la aplicación de este método.

El <abbr>HCD</abbr> fue de gran utilidad como un modelo para entender el proceso de diseño como un proceso de "caja transparente"[^aclaracionCJones], y por lo tanto desmitificarlo de la magia y la "genialidad creadora" con la que suele asociar. El diseño tiene potencial innovativo cuando se basa en la investigación y en la generación de alternativas; y, si bien el resultado nunca es "perfecto", es siempre mejor mientras más alternativas se generen.

> "Siempre hay un grupo de soluciones, una de las cuales es mejor bajo ciertas condiciones."
> — Karl Gerstner (1964)

Una de las premisas de este método es su naturaleza iterativa. En cada iteración se llegaría a una solución que supliría más efectivamente las necesidades de los usuarios. Como modelo más acertado al del doble-diamante, se llegó a representar este proceso como un “espiral” que se va cerrando: se va acercando a soluciones que resuelven cada vez mejor el problema (Don Norman, 2013). 


<!-- (¿Tal vez se pueda representar como un espiral formado por doble-diamantes..?) -->

En la iteración también hay límites menos obvios y más difusos de lo que se esperaba. Los mismos resultados de la etapa de testeo pueden ser, al mismo tiempo, las consignas para revisar en una próxima etapa de investigación. Por ejemplo, la próxima etapa de investigación contará con datos demográficos más precisos que provienen del sitio web, con lo que se podrían encontrar nuevas necesidades y oportunidades.

A la vez, la solución propuesta en una etapa puede crear necesidades nuevas. También puede actuar como disparador para replantearse objetivos. Por ejemplo, el comitente, durante el proceso, planteó nuevas necesidades a medida que se fueron generando soluciones:

- Necesidad de una forma de "desligarse" de mercadolibre en la página web. Se podría mostrar el precio de los productos mediante un sistema más eficiente, menos costoso, que permita mayor independencia (como puede ser un software para gestionar stock).
- Necesidad de una forma simple de generar gráficas de oferta para las redes sociales: generar gráficas de forma asistida de forma que sea fácil de usar sin depender de un diseñador.

Adicionalmente, se encontraron nuevas necesidades a medida que el proceso avanzaba:

- Necesidad de seguir trabajando en piezas gráficas para el espacio comercial.
- Necesidad de crear un plan de medios sociales (PSM) o similar, para planificar contenido regular que sirva para crear "novedades" en el sitio y publicaciones en redes sociales.

Estos serían los disparadores que podrían justificar una nueva etapa de investigación, en un proceso que se retroalimenta en sí mismo.

Ahora bien, si en este método nunca se llega a una solución "definitiva", ¿será que el diseño es constantemente necesario durante el crecimiento de las empresas y organizaciones?

Tal vez si la empresa creciese hasta cumplir todos sus objetivos, o si se cumpliese su visión, sería necesario plantear nuevos objetivos y una nueva visión. Sino la empresa se “estancaría” y dejaría de ser competitiva en el mercado.

Además, el mercado (altamente competitivo) le exigiría a las empresas una renovación constante de su propuesta visual para conservar el interés de los usuarios y destacar en el mismo (Chavez, 1990)[^chavez].

Por estas razones, ¿qué mejor abstracción para entender el proceso diseño como aquella que lo considere un proceso gradual y constante?

Pero aún renovandose constantemente, el *corpus* comunicacional de una institución debe siempre ser asociado con la misma identidad, con los mismos conceptos e ideas de fondo: he aquí un desafío para los diseñadores.

Por esto reivindico que el diseñador no maneja meramente "estilos" visuales (que fluctúan como las modas), sino "conceptos". La búsqueda de "lo bello" puede ser innovativa cuando el diseño está involucrado en el proceso de creación del producto y no sólamente como "un adorno", como "una carcaza" que se le pone por encima a los productos (Anna Calvera, 2013). El diseño debe ser parte de los procesos de toma de decisión de las instituciones.


[^chavez]: Chavez (1990), "La imagen corporativa: teoría y metodología de la identificación institucional", España (Cataluña): Gustavo Gili.
[^AnnaCalvera]: Anna Calvera, "De lo Bello de las Cosas" (2013), recuperado de http://www.fadu.edu.uy/estetica-diseno-i/files/2016/08/Anna-Calvera-De-lo-bello-de-las-cosas.pdf



<div style="page-break-after:right"></div>

# Notas y bibliografía

- 1964, Karl Ger­st­ner wrote Design­ing Pro­grammes


[^norman1]: Don Norman (2013), *The Design of Everyday Things (revised & expanded edition)*, Nueva York: Basic Books, p.220.

[^designpedia1]: Thinkers Co. (2015), *Designpedia: 80 herramientas para construir tus ideas*, Buenos Aires: LID Editorial Empresarial, p.28.

[^estadisticasArg1]: *Colectivo, el más elegido por los argentinos para ir a trabajar* (6 de julio de 2017), Urgente 24, Recuperado de <http://bit.ly/2tPD22c>

[^estadisticasArg2]: Pablo Tomino (26 de mayo de 2013), *Crece el uso de la bicicleta como medio de transporte*, La Nación, Recuperado de <http://bit.ly/1iAUbEF>

[^estadisticasInsta1]: Carlota Fominaya (30 de agosto de 2016), *Por qué los adolescentes prefieren Instagram*, <abbr>ABC</abbr> familia, Recuperado de <http://bit.ly/2bQkx4E>

[^wikiCiclismoMontania]: *Ciclismo de Montaña* (9 de agosto de 2017), Wikipedia. <http://bit.ly/1TZ8BeL>

[^subdisciplinas1]: *Tipos de bicicleta de carretera y montaña (MTB) y como elegir la talla.* (2015), mundorutas.com. <http://bit.ly/2uqVIK3>

[^wikiCicloturismo]: *Cicloturismo* (9 de agosto de 2017), Wikipedia. <http://bit.ly/2vGz6oR>

[^wikilockCicloturismo1]: *Las mejores rutas cicloturismo en Córdoba (Argentina) : Wikiloc* (9 de agosto de 2017), Wikiloc. <http://bit.ly/2wJAmnZ>

[^aclaracionCJones]: Inspirado en las teorías de Christopher Jones. Él afirmaba que existían dos tipos de métodos de diseño, los métodos “de caja transparente”, porque están basados en la racionalidad y en el control sobre el proceso (la caja transparente nos permite “ver a través”, que —según Jones—, es la característica de la racionalidad); y los métodos de “caja negra”, basados en la creatividad, que es considerada como misteriosa, opaca. Habiendo dicho esto se puede criticar un aspecto de esta teoría: tanto la creatividad, la racionalidad y el control sobre el proceso actúan conjuntamente como una unidad simultánea y de intercambio constante, y esta dualidad (caja negra, caja transparente) es una forma demasiado simplista de ver el ejercicio de diseñar. Christopher Jones (1982), *Métodos de diseño*. Recuperado de una adaptación realizada por la prof. Pía Reynoso, profesora de la carrera Diseño Gráfico y Publicitario de la Universidad Provincial de Córdoba.

[^wendt]: La cita de Allan Chochinov aparece en el libro de Thomas Wendt, *Design for Dasein: Understanding the Design of Experiences* (2015), pág.6. Recuperado de <http://amzn.to/2ueaUG9>. La fecha de la frase es del 2014 porque Wendt y otros usuarios la twitearon en esa fecha, según el *tweet de @Thomas_Wendt* (22 de abril de 2014) <http://bit.ly/2fVkknL>

[^wikilocRuta17km]: PSK74 (15 de octubre de 2015), *Alta Gracia - Copina -El Condor-San Clemente -Alta Gracia* [sic], Wikiloc. <http://bit.ly/2vR2usK>

[^shimano]: *Shimano Bycicle Components* (13 de agosto de 2017). Recuperado de <http://bit.ly/2vSTzXr>

[^mercadopoliticas]: *Políticas de Publicación* (consultado el 8 de agosto del 2017). Recuperado de <http://bit.ly/2vbrrvP>

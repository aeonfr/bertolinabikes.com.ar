---
title: "Diseño de Contenido para la sección Novedades"
date: 2017-12-07T15:45:27-03:00
draft: false
toc: true
---

Esta sección determina las imágenes que se muestran en el carrousel de la página de inicio.

Para agregar una novedad, navegar a la carpeta donde se ubica tu repositorio, y desde la consola ejecutar el siguiente comando:

```
hugo new novedades/identificador-de-la-novedad.md
```

(donde `identificador-de-la-novedad.md` puede ser cualquier nombre de archivo con extensión `.md` o `.html`).

Luego de ejecutar este comando, si iniciaste el entorno de desarrollo previamente (con el comando `hugo serve`), la nueva publicación se verá en la siguiente URL:

https://localhost:1313/novedades/identificador-de-la-novedad

Ten en cuenta que hasta que no agregues el metadato imágen a la novedad, no podrás verla en la página de inicio.

{{% info %}}

Recuerda que para ejecutar estos comandos, es necesario seguir los pasos para instalar Hugo e iniciar el entorno de desarrollo, visitando [esta guía](/meta/modificar-el-contenido/).

{{% /info %}}

# Formato de imagen

Las novedades están vinculadas con una imágen que se muestra en el carrousel de la página de inicio. Por conveniencia, la relación de aspecto de dicha imágen es igual al formato de una foto de portada en Facebook, excepto que al doble de tamaño (porque tiene que ocupar más espacio).

**Tamaño de imagen de la publicación en "Novedades":** 1640x624px

**Formato de archivo recomendado:** JPG

Sin embargo, hay que tener en cuenta que este formato es muy "chato" para celulares, donde se achica a resoluciones de hasta 320px, quedando apenas visible la imágen. Por lo tanto en celulares **la imagen se recorta por el centro** a una relación de aspecto 1:1.5.

**Tamaño de la imagen para móviles:** 936x624px

Por lo tanto se deben diseñar imágenes de 1640x624px, pero poniendo toda la información escencial en los 936x624px del centro de la imagen (como en el ejemplo de abajo).

![Ejemplo](/recursos/imagenes/meta/ejemplo-novedades.jpg)

# Cómo vincular la imagen con la publicación

El metadato imágen se vincula con el ID de una imágen subida en Cloudinary y con el nombre de usuario de quien la subió. Para aprender más sobre cómo usar Cloudinary, lee [este artículo](/meta/trabajando-con-cloudinary-para-imagenes/).

Este es un ejemplo de cómo se ven los metadatos de una novedad:

```
---
title: "Lanzamiento de nuestro nuevo sitio web"
date: 2017-11-17T20:35:04-03:00
draft: false
cloudinary: aeonfr
imagen: v1512673441/bertolina/bienvenidos-al-nuevo-sitio-3.jpg
---
```
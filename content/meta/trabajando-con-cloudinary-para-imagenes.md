---
title: "Trabajando con Cloudinary para Imagenes"
date: 2017-11-20T15:39:58-03:00
draft: false
toc: true
---

# Por qué se utiliza Cloudinary

**TL;DR**: Para obviar por completo a Node.js y Gulp en el entorno de desarrollo (resultaba lento y poco flexible), y para servir imágenes más livianas y en el tamaño justo para cada dispositivo.

Las imágenes deben ser servidas para la web en tamaños razonables, caso contrario, el navegador web tardará mucho en descargarlas, influyendo negativamente en la experiencia del usuario.

Como en la página web hay varias vistas, como pueden ser la vista de [productos como lista](/productos) y la vista de [producto simple](/productos/venzo-amphion), la imagen del producto se necesita en varios formatos. Así mismo, en las galerías del producto se utiliza una versión miniatura de la imagen, que una vez abierta se descarga en alta calidad. Pero no son sólo las imágenes de productos que se necesitan en varias calidades, las imágenes de la sección novedades (que se muestran en una galería en la página de inicio) también se necesitan en varias calidades.

En un primer momento se creó una tarea para redimensionar las imagenes, que corria en el entorno de desarrollo mediante **Gulp** (automatizador de tareas que funciona en el entorno Node.js). Dicha tarea controlaba los cambios en la carpeta *`/imagenes/`* y producia miniaturas (o _thumbnails_) de las mismas en la carpeta *`/thumbnails/`*. El problema principal de este método es el bajo rendimiento y las limitaciones a la hora de detectar imágenes borradas o que cambiaron de nombre. Cada vez que se iniciaba (o reiniciaba) el entorno de desarrollo, **Gulp** tenía que crear nuevamente todas las miniaturas, lo que tardaba una cantidad de tiempo considerable (aproximadamente un minuto cada 128 imágenes). Esta solución no parece muy escalable.

Como **Gulp** es muy lento, también se creó [un programa que redimensionaba las imágenes](/meta/imgrsizr). La lógica era tener un programa que haga una reducción previa del tamaño de archivo, que además generaba imágenes con la marca de agua en una posición igual para todas las imágenes. De esta forma se obtiene, por un lado, la misma marca de agua en todas las imágenes, por otro lado, se reduce previamente el tamaño del archivo para que la tarea de Gulp tenga que realizar "menos trabajo".

Cloudinary es una solución que se eligió como un servicio que comprime y redimensiona las imágenes automáticamente. La tercerización de este aspecto permite delegar la correcta compresión, almacenamiento en caché y redimensión de imágenes a este servicio. De esta forma se puede trabajar con un entorno de desarrollo más ligero, **pudiendo obviarse completamente Gulp y Node.js**; y a la vez servir a los usuarios con imágenes del tamaño justo que necesitan —de acuerdo a su dispositivo—, sin desperdiciar su ancho de banda o plan de datos.

# Configurar una cuenta en Cloudinary

Para empezar a usar cloudinary en vez de Gulp, basta con crearse una cuenta en el servicio, desde la web [cloudinary.com](https://cloudinary.com/) y recordar el nombre de usuario.

Una vez creada tu cuenta, desde el [panel de administración de medios](https://cloudinary.com/console/media_library) (imagen 1) se podrán subir todas las imágenes que se crean necesarias, siendo posible también organizarlas en carpetas.

{{<figure src="/recursos/imagenes/meta/cloudinary-01.jpg" caption="Imagen 1" >}}

# El ID de cloudinary

Al momento de subir una imagen en tu cuenta de Cloudiary, se puede hacerle click y obtener información adicional. Desde la pantalla de información adicional (imagen 2), se pude obtener **un identificador y un nombre de la imagen**, que es el que vamos a usar.

{{<figure src="/recursos/imagenes/meta/cloudinary-02.jpg" caption="Imagen 2." >}}

## Entendiendo la URL de la imagen

El identificador de la imagen es la parte de la URL que aparece en amarillo y empieza con un código, luego tiene la carpeta donde se ubica la imagen (si hubiere), y luego el nombre del archivo con la extensión. (El ID es la cadena completa **[codigo]/[carpeta]/[nombre.jpg]**).

Una imagen JPG tendría la siguiente URL, con el ID marcado en negrita.

`http://res.cloudinary.com/`\
`:nombre_de_usuario:/`\
`image/upload/`\
**`[codigo]/[carpeta]/[nombre.jpg]`**

En el caso del ejemplo en la imagen 2, la URL es:\
[`http://res.cloudinary.com/aeonfr/image/upload/v1510660145/bertolina/hero-img.jpg`](http://res.cloudinary.com/aeonfr/image/upload/v1510660145/bertolina/hero-img.jpg)

Y el id es:\
`v1510660145/bertolina/hero-img.jpg`

**Nota: no confundir el ID de la imagen con las transformaciones**:

El id es, generalmente, todo lo se ubica luego de `images/upload/` en la URL completa de la imagen. Sin embargo, al aplicar una transformación sobre la imagen, la url cambia.

Por ejemplo, la url de la imagen podría verse así:

`.../image/upload/`**`c_lfill,w_1080/`**`v1510660145/bertolina/hero-img.jpg`

Esta cadena (marcada en negrita) en la URL especifica que la imagen será **transformada**, se redimencionará a 1080px siempre y cuando la imagen no tenga menos de 1080px de ancho (si tiene menos, se dejará tal como está). Las transformaciones se llevan a cabo de forma automática y **no deben ser incluidas en el id**.

# El metadato "imagen"

El metadato imagen, que anteriormente contenía la dirección de una imagen almacenada en `/static/imagenes/` (y con una miniatura en `/static/thumbnails/`), debe contener un valor alternativo si se trabaja con cloudinary.

Para indicarle a **Hugo** que en tu publicación se van a subir las imágenes a través de Cloudinary, y no mediante el método anterior, se debe agregar este metadato a la publicación:

```
---
cloudinary: "tu_nombre_de_usuario"
...
---
```

El metadato "imagen" debe contener el id de la imagen en cloudinary, tal como se explicó en el paso anterior.


```
---
cloudinary: "nombre_de_usuario_cloudinary"
imagen: "v1510660145/bertolina/hero-img.jpg"
...
---
```

# Cloudinary para generar Galerías de productos

Al momeno de generar una galería, también se puede usar Cloudinary en vez del metodo anterior.

Primero que nada, para indicarle a **Hugo** tu nombre de usuario en cloudinary, se debe agregar este metadato a la publicación:

```
---
cloudinary: "tu_nombre_de_usuario"
...
---
```

Al momento de declarar el metadato "gallery", debe realizarse igual que siempre, pero reemplazando la línea "file" por "cid" (cloudinary id).

```
---
cloudinary: "tu_nombre_de_usuario"
gallery:
  - cid: "v000/ejemplo.jpg"
    size: "1200x900"
    description: "Descripción de la imagen..."
...
---
```

El atributo size, tal como ya se trabajaba anteriormente, debe indicar el tamaño de la imagen tal como se la subió a Cloudinary. Tal como ya se venía trabajando, no se recomienda utilizar imágenes de tamaños exesivamente grandes, pero tampoco muy chicas (1200px de ancho o altura máxima es un buen número).

Es posible que en una misma galería haya imágenes que se sirvan desde cloudinary e imágenes que se sirvan desde el mismo sitio web. Por ejemplo, en esta galería hay una imagen alojada en el repositorio y otra en la nube de cloudinary.

```
gallery:
  - cid: "v000/imagen-en-cloudinary.jpg"
    size: "1200x900"
  - file: "imagen-en-el-repositorio.jpg"
    size: "900x1200"
```

# El shortcode "img"

Los shortcodes son abreviaciones que permiten generar estructuras de contenido simples dentro del contenido de una publicación. Se creó un shortcode "img" que automáticamente agrega una imágen al cuerpo de la publicación. Esta imagen, si se agrega mediante Cloudinary, se verá a la máxima calidad posible de acuerdo a la resolución y densidad de píxeles de cada dispositivo.

```
{{/*% img cloudinary="aeonfr" cid="v1511223332/bertolina/luz-trasera-5-leds.jpg" alt="Texto explicativo para lectores de pantalla" %*/}}
```

{{% img cloudinary="aeonfr" cid="v1511223332/bertolina/luz-trasera-5-leds.jpg" %}}

Sin embargo también se le puede dar un uso común a este shortcode, en ese caso es similar a la etiqueta nativa de Markdown...

```
{{/*% img src="//res.cloudinary.com/aeonfr/image/upload/v1511223332/bertolina/luz-trasera-5-leds.jpg" alt="Texto explicativo para lectores de pantalla" %*/}}
<!-- similar a escribir ![texto explicativo para lectores de pantalla](//res.cloudinary.com/aeonfr/image/upload/v1511223332/bertolina/luz-trasera-5-leds.jpg) -->
```

Una cuestión importante a tener en cuenta al linkear en cloudinary es borrar el "http:" o el "https:". El link debería, óptimamente, empezar así... "//res.cloudinary...". Si el link empezase en "https:" no habría problema, sin embargo si el link empezase en "http:" sí, ya que los usuarios que navegan en "https" (conección segura) no van a poder cargar la imagen.


---
title: "Política de privacidad (actualizada el 08/06/2018)"
date: 2018-06-08
layout: page
---

# Política de privacidad

Actualizada el 08/06/2018

## Resumen

> Recolectamos datos de forma anónima para entender la forma en que es usado este sitio web. Los datos nos son suministrados a través de la plataforma Google Analytics, exclusivamente (no usamos otra herramienta de analítica). No instalamos cookies en tu dispositivo, pero Google Analytics podría hacerlo. Para desactivar el seguimiento de Google Analytics, puedes <a href="#" onclick="localStorage.setItem('disable-analytics', true);return alert('Google Analytics Desactivado');">hacer click aquí</a>. (Nota: debes clickear el enlace en todos los navegadores y dispositivos que uses.)

## 1. Datos que recolectamos

Recolectamos datos personales y de uso de los clientes que utilizan este sitio web, de forma totalmente anónima, a través de Google Analytics. Estos datos son manejados por Google y son almacenados en sus servidores. Al usar el sitio damos por entendido que consientes con este tipo de uso de tus datos.

### 1.1. Datos personales

Los datos que recolectamos nos son suministrados de forma totalmente anónima por Google. Sólo nos está permitido consultar los datos personales del total de usuarios, como porcentajes: no tenemos un seguimiento de sesiones individuales. Estos datos se corresponden con datos que podrías haber compartido con Google a través de tu cuenta de Gmail: tu rango de edad, tu país, tus gustos (Google podría inferir esto basado en tus búsquedas previas), tu idioma y tu sexo. También podemos consultar datos sobre el tipo de palabras clave que más se usan en la búsqueda de Google para llegar a nuestro sitio web. Recuerda que a estos datos no accedemos de forma particular, por sesión, sino que sólo los obtenemos como una estadística global, sumados a los de todos los  otros usuarios del sitio.

### 1.2. Datos de uso

Adicionalmente, Google Analytics nos suministra información relacionada con tu uso en el sitio, también de forma anónima. Estos datos pueden incluir: el dispositivo y navegador que usas, la frecuencia con la que visitas el sitio, los canales desde los cuales visitas el sitio, los links externos que clickeas y las páginas que visitas.

### 1.3. Propósito de la recolección de datos

Los datos almacenados se utilizan con fines estadísticos para evaluar la efectividad de cambios en el diseño y en el sitio. Entendemos por efectividad, en este contexto, la capacidad que tenga para mantener a los usuarios mayor tiempo en el sitio, la capacidad para fomentar que los usuarios visiten mayor cantidad de páginas, la capacidad para permitirles a los usuarios llegar a las páginas que buscan de forma más rápida y la capacidad para incrementar las ventas a través de los links hacia MercadoLibre y TodoPago suministrados en el sitio.

### 1.4. Tratamiento de datos personales al momento de realizar una compra

Por motivos legales, en el momento en que realices una compra debemos recolectar tus datos personales tales como tu nombre, dirección de envío, email, entre otros (el tipo de datos podría variar según la plataforma en que realices la compra). Tratamos estos datos con sumo cuidado y sólo con la finalidad de proveer el servicio.

### 1.5. Con respecto a usuarios de Europa

A pesar de no proveer ningún servicio en Europa, nuestro sitio es igualmente accesible desde países europeos. Como forma de acordar con el Reglamento General de Protección de Datos, deshabilitamos los *scripts* de Google Analytics para usuarios de países Europeos. Deducimos tu país a través de la zona horaria de tu dispositivo (para Europa: UTC+0, UTC+1, UTC+2, UTC+3 y UTC+4).

### 2. Sitios de terceros

Los sitios de terceros que figuran enlazados en los artículos de este sitio, como MercadoLibre y TodoPago, cuentan con su propia política con respecto a los datos personales, y no necesariamente coincide con nuestra política.

### 3. Actualización de nuestra política de privacidad

Este documento podría actualizarse en el futuro. Se ha decidido incluir el enlace a este documento, junto con la fecha de última actualización, en el pie de página para todas las páginas de este sitio.

### 4. Consentimiento con la política de privacidad

Al continuar utilizando este sitio entendemos que aceptas y entiendes 

"use strict";

var apiRequest = require('../_inc/apiRequest.js');
var fs         = require('fs');
var csvWriter  = require('csv-write-stream');

var headers = [
    'id (tiene que coincidir con URL en la web) (si el producto aún no está subido en la web, inventar un nombre sin tildes, mayúsculas, espacios: sólo letras, números y "-" (generalmente se usó "marca-modelo", ejemplo "venzo-raptor")) (*requerido)', // [0]
    'nombre (este campo no se tiene en cuenta, sólo lo dejo para que sirva como referencia)', // [1]
    'id en mercadolibre (sin "MLA-")', // [2]
    'precio 6 cuotas (si está vacío y hay ID en Mercadolibre, se usará el precio de MercadoLibre)', // [3]
    'multiplicador precio de contado (0.93)', // [4]
    'multiplicador precio en 12 cuotas (1.12)', // [5]
    'usa esta columna o las siguientes para lo que quieras! (notas, etc.)',
    'nota: para ver un producto en MercadoLibre dede su ID, seguir este link (SE ACTUALIZA SOLO MEDIANTE UNA FORMULA, CUANDO CAMBIE EL ID)',
    'nota 2: se pueden cambiar los titulos de las columnas y eliminar los (*comentarios*) si se prefiere',
];

var writer = csvWriter({
    headers: headers
});
writer.pipe(fs.createWriteStream('./data/prices.csv'));

console.log('------- API Request');

apiRequest('http', {
    host: 'localhost',
    port: 1313,
    path: '/productos/index.json',
    method: 'GET'
}, function(response){
    var data = JSON.parse(response);

    for (let i = 0; i < data.length; i++) {

        let item = data[i];
        writer.write([
            item.slug, // [0]
            item.title, // [1]
            (item.mercadolibre_id || ''),
            '', // [3]
            (item.lowest_price_multiplier == 0.93) ? '' : item.lowest_price_multiplier, // [4]
            (item.highest_price_multiplier == 1.12) ? '' : item.highest_price_multiplier, // [5]
        ]);
    }
    
    
    writer.end()
    
});

var fs = require('fs');
var yamlFront = require('yaml-front-matter');
var json2yaml = require('json2yaml');

var yyyymmdd = function(date) {
    if (typeof date =='string') return date;
    var mm = date.getMonth() + 1; // getMonth() is zero-based
    var dd = date.getDate();

    return [date.getFullYear() + '-',
            (mm>9 ? '' : '0') + mm + '-',
            (dd>9 ? '' : '0') + dd
            ].join('');
};

var targetDirectory = 'content/productos/';

var comprarDirectory = 'content/comprar/';

var trashDirectory = '_functions/migrateContentToProd/deleted/';

fs.readdir(targetDirectory, function(err, files) {

    if (err) throw err;

    for (let i = 0; i < files.length; i++) {
        const file = files[i];
        
        fs.readFile(targetDirectory+file, function(err, content){
            if (err) throw err;

            const fileFrontMatter = yamlFront.loadFront(content);


            /**
             * Duplicate de object and prepare it for the creation of a new file or files
             */
            var newContentBoot = Object.assign({}, fileFrontMatter);
            delete newContentBoot.__content;
            delete newContentBoot.envio_gratis;
            delete newContentBoot.comprar;
            delete newContentBoot.date;
            newContentBoot.date = yyyymmdd(fileFrontMatter.date);

            if (fileFrontMatter.comprar) {
                
                
                /**
                 * El producto está asociado a una sóla compra
                 * Acción:
                 * Llevar title, mercadolibre_id & price_multiplier al producto principal,
                 * y borrar el de compra
                 */
                if (fileFrontMatter.comprar.length == 1) {
                    fs.readFile(comprarDirectory + fileFrontMatter.comprar[0] + '.md', function(err, comprarContent) {
                        if (err) throw err;

                        const comprarFileFrontMatter = yamlFront.loadFront(comprarContent);

                        newContentBoot.title = comprarFileFrontMatter.title;
                        newContentBoot.mercadolibre_id = comprarFileFrontMatter.mercadolibre_id;
                        newContentBoot.lowest_price_multiplier = comprarFileFrontMatter.lowest_price_multiplier;

                        var newContent = json2yaml.stringify(newContentBoot) + '---\r\n' + fileFrontMatter.__content ;

                        fs.writeFile(targetDirectory+file, newContent, null, function(err){
                            if (err) throw err;
                            console.log(targetDirectory+file, 'edit');
                            fs.unlink(comprarDirectory + fileFrontMatter.comprar[0] + '.md', function(err){
                                if (err) throw err;
                                console.log(comprarDirectory + fileFrontMatter.comprar[0] + '.md', 'delete');
                            });
                        });

                    });
                    
                } else {
                    /**
                     * El producto está asociado a más de una compra
                     * Acción:
                     * Eliminar el producto principal  y los de compra (mover al "trash" directory)
                     * Crear dos productos nuevos en base al contenido en "comprar/"
                     */
                    fs.unlink(targetDirectory+file, function(err){
                        // Main product deleted
                        if (err) throw err;
                        
                        // Loop though compras
                        for (let e = 0; e < fileFrontMatter.comprar.length; e++) {
                            fs.readFile(comprarDirectory + fileFrontMatter.comprar[e] + '.md', function(err, comprarContent){
                                if (err) throw err;
        
                                const comprarFileFrontMatter = yamlFront.loadFront(comprarContent);
        
                                newContentBoot.title = comprarFileFrontMatter.title;
                                newContentBoot.mercadolibre_id = comprarFileFrontMatter.mercadolibre_id;
                                newContentBoot.lowest_price_multiplier = comprarFileFrontMatter.lowest_price_multiplier;
    
                                if (newContentBoot.gallery){
                                    delete newContentBoot.gallery;
                                }
    
                                var newContent = json2yaml.stringify(newContentBoot) + '---\r\n' + comprarFileFrontMatter.__content;
    
    
                                // create new product from this compra
                                fs.writeFile(targetDirectory + fileFrontMatter.comprar[e] + '.md', newContent, null, function(err){
                                    if (err) throw err;
                                    console.log(targetDirectory+fileFrontMatter.comprar[e] + '.md', 'create');
                                    //delete the compra
                                    fs.unlink(comprarDirectory + fileFrontMatter.comprar[e] + '.md', function(err){
                                        if (err) throw err;
                                        console.log(comprarDirectory + fileFrontMatter.comprar[e] + '.md', 'delete');
                                    });
                                });
        
        
                            });
                        }
                    });

                }

            }

        })
    }
});
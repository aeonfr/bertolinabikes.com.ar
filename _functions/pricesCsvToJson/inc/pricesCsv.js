var csvreader = require('csvreader');

var parsed = [];

function recordHandler(data){
    if (data[0] && data[0] != ''){

        var item = {
            slug: data[0],
            title: data[1],
            mercadolibre_id: data[2],
            price: data[3],
            lowest_price_multiplier: data[4],
            highest_price_multiplier: data[5]
        };
        parsed.push(item);
    }
}


var options = {
    skip: 1
};




module.exports = {
    load: function(csvFileName){
        return new Promise(function(resolve, reject) {
            csvreader
            .read(csvFileName, recordHandler, options)
            .then(function(){
                resolve(parsed);
            })
            .catch(err => {
                reject(err);
            });
        });
    },
}
var fs = require('fs');
var json2yaml = require('json2yaml');
var _ = require('lodash');
var apiRequest = require('../../_inc/apiRequest.js');
var yyyymmdd = require('../../_inc/yyyymmdd.js');

var config = JSON.parse(fs.readFileSync('./data/config.json'));

var product = {};

product.exists = function (slug){
    return fs.existsSync('./content/productos/' + slug + '.md');
}
// API calls
product.getFromMercadolibre = function(mercadolibre_id){
    return new Promise(function (resolve, reject) {
        apiRequest('https', {
            host: 'api.mercadolibre.com',
            path: '/items/MLA' + mercadolibre_id,
            method: 'GET'
        }, function(response){
            if (!response) reject();
            resolve(JSON.parse(response));
        });
    });
}
product.getGalleryFromMercadolibre = function(mercadolibreData){

    var galleryItems = [];

    function recursiveGetGalleryItem(data, index, callback){
        var val = data[index];
        apiRequest('https', {
            host: 'api.mercadolibre.com',
            path: '/pictures/' + val.id,
            method: 'GET'
        }, function(response){
            var galleryItem = JSON.parse(response);
            galleryItems.push({
                id: galleryItem.id,
                imgUrl: true,
                size: galleryItem.variations[0].size,
                url: galleryItem.variations[0].secure_url,
                thumbnail_url: mercadolibreData.pictures[index].secure_url
            });

            index++;

            if (data[index]) recursiveGetGalleryItem(data, index, callback);
            else callback();

        });

    }

    return new Promise(function(resolve, reject){
        recursiveGetGalleryItem(mercadolibreData.pictures, 0, function(){
            resolve(galleryItems);
        })
    });
}
//utils
product.writeProductFile = function({ slug, mercadolibre_id, mercadolibreData }){
    return new Promise(function(resolve, reject){
        // get description(s?)
        apiRequest('https', {
            host: 'api.mercadolibre.com',
            path: '/items/MLA' + mercadolibre_id + '/descriptions',
            method: 'GET'
        }, function(response){
            var mercadolibreDescriptions = JSON.parse(response);
            var productFileObject = {
                title: mercadolibreData.title,
                date: yyyymmdd(new Date()),
                mercadolibre_id: mercadolibreData.id.replace('MLA', ''),
                imgUrl: true,
                imagen: (mercadolibreData.pictures[0]) ? 
                mercadolibreData.pictures[0].url.replace(/-O/, '-F') : '',
                marcas: _.find(mercadolibreData.attributes, { name: 'Marca' }) ? [ _.find(mercadolibreData.attributes, { name: 'Marca' }).value_name ] : [],
                description: "",
                categorias: [],
                weight: -99
            };

            var fileData = json2yaml.stringify(productFileObject)
                + '---\r\n'
                + mercadolibreDescriptions[0].plain_text.replace('-BERTOLINA BIKES-', '');
            // now write the file (in a temp folder, since this items need revisions)
            console.log('-- WRITE -- ', slug+'.md')
            fs.writeFileSync('./_functions/temp/' + slug + '.md', fileData);
            resolve();
        });
    });

}
product.writeGalleryFile = function({ mercadolibreData }){
    return new Promise(function(resolve, reject){
        product.getGalleryFromMercadolibre(mercadolibreData)
        .then(function(data){
            
            console.log('-- WRITE -- gallery/', mercadolibreData.id.replace('MLA', '') + '.json');
            var galleryJSON = JSON.stringify(data, null, 2);
            fs.writeFileSync('./data/galleries/' + mercadolibreData.id.replace('MLA', '') + '.json', galleryJSON);
            resolve();
        });
    })
}
product.getPrice = function({ mercadolibreData, price, lowest_price_multiplier, highest_price_multiplier }){
    var prices = {};

    if (price){
        prices["6paymentsPrice"] = parseFloat(price.replace(',','.'));
    } else {
        prices["6paymentsPrice"] = parseFloat(mercadolibreData.price);
    }

    var lpm = (lowest_price_multiplier == '' || parseFloat(lowest_price_multiplier.replace(',','.')) == NaN)
                ? config.default_lowest_price_multiplier
                : parseFloat(lowest_price_multiplier.replace(',','.'));
    var hpm = (highest_price_multiplier == '' || parseFloat(highest_price_multiplier.replace(',','.')) == NaN)
                ? config.default_highest_price_multiplier
                : parseFloat(highest_price_multiplier.replace(',','.'));

    prices["cashPrice"] = Math.round(10 * prices["6paymentsPrice"] * lpm) /10;
    prices["12paymentsPrice"] = Math.round(10 * prices["6paymentsPrice"] * hpm) /10;

    prices["lowest_price_multiplier"] = lpm;
    prices["highest_price_multiplier"] = hpm;

    prices["warranty"] = mercadolibreData.warranty;
    return prices;
}
//public interface:
product.syncFromMercadolibre = function({ mercadolibre_id, slug, price, lowest_price_multiplier, highest_price_multiplier }) {

    return new Promise(function(resolve, reject){

        if (mercadolibre_id){
            /**
             * Flujo 1: actualizar producto desde mercadolibre
             */
            product.getFromMercadolibre(mercadolibre_id)
            .then(function(mercadolibreData){
                if (!product.exists(slug)) {
                    /**
                     * Create a product and gallery
                     */
                    product.writeProductFile({ slug, mercadolibre_id, mercadolibreData, lowest_price_multiplier, highest_price_multiplier })
                    .then(function(){
                        product.writeGalleryFile({ mercadolibreData })
                        .then(function(){

                            var prices = product.getPrice({mercadolibreData, price, lowest_price_multiplier, highest_price_multiplier})
                            resolve(prices);
                        })
                    });
                } else {
                    var prices = product.getPrice({mercadolibreData, price, lowest_price_multiplier, highest_price_multiplier})
                    resolve(prices);
                }
            });
        } else {
            /**
             * Flujo 2: actualizar producto sólamente con valor para precio
             */
            resolve({});
        }
    });
    
}

module.exports = product;
var pricesCsv = require('./inc/pricesCsv.js');
var product = require('./inc/product.js');
var fs = require('fs');

var pricesCsvPath = './_functions/pricesCsvToJson/prices.csv';
console.log('Loading', pricesCsvPath);

var pricesResult = {};

function recursiveDataLoad(data, index, callback){
    var val = data[index];

    index++;

    if (val.mercadolibre_id == ''){
        if (val.price) console.warn('El flujo para actualizar un producto contando únicamente con un valor para precio aún no está definido')
        
        if (data[index]) recursiveDataLoad(data, index, callback);
        else callback();
    } else {
        product.syncFromMercadolibre(val)
        .then(function(price){
            pricesResult[val.mercadolibre_id] = price;
    
            if (data[index]) recursiveDataLoad(data, index, callback);
            else callback();
    
        });
    }

}

pricesCsv.load(pricesCsvPath)
.then(function(data){
    console.log('Loaded', data.length, 'products from CSV file')
    recursiveDataLoad(data, 0, function(){
        console.log('---- WRITE data/prices.json');
        fs.writeFileSync('./data/prices.json', JSON.stringify(pricesResult, null, 2));
    });
});
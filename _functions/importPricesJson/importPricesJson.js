"use strict";

var apiRequest = require('../_inc/apiRequest.js');
var fs = require('fs');



console.log('Starting...');

var prices = {};



apiRequest('http', {
    host: 'localhost',
    port: 1313,
    path: '/productos/index.json',
    method: 'GET'
}, function(response){
    var data = JSON.parse(response);
    function queryApi(i, callback){
        const bkData = data[i];
        if (!bkData.mercadolibre_id) {
            //increment
            i++;
            if (data[i])
                queryApi(i, callback);
            else
                callback();

            return;
        }
        apiRequest('https', {
            host: 'api.mercadolibre.com',
            path: '/items/MLA' + bkData.mercadolibre_id,
            method: 'GET'
        }, function(response){
            var mlData = JSON.parse(response);
            prices[bkData.mercadolibre_id] = {
                'cashPrice': Math.round(mlData.price * bkData.lowest_price_multiplier * 10)/10,
                '6paymentsPrice': mlData.price,
                '12paymentsPrice': Math.round(mlData.price * bkData.highest_price_multiplier*10)/10,
                'warranty': mlData.warranty
            };
            //increment
            i++;
            if (data[i])
                queryApi(i, callback);
            else
                callback();
        })
    };
    
    console.log('---------- Begin API queries');
    queryApi(0, function(){
        console.log('---------- End API queries');
        var json = JSON.stringify(prices, null, 2);
        fs.writeFile('./data/prices.json', json, 'utf8', function(){
            console.log('---------- WRITE prices.json');
            return;
        });
    });
    
});
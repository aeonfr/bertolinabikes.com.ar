module.exports = function(protocol, options, callback){
    if (protocol == 'http')
        var driver = require('http');
    else
        var driver = require('https');
    
    
    var body='';

    var request = driver.request(options, function(res) {
        console.log(options.host+options.path, " with StatusCode: ", res.statusCode);
        res.on('data', function (chunk) {
            body += chunk;
        });
    });

    request.on('error', (e) => {
        console.error(e);
        return callback(new Error("Request error"));
    });

    request.on('close', function(){
        return callback(body); // <-- Call callback!
    });

    request.end();

    return;
}
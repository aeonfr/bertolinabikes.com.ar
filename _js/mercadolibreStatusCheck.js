var $mercadolibre = document.getElementById('mercadolibre'),
  $mercadoLinks = document.querySelectorAll('#mercadolibre a[href]');


checkMercadoLinksRecursive($mercadoLinks, 0);

function checkMercadoLinksRecursive($mercadoLinks, iterationNumber) {
  var $link = $mercadoLinks[iterationNumber],
    linkHref = $link.href,
    productIdRgx = linkHref.replace(/(^.+MLA-)/, '')
    .match(/^[0-9]+/);
  if(productIdRgx[0]){
    var productId = productIdRgx[0];
    axios.get('https://api.mercadolibre.com/items/MLA' + productId)
    .then(function(response){
      var productData = response.data;

      if (productData.status != 'active'){
        $link.style.opacity = '0.5';
        $link.querySelector('.description').innerHTML = 
        '<b class="f6 nl all-caps">['
        + ( (productData.status == 'paused') ? 'Stock Agotado' : 'Publicación no disponible')
        + ']</b> '
        + $link.querySelector('.description').innerHTML;

        $link.parentNode.appendChild($link);
        
      } else if (productData.price && $link.querySelector('.left-info')){
        $link.querySelector('.left-info').innerHTML = 
        ((productData.shipping && productData.shipping.free_shipping) ? '<img class="dib h1 m0 v-sub o-75" src="/recursos/imagenes/svg-text/oca-gratis.svg"> ' : '')
        + '$' + productData.price;
      }

      if (productData.title && productData.secure_thumbnail){

        $link.setAttribute('title', productData.title + ((productData.warranty) ? ' - ' + productData.warranty : '') );

        if (window.matchMedia('(min-width: 30rem)')){
          $link.querySelector('.thumbnail').classList.remove('w1');
          $link.querySelector('.thumbnail').classList.remove('h1');
          $link.querySelector('.thumbnail').style.margin = '-1rem 1rem -1rem -1rem';
          $link.querySelector('.thumbnail').innerHTML = '<img class="m0 w3 h3" src="' + productData.secure_thumbnail + '" style="mix-blend-mode:multiply" />';
        }
      }

      if ( $mercadoLinks[ iterationNumber+1 ] )
        return checkMercadoLinksRecursive( $mercadoLinks, (iterationNumber+1) );
      else return 0;
    });
  } else {
    if ( $mercadoLinks[ iterationNumber+1 ] )
      return checkMercadoLinksRecursive( $mercadoLinks, (iterationNumber+1) );
    else return 0;
  }

}
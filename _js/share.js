function shareInit(){
  document.getElementById('share-menu').style.height = '4rem';
  document.getElementById('share-menu').removeAttribute('aria-hidden')
  document.getElementById('share-menu-div-arrow').classList.remove('hidden');
  document.getElementById('openShare').classList.add('hidden');
  document.getElementById('closeShare').classList.remove('hidden');
}

function shareClose(){
  document.getElementById('share-menu').style.height = '0';
  document.getElementById('share-menu').setAttribute('aria-hidden', 'true')
  document.getElementById('share-menu-div-arrow').classList.add('hidden');
  document.getElementById('openShare').classList.remove('hidden');
  document.getElementById('closeShare').classList.add('hidden');
}

window.addEventListener('load', function() {

  var currentLinkInput = document.getElementById('currentLinkInput'),
  currentLink = currentLinkInput.value,
  copiado = document.getElementById('copiado');

  currentLinkInput.addEventListener('focus', function(){
    currentLinkInput.select();
    this.select();document.execCommand('copy');
    copiado.classList.remove('hidden');
    window.setTimeout(function(){
      copiado.classList.add('hidden');

    }, 2000)
  });

  currentLinkInput.addEventListener('click', function(){
    currentLinkInput.value = currentLink;
  });

  currentLinkInput.addEventListener('keyup', function(){
    currentLinkInput.value = currentLink;
  });

  var whatsappShare = document.getElementById('whatsappShare'),
      whatsappShareLink = 'whatsapp://send?text=' + currentLink;

  if (navigator.userAgent.match(/iPhone|Android/i)) {
    whatsappShare.setAttribute( 'href',  whatsappShareLink);
  }

});